package com.WeApplify.Tap2Meet.Services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.Tables.UserLogin;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import me.pushy.sdk.Pushy;

/**
 * Created by harshagulati on 25/07/17.
 */

public class RegisterForPushNotificationsAsync extends AsyncTask<Void, Void, Exception> {
    Context context;
    SharedPreferences sp;
    String lsDeviceId;
    SharedPreferences.Editor et;
    String deviceToken,lsEmail;
    public static final String TAG = "RegisterForPushNotificationsAsync";
    public void setContext(Context context, String lsEmail)
    {
        this.context = context;
        this.lsEmail = lsEmail;
    }
        protected Exception doInBackground(Void... params) {
            try {
                // Assign a unique token to this device

                 deviceToken = null;
                sp = context.getSharedPreferences(App.DEVICETOKEN, context.MODE_PRIVATE);

                if(deviceToken == null)
                {
                     deviceToken = Pushy.register(context);
                }

                // Log it for debugging purposes
                Log.d("MyApp", "Pushy device token: " + deviceToken);

                et = sp.edit();
                et.putString(UserLogin.UL_DEVICEKEY, deviceToken);
                et.commit();

                callRegisterDevice();

            }
            catch (Exception exc) {
                // Return exc to onPostExecute
                return exc;
            }

            // Success
            return null;
        }

    public void callRegisterDevice() {
        ArrayList<String> arrayList = new ArrayList<>();
        String lsSql;

        lsSql = "UPDATE "+UserLogin.TABLENAME+" SET "+UserLogin.UL_DEVICEKEY+" = '"+deviceToken+"' WHERE "+UserLogin.UL_EMAIL+" = '"+lsEmail+"'";
         arrayList.add(lsSql);

        callCloud(arrayList);

    }

    public void callCloud(ArrayList alSql) {

        DataTransmitter dt = new DataTransmitter();
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        String lsSql;
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);

            dt.setLink(App.getSQLURL());
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            } else {
                /*
                CloudPending cp = new CloudPending(context);
                ContentValues cv = new ContentValues();
                cv.put(CloudPending.CP_OPERATION, jsonArray.toString());
                cv.put(CloudPending.CP_TIME, CustomServices.getCurrentTimeStamp());
                cv.put(CloudPending.CP_FILE, "N");
                cv.put(CloudPending.CP_UPLOADED, "N");
                cv.put(CloudPending.CP_URL, Posita.getSQLURL(context));
                cp.CRUD(cv);
                */
            }
        }

    }

        @Override
        protected void onPostExecute(Exception exc) {
            // Failed?
            if (exc != null) {
                // Show error as toast message
                Toast.makeText(context, exc.toString(), Toast.LENGTH_LONG).show();
                return;
            }
        }
    }

