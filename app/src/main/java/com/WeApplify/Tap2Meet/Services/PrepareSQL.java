package com.WeApplify.Tap2Meet.Services;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


public class PrepareSQL {


    public static String prepareInsert(Context context, String lsTableName, String lsWhere, boolean lbInserClientCode) {
        String lsSql = null;
        lsSql = "SELECT * FROM " + lsTableName;
        String lsClientCode = null;
        if (lsWhere != null) {
            lsSql = lsSql + " WHERE " + lsWhere;
        }
        DbHelper dbHelper = DbHelper.getInstance(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor colCursor = db.rawQuery(lsSql, null);
        colCursor.moveToFirst();
        String lsInsert = " INSERT INTO " + lsTableName + " (";
        String lsCols = "";
        int liCount = 0;
        if (colCursor.getCount() > 0) {
            while (liCount < colCursor.getColumnCount()) {
                String lsColumnName = colCursor.getColumnName(liCount);
                if (liCount > 0) {
                    lsCols = lsCols + "," + lsColumnName;
                } else {
                    lsCols = lsColumnName;
                }
                liCount++;

            }
            if (lbInserClientCode) {
                lsInsert = lsInsert + lsCols + ",clientcode)" + " VALUES (";
            } else {
                lsInsert = lsInsert + lsCols + ") VALUES (";
            }
            String lsValues = "";
            liCount = 0;
            while (liCount < colCursor.getColumnCount()) {
                String lsValue = colCursor.getString(liCount);
                if (lsValue != null) {
                    lsValue = "'" + lsValue + "'";
                }
                if (liCount > 0) {
                    lsValues = lsValues + "," + lsValue;
                } else {
                    lsValues = lsValue;
                }
                liCount++;

            }
            if (lbInserClientCode)
            {
                lsInsert = lsInsert + lsValues + "," + lsClientCode + ")";
            }
            else
            {
                lsInsert = lsInsert + lsValues + ")";
            }

        }
        return lsInsert;
    }


    public static String prepareDelete(Context context, String lsTableName, String lsWhere, boolean lbClientCode)
    {
        String lsClientCode = "";
        String lsSql = "";
        lsSql = "DELETE FROM " + lsTableName;
        if (lsWhere != null) {
            lsSql = lsSql + " WHERE " + lsWhere;
        }


        return lsSql;
    }

}