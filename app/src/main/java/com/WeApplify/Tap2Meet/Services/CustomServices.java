package com.WeApplify.Tap2Meet.Services;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.WeApplify.Tap2Meet.App;

import org.json.JSONObject;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/* This file is used to format the numeric columns
 * Parameters required : String (Input Value)
 * Format Type		   : To Which format the input needs to be formatted
 */

public final class CustomServices
{

    public static Context context;
    private static final String TAG = "Cresco_NF";
    int li_division;

    public CustomServices(Context context)
    {
        CustomServices.context = context;

    }

    public static String AmountNF(String ls_value, String ls_format,
                                  Boolean lb_round)
    {
        String ls_return = "";
        if (ls_value != null)
        {
            Double ld_value = Double.valueOf(ls_value);
            ls_value = String.valueOf(ld_value);
            long ll_value = 0;
            if (lb_round)
            {
                ld_value = ld_value + 0.5;
                ld_value = (double) Math.round(ld_value * 100) / 100;
                ll_value = Math.round(ld_value * 100) / 100;
                ls_value = String.valueOf(ll_value);
            }

            ls_return = Double.toString(ld_value);

            if (ls_format.equals("Rs"))
            {
                int dotPos = -1;

                for (int i = 0; i < ls_value.length(); i++)
                {
                    char c = ls_value.charAt(i);
                    if (c == '.')
                    {
                        dotPos = i;
                    }
                }

                if (dotPos == -1)
                {
                    ls_return = ls_value + ".00";
                } else
                {
                    if (ls_value.length() - dotPos == 1)
                    {
                        ls_return = ls_value + "00";
                    } else if (ls_value.length() - dotPos == 2)
                    {
                        ls_return = ls_value + "0";
                    }
                }
            }
        }

        return ls_return;

    }






    public static void setTypeFaceForViewGroup(ViewGroup vg, Activity mActivity)
    {

        for (int i = 0; i < vg.getChildCount(); i++)
        {
            Log.v(TAG, "In Loop");
            if (vg.getChildAt(i) instanceof ViewGroup)
                setTypeFaceForViewGroup((ViewGroup) vg.getChildAt(i), mActivity);

            else if (vg.getChildAt(i) instanceof TextView)
                ((TextView) vg.getChildAt(i)).setTypeface(Typeface
                        .createFromAsset(mActivity.getAssets(),
                                "fonts/Robot-Italic.ttf"));

        }
    }

    public static String getFormattedDate()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        Date date = new Date();
        String ls_today = sdf.format(date);
        return ls_today;
    }

    public static String getTodaysDate()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String ls_today = sdf.format(date);
        return ls_today;
    }
    public static Double convertToDouble(String ls_amount)
    {
        Double ld_amount = 0.0;
        boolean isMinus = false;

        if(ls_amount == null)
        {
            ls_amount="";
        }        if (ls_amount.indexOf("-") >= 0)
        {
            isMinus = true;
            // Log.v(TAG,ls_amount);
        }

        if (ls_amount.indexOf(" ") > 0)
        {
            ls_amount = ls_amount.substring(ls_amount.indexOf(" "),
                    ls_amount.length());
        }
        ls_amount = ls_amount.replace(",", "").trim();

        try
        {
            ld_amount = Double.parseDouble(ls_amount);
        } catch (NumberFormatException nfe)
        {
            ld_amount = 0.00;
        }
        if (isMinus)
        {
            if (ld_amount > 0)
            {
                ld_amount = ld_amount * -1;
            }
            // ld_amount = ld_amount * -1;
            // Log.v("Minus Amount","Neg Amt " +ld_amount);
        }
        // int li_amount = (int)(ld_amount * 10000);
        // ld_amount = (double) (li_amount / 10000);

        double roundedValue = BigDecimal.valueOf(ld_amount).doubleValue();
        // Log.v(TAG,"Amount is "+ld_amount + " AND Round is "+roundedValue);
        return roundedValue;
    }
    public static Long convertToLong(String ls_amount)
    {
        Double ld_amount = 0.0;
        boolean isMinus = false;

        if(ls_amount == null || ls_amount.equals(""))
        {
            ls_amount = "0";
        }

        long roundedValue =0;
        try {
            roundedValue = Long.valueOf(ls_amount);
        }
        catch (Exception e)
        {
            roundedValue = 0;
        }
        // Log.v(TAG,"Amount is "+ld_amount + " AND Round is "+roundedValue);
        return roundedValue;
    }
    public static Date getTStoDate(long time) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");
        sdf.setTimeZone(tz);//set time zone.
        time = time * 1000;
        String localTime = sdf.format(new Date(time));
        Date date = new Date();
        try {
           date = sdf.parse(localTime);//get local date

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date getDate(long time) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        sdf.setTimeZone(tz);//set time zone.
        String localTime = sdf.format(time * 1000);
        Date date = new Date();
        try {
            date = sdf.parse(localTime);//get local date
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static void showKB(Activity mActivity)
    {
        mActivity.getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    public static void hideSoftKeyboard(Activity activity)
    {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void hideKB(Activity mActivity)
    {
        mActivity.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    public static void showKB(Dialog mDialog)
    {
        mDialog.getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    public static void hideKB(Dialog mDialog)
    {
        mDialog.getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    public static void showKB(Context context)
    {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, 0);

    }

    public static BigDecimal getAmount(String ls_amount)
    {
        if (ls_amount == null || ls_amount.length() <= 0)
        {
            ls_amount = "0.00";
        }

        boolean isMinus = false;

        if (ls_amount.indexOf("-") >= 0)
        {
            isMinus = true;
            Log.v(TAG, ls_amount);
        }

        if (ls_amount.indexOf("(") >= 0)
        {
            isMinus = true;
            ls_amount = ls_amount.replace(")", "").trim();
            ls_amount = ls_amount.replace("(", "").trim();

        }

        if (ls_amount.indexOf(" ") > 0)
        {
            ls_amount = ls_amount.substring(ls_amount.indexOf(" "), ls_amount.length());
        }
        ls_amount = ls_amount.replace(",", "").trim();
        BigDecimal bd_amount;
        try
        {
            bd_amount = new BigDecimal(ls_amount);
        } catch (NumberFormatException nfe)
        {
            Log.v(TAG, nfe.toString());
            bd_amount = new BigDecimal("0.00");
        }

        if (isMinus)
        {
            if (bd_amount.floatValue() > 0)
            {
                bd_amount = bd_amount.abs();
            }

        }
        // bd_amount = bd_amount.
        Log.v(TAG, "Amount " + bd_amount);
        return bd_amount;
    }

    public static Double getOpBalance(String ls_amount)
    {
        Double ld_amount = 0.0;
        boolean isMinus = false;

        if (ls_amount.indexOf("-") >= 0)
        {
            isMinus = true;
            // Log.v(TAG,ls_amount);
        }

        if (ls_amount.indexOf(" ") > 0)
        {
            ls_amount = ls_amount.substring(ls_amount.indexOf(" "),
                    ls_amount.length());
        }
        ls_amount = ls_amount.replace(",", "").trim();

        try
        {
            ld_amount = Double.parseDouble(ls_amount);
        } catch (NumberFormatException nfe)
        {
            ld_amount = 0.00;
        }
        if (isMinus)
        {
            if (ld_amount > 0)
            {
                ld_amount = ld_amount * -1;
            }
            // ld_amount = ld_amount * -1;
            // Log.v("Minus Amount","Neg Amt " +ld_amount);
        }
        // int li_amount = (int)(ld_amount * 10000);
        // ld_amount = (double) (li_amount / 10000);

        double roundedValue = BigDecimal.valueOf(ld_amount).doubleValue();
        // Log.v(TAG,"Amount is "+ld_amount + " AND Round is "+roundedValue);
        return roundedValue;
    }


    public static String getJSONData(JSONObject json, String objectName)
    {
        String ls_data = null;
        try
        {
            ls_data = json.getString(objectName);
        } catch (Exception e)
        {
            Log.v(TAG, "Exception in getJSONDATA " + e.toString());
            ls_data = null;
        }
        return ls_data;
    }

    public static double convertDouble(String ls_string)
    {
        double ld_value = 0.00;
        try
        {
            ld_value = Double.valueOf(ls_string);
        } catch (Exception e)
        {
            Log.v(TAG, "Exception in getDouble of FileType " + e.toString());
        }
        return ld_value;
    }

    public static String NumberFormat(double value, int dec, String format)
    {
        String retValue = "";
        try
        {
            DecimalFormat df = new DecimalFormat(format);
            if (dec > 0)
            {
                df.setMinimumFractionDigits(dec);
                NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
                retValue = formatter.format(value);
            } else
            {
                retValue = df.format(value);
            }


        } catch (Exception e)
        {
            Log.v(TAG, "Exception in NumberFormat " + e.toString());
            retValue = "";
        }
        return retValue;
    }

    public static Bundle getCoordinates(Context context, double xPerc, double yPerc)
    {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        if (xPerc > 0 && xPerc < 100)
        {
            width = (int) (width * xPerc);
        }
        if (yPerc > 0 )
        {
            height = (int) (height * yPerc);
        }
        Bundle b = new Bundle();
        b.putInt("WIDTH",width);
        b.putInt("HEIGHT",height);
        return b;
    }



    public static double round(double value, int places)
    {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static String padString(String str, int length)
    {
        if (str.length() < length) {
            for (int i = 0; i <= length; i++)
                str += " ";
        }
        else {
            str = str.substring(0,length);
        }
        return str;
    }

    public static Bundle getScreenDetails(Activity mActivity) {
        Bundle bundle = new Bundle();
        Point rect = new Point();
        mActivity.getWindowManager().getDefaultDisplay().getSize(rect);
        bundle.putInt("WIDTH", rect.x);
        bundle.putInt("HEIGHT", rect.y);
        return bundle;

    }

    public static boolean isConnectionAvailable(Context context)
    {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }

        }
        return false;
    }

    public static boolean TimeValidator(String time1, String time2) {
        boolean b = false;

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

            try {
                Date ArrTime = sdf.parse(time1);
                Date DepTime = sdf.parse(time2);

                // Function to check whether a time is after an another time
                if( b = DepTime.after(ArrTime))
                {
                    b = true;
                }

            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        return b;
    }


    public static String getStringResourceByName(Context context, String aString) {
        String packageName = context.getPackageName();
        int resId = context.getResources().getIdentifier(aString, "string", packageName);
        return context.getString(resId);
    }


    public static int getColorByName(Context context, String aColor) {
        String packageName = context.getPackageName();
        int resId = context.getResources().getIdentifier(aColor, "color", packageName);
        return context.getResources().getColor(resId);
    }

    public static void ReadPdfs(Activity activity, String lsFile) {

        File file = null;

        file = new File(Environment.getExternalStorageDirectory()+ "/TMF/pdfs/"+lsFile);


        if (file.exists()) {
            Intent target = new Intent(Intent.ACTION_VIEW);

            target.setDataAndType(Uri.fromFile(file), "application/pdf");

            target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            Intent intent = Intent.createChooser(target, "Open File");

            try {
                activity.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                // Instruct the user to install a PDF reader here, or something
            }
        } else
            Toast.makeText(context.getApplicationContext(), "File path is Invalid.", Toast.LENGTH_LONG).show();
    }

    public static void copyImage(String lsImageName)
    {
        String lsDir = App.getImageDir();
        File file = new File(lsDir);
        if (!(file.exists()))
        {
            boolean lbDirCreated = file.mkdirs();
        }
        File tempFile = new File(lsImageName);
        String lsToFile = lsDir + "/"+tempFile.getName();

        try {
            FileUtils.copyFile(lsImageName.toString(), lsToFile);
        }
        catch (Exception e)
        {

            Debugger.debug(TAG,"DB Restore error "+e.toString());
        }
    }
}
