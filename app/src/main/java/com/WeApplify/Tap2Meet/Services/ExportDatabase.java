package com.WeApplify.Tap2Meet.Services;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.WeApplify.Tap2Meet.App;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;


public class ExportDatabase extends AsyncTask<String, Void, Boolean>
{
	Context context ;
    private static final String TAG = "ExportDatabase";

	public ExportDatabase(Context context)
	{
		this.context = context;
	}
		//private final ProgressDialog dialog = new ProgressDialog(context);

        // can use UI thread here
        protected void onPreExecute()
        {
           //this.dialog.setMessage("Exporting database...");
           //this.dialog.show();
        }

        // automatically done on worker thread (separate from UI thread)
        protected Boolean doInBackground(final String... args)
        {
            //Debugger.debug(TAG,"Path is "+Environment.getDataDirectory() + " AND DB FileName "+DbHelper.DB_FILEPATH+ App.getDBName() + " AND App DB NAme "+ App.getDBName());
           //File dbFile = new File(Environment.getDataDirectory() + DbHelper.DB_INTERNALPATH);
            File dbFile = new File(Environment.getDataDirectory() + DbHelper.DB_FILEPATH+ App.getDBName());
            String currentDBPath = Environment.getDataDirectory()+DbHelper.DB_FILEPATH+App.getDBName();
            Debugger.debug(TAG,"DB path is "+currentDBPath);
            dbFile = new File(currentDBPath);
           File exportDir = new File(Environment.getExternalStorageDirectory(), "/" + App.getAppName() + "/");
           if (!exportDir.exists())
           {
              boolean lbFolderCreated = exportDir.mkdirs();
              Debugger.debug(TAG,"Folder Created "+lbFolderCreated);
           }
           File file = new File(exportDir, dbFile.getName());

           try {
              file.createNewFile();
              this.copyFile(dbFile, file);
              return true;
           } catch (IOException e) {
              Log.v("mypck", "error is " + e.toString());
               Debugger.debug(TAG,"Error is "+e.toString());
              return false;
           }
        }

        // can use UI thread here
        protected void onPostExecute(final Boolean success)
        {
        	/*
           if (this.dialog.isShowing()) {
              this.dialog.dismiss();
           }
           */
           if (success) 
           {
              //Toast.makeText(context, "Export successful!", Toast.LENGTH_SHORT).show();
        	 //  CrescoToast.showToast(context,"Please Enter Company Name ",Toast.LENGTH_SHORT);
           } else 
           {
              //Toast.makeText(context, "Export failed", Toast.LENGTH_SHORT).show();
           }
        }

        void copyFile(File src, File dst) throws IOException
        {
           FileChannel inChannel = new FileInputStream(src).getChannel();
           FileChannel outChannel = new FileOutputStream(dst).getChannel();
           try 
           {
              inChannel.transferTo(0, inChannel.size(), outChannel);
           } 
           finally 
           {
              if (inChannel != null)
                 inChannel.close();
              if (outChannel != null)
                 outChannel.close();
           }
        }

     }