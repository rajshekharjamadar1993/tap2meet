package com.WeApplify.Tap2Meet.Services;

import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class FileUtils
{
    /**
     * Creates the specified <code>toFile</code> as a byte for byte copy of the
     * <code>fromFile</code>. If <code>toFile</code> already exists, then it
     * will be replaced with a copy of <code>fromFile</code>. The name and path
     * of <code>toFile</code> will be that of <code>toFile</code>.<br/>
     * <br/>
     * <i> Note: <code>fromFile</code> and <code>toFile</code> will be closed by
     * this function.</i>
     * 
     * @param fromFile
     *            - FileInputStream for the file to copy from.
     * @param toFile
     *            - FileInputStream for the file to copy to.
     */
	private static final String TAG = "FileUtils";
	public final static String TAG_MAPP_TYPE	= "APPTYPE";
    public final static String TAG_CLIENTID	= "CLIENTID";

    public static void copyFile(File fromFile, File toFile) throws IOException
    {
    	FileInputStream fis 	= new FileInputStream(fromFile);
    	FileOutputStream fos 	= new FileOutputStream(toFile);
    	
        FileChannel fromChannel = null;
        FileChannel toChannel = null;
        try
        {
            fromChannel = fis.getChannel();
            toChannel = fos.getChannel();
            long ll_bytes = fromChannel.transferTo(0, fromChannel.size(), toChannel);
            Log.v(TAG,"File Size "+ll_bytes);
        } 
        finally {
            try {
                if (fromChannel != null) {
                    fromChannel.close();
                }
            } finally {
                if (toChannel != null) {
                    toChannel.close();
                }
            }
        }
    }
    
    public static void copyFile(String fromFilePath, String toFilePath) throws IOException
    {
    	File fromFile 	= new File(fromFilePath);
    	File toFile 	= new File(toFilePath);
    	
    	FileInputStream fis 	= new FileInputStream(fromFile);
    	FileOutputStream fos 	= new FileOutputStream(toFile);
    	
        FileChannel fromChannel = null;
        FileChannel toChannel = null;
        try
        {
            fromChannel = fis.getChannel();
            toChannel = fos.getChannel();
            long ll_bytes = fromChannel.transferTo(0, fromChannel.size(), toChannel);
            Log.v(TAG,"File Size "+ll_bytes);
        } 
        finally {
            try {
                if (fromChannel != null) {
                    fromChannel.close();
                }
            } finally {
                if (toChannel != null) {
                    toChannel.close();
                }
            }
        }
    }
    


	

	


}