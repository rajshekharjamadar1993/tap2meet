package com.WeApplify.Tap2Meet.Services;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.R;


public class CustomDialog extends PopupWindow implements View.OnClickListener {
    Activity activity;
    Context context;
    PopupWindow popW;
    View popUp;
    private DialogButtonClick mClickListener;
    Button cbOK, cbCancel;
    TextView tvTitle, tvMessage;
    String lsTitle = "", lsMessage = "";
    View blankView, blankViewOK;
    Boolean lbCancelVisible;


    public CustomDialog(Activity activity, Context context) {
        this.activity = activity;
        this.context = context;
    }

    public void showDialog() {
        popUp = LayoutInflater.from(context).inflate(R.layout.custom_dialog, null);


        tvMessage = (TextView) popUp.findViewById(R.id.tvMessage);
        tvTitle = (TextView) popUp.findViewById(R.id.tvTitle);

        tvTitle.setText(lsTitle);
        tvMessage.setText(lsMessage);

        cbOK = (Button) popUp.findViewById(R.id.cbOK);
        cbCancel = (Button) popUp.findViewById(R.id.cbCancel);
        blankView = (View) popUp.findViewById(R.id.blankView);
        blankViewOK = (View) popUp.findViewById(R.id.blankViewOK);

        cbOK.setOnClickListener(this);

        cbCancel.setOnClickListener(this);


        popW = new PopupWindow(popUp, 0, 0, true);
        Bundle b = CustomServices.getCoordinates(context, .80, .30);
        popW.setWidth(b.getInt("WIDTH"));
        popW.setHeight(b.getInt("HEIGHT"));
        popW.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popW.setOutsideTouchable(false);

        popW.showAtLocation(activity.getWindow().getDecorView(), Gravity.CENTER, 0, 0);
        popUp.startAnimation(AnimationUtils.loadAnimation(context,
                R.anim.slide_down));
    }


    public void setDialogButtonClickListener(DialogButtonClick listener) {
        mClickListener = listener;
    }


    public interface DialogButtonClick {

        void DialogButtonClicked(View view);
    }

    @Override
    public void onClick(View v) {
        mClickListener.DialogButtonClicked(v);
        if (popW != null) {
            popW.dismiss();

        } else {

            ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).removeView(popUp);
            popUp = null;
        }
    }


    public void setTitle(String lsTitle) {
        this.lsTitle = lsTitle;
    }

    public void setMessage(String lsMessage) {
        this.lsMessage = lsMessage;
    }

    public void setCancel(boolean lbEnableCancel) {
        lbCancelVisible = lbEnableCancel;
    }

}