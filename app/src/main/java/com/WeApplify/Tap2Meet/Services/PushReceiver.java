package com.WeApplify.Tap2Meet.Services;

/**
 * Created by harshagulati on 25/07/17.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.Models.DateFromNotificationModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.UserLogin;
import com.WeApplify.Tap2Meet.UI.CaptureDetails;
import com.WeApplify.Tap2Meet.UI.Home;
import com.WeApplify.Tap2Meet.UI.MyBookings;
import com.WeApplify.Tap2Meet.UI.Notifications;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PushReceiver extends BroadcastReceiver {
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onReceive(Context context, Intent intent) {
        String notificationTitle = "Tap2Meet";
        String notificationText = "";
        Intent myintent = null;
        SharedPreferences sp = context.getSharedPreferences(App.SCHEDULEDATE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor;
        String det = intent.getStringExtra(App.MEETING_DATE);
        String startTime = intent.getStringExtra(App.MEETING_STARTTIME);
        String endTime = intent.getStringExtra(App.MEETING_ENDTIME);

        MyBookings myBookings;

        //saveDetails();
        if (intent.getStringExtra(App.NOTIFICATION) != null) {
            notificationText = intent.getStringExtra(App.NOTIFICATION);
        }
        if (intent.getStringExtra(App.NOTIFICATION).contains(App.MESSAGE_NOTIFICATION + " " + det)) {
            notificationText = notificationText + " from "+startTime+ "  to  "+endTime;
            myintent = new Intent(context, MyBookings.class);
            myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            editor = sp.edit();
            editor.putString(App.SCHEDULEDATE, det);
            editor.commit();

        } else if (intent.getStringExtra(App.NOTIFICATION).contains(App.CANCEL_NOTIFICATION)) {
            myintent = new Intent(context, Home.class);
            myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            notificationText = notificationText + " scheduled on " + det +
                    " from " + startTime + "  to  " + endTime;
        }
        else if(intent.getStringExtra(App.NOTIFICATION).contains(App.RESCHEDULE_NOTIFICATION))
        {
            String meetingDateBeforeChange = intent.getStringExtra("MeetingDateBeforeChange");
            notificationText = notificationText + " from "+meetingDateBeforeChange
                    +"  to  "+det+" \n\nTime Details : "+startTime+"  to  "+endTime;
            myintent = new Intent(context, MyBookings.class);
            myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            editor = sp.edit();
            editor.putString(App.SCHEDULEDATE, det);
            editor.commit();
        }

        // Prepare a notification with vibration, sound and lights
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.drawable.tap2meetlogo);
        builder.setContentTitle(notificationTitle);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(notificationText));
        builder.setContentText(notificationText);
        builder.setDefaults(Notification.DEFAULT_ALL);
        builder.setPriority(Notification.PRIORITY_HIGH);
        builder.setLights(Color.RED, 1000, 1000);
        builder.setVibrate(new long[]{0, 400, 250, 400});
        builder.setAutoCancel(true);
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        builder.setContentIntent(PendingIntent.getActivity(context, 0, myintent, PendingIntent.FLAG_UPDATE_CURRENT));

        // Get an instance of the NotificationManager service
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        // Build the notification and display it
        notificationManager.notify(1, builder.build());
    }

    public void saveDetails()
    {

    }
    /*private PendingIntent createOnDismissedIntent(Context context, int notificationId) {
        Intent intent1 = new Intent(context, Notifications.class);
        intent1.putExtra("ABC", notificationId);
        PendingIntent pendingIntent =
                PendingIntent.getBroadcast(context.getApplicationContext(),
                        notificationId, intent1, 0);
        return pendingIntent;
    }*/

}

