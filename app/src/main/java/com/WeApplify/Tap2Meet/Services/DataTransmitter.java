package com.WeApplify.Tap2Meet.Services;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DataTransmitter extends AsyncTask<Void, Void, Void> {
    private static String TAG = "DataTransmitter";
    public static String IMAGE = "IMAGE";
    public static String DATA = "DATA";
    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
    String link;
    HashMap<String, String> params;
    FormBody.Builder fbBuilder;
    String lsUploadType;
    Context context;
    ArrayList imageName;
    onResponseReceived mOnResponseReceived;
    onErrorReceived mOnErrorReceived;
    JSONObject jsonObject;

    public void setLink(String lsLink) {
        link = lsLink;
    }

    public void setPostParams(HashMap<String, String> hMap) {
        params = hMap;

    }

    public void setType(String lsImageORData) {
        lsUploadType = lsImageORData;
    }

    public void setImageData(Context context, ArrayList imageName) {
        this.context = context;
        this.imageName = imageName;
    }

    @Override
    protected Void doInBackground(Void... xParams) {

        if (lsUploadType.equals(IMAGE)) {
            if (imageName != null) {
                //uploadFile(context, imageName);
            }
        } else {
            getData();
        }
        return null;
    } //end of


    public void getData() {
        OkHttpClient client = new OkHttpClient();
        fbBuilder = new FormBody.Builder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            fbBuilder.add(entry.getKey(), entry.getValue());
        }

        RequestBody formBody = fbBuilder.build();
        Request request = new Request.Builder()
                .url(link)
                .post(formBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Debugger.debug(TAG,"Failure Occured "+e.toString());
                if (mOnErrorReceived != null)
                {
                    mOnErrorReceived.onError(e);
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String in = response.body().string();
                if (mOnResponseReceived != null) {
                    mOnResponseReceived.onResponse(in);
                }

                Log.v(TAG, "PHP Response " + in);
            }
        });
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }


    public interface onResponseReceived
    {
        public void onResponse(String data);
    }

    public interface onErrorReceived
    {
        public void onError(IOException e);
    }


    public void setOnResponseRevieved(onResponseReceived val)
    {
        this.mOnResponseReceived = val;
    }


    public void setOnErrorRevieved(onErrorReceived val)
    {
        this.mOnErrorReceived = val;
    }


}