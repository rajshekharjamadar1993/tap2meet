package com.WeApplify.Tap2Meet.Services;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.WeApplify.Tap2Meet.R;

/**
 * Created by harshagulati on 09/08/17.
 */

public class ShowProgressDialog {

    static ProgressDialog progressDialog;
    static String lsMessage;


    public static void showProgress(Context context) {

        progressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        progressDialog.getLayoutInflater().inflate(R.layout.progress_bar,null);
        if (lsMessage == null || lsMessage.length() <= 0) lsMessage = "Loading";
        progressDialog.setTitle(lsMessage);
        progressDialog.setProgressStyle(progressDialog.STYLE_SPINNER);
        progressDialog.setMax(100);
        progressDialog.setProgress(0);
        progressDialog.show();
    }

    public static void hideProgressDialog(Context context) {
        progressDialog.hide();
    }

    public static void setTitle(String lsText) {
        lsMessage = lsText;
    }
}
