package com.WeApplify.Tap2Meet.Services;

import  android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;


public class CalendarView extends PopupWindow implements DatePickerDialog.OnDateSetListener {

    private TextView dateTextView;
    private CheckBox modeDarkDate;
    private CheckBox modeCustomAccentDate;
    private CheckBox vibrateDate;
    private CheckBox dismissDate;
    private CheckBox titleDate;
    private CheckBox showYearFirst;
    private CheckBox showVersion2;
    private CheckBox limitSelectableDays;
    private CheckBox highlightDays;
    Activity activity;
    Context context;
    boolean lbLimit;
    TextView textView;
    View popView;
    PopupWindow popW;
    CVOnDateChanged mOnDateChanged;
    DatePickerDialog dpd;


    public CalendarView(Activity activity, Context context, boolean lbLimit)
    {
        this.activity = activity;
        this.context  = context;
        this.lbLimit  = lbLimit;
    }



    public void setupPopUp(TextView tvDate,String lsCurrentDate,int color)
    {
        this.textView = tvDate;

        int liYear,liMonth,liDayOfMonth;

        Calendar now = Calendar.getInstance();
        liYear = now.get(Calendar.YEAR);
        liMonth = now.get(Calendar.MONTH);
        liDayOfMonth  = now.get(Calendar.DAY_OF_MONTH);
        if (lsCurrentDate != null && lsCurrentDate.length() > 0)
        {
            liYear = Integer.valueOf(lsCurrentDate.substring(6,10));
            liMonth = Integer.valueOf(lsCurrentDate.substring(3,5)) - 1;
            liDayOfMonth   = Integer.valueOf(lsCurrentDate.substring(0,2));

        }

        dpd = DatePickerDialog.newInstance(
                this,
                liYear,
                liMonth,
                liDayOfMonth
        );

        dpd.setAccentColor(color);
        dpd.setMinDate(Calendar.getInstance());
        dpd.show(activity.getFragmentManager(),"Show Dates");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "You picked the following date: "+dayOfMonth+"/"+(++monthOfYear)+"/"+year;
        String lsDay,lsMonth;
        lsDay = String.valueOf(dayOfMonth);
        lsMonth = String.valueOf(monthOfYear);
        if (dayOfMonth < 10)
        {
            lsDay = "0"+dayOfMonth;
        }
        if (monthOfYear < 10)
        {
            lsMonth = "0"+monthOfYear;
        }
        date = lsDay + "/"+lsMonth + "/"+year;
        mOnDateChanged.CVDateChanged(textView,date);
        textView.setText(date);
        dpd.dismiss();
    }

    public interface CVOnDateChanged
    {
        void CVDateChanged(TextView tvDate, String lsDate);
    }

    public void setOnDateChanged(CVOnDateChanged mDateChanged)
    {
        this.mOnDateChanged = mDateChanged;
    }


}


