package com.WeApplify.Tap2Meet.Services;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.Tables.CreateTables;
import com.WeApplify.Tap2Meet.Upgrade.AssetUtils;
import com.WeApplify.Tap2Meet.Upgrade.SqlParser;
import com.WeApplify.Tap2Meet.Upgrade.VersionUtils;

import java.io.IOException;

public class DbHelper extends SQLiteOpenHelper {
    protected static final String TAG = "DbHelper";
    public static final String DB_FILEPATH = Environment.getDataDirectory() + "/com.WeApplify.Tap2Meet/databases/";
    public static final String APP_PATH = Environment.getExternalStorageDirectory() + "/ " + App.getAppName() + "/";
    private static DbHelper mInstance = null;

    public Context context;
    private static final String CREATEFILE = "create.sql";
    private static final String UPGRADEFILE_PREFIX = "upgrade-";
    private static final String UPGRADEFILE_SUFFIX = ".sql";
    private static final String SQL_DIR = "sql";

    private SQLiteDatabase db;
    static int dbVersion;
    private String dbBackPath = "";


    public DbHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DbHelper(Context c, int version) {
        super(c, App.getDBName(), null, version);
        context = c;
    }

    public static DbHelper getInstance(Context c) {
        try {
            dbVersion = VersionUtils.getVersionCode(c);
        } catch (NameNotFoundException e) {

            e.printStackTrace();
        }
        if (mInstance == null) {
            mInstance = new DbHelper(c, dbVersion);
        }

        return mInstance;
    }

    public static void closeInstance() {
        mInstance = null;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //Create M_Group Table
        this.db = db;
        Log.v(TAG, "In Oncreate of DBHelper");
        CreateTables ct = new CreateTables(context);
        ct.createUser(db, context);
        //setUpDefaultData(db);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            Log.v(TAG, "upgrade database from {} to {}" + oldVersion + " to " + newVersion);
            for (String sqlFile : AssetUtils.list(SQL_DIR, this.context.getAssets())) {
                if (sqlFile.startsWith(UPGRADEFILE_PREFIX)) {
                    int fileVersion = Integer.parseInt(sqlFile.substring(UPGRADEFILE_PREFIX.length(), sqlFile.length() - UPGRADEFILE_SUFFIX.length()));
                    if (fileVersion > oldVersion && fileVersion <= newVersion) {
                        execSqlFile(sqlFile, db);
                    }
                }
            }
        } catch (IOException exception) {
            throw new RuntimeException("Database upgrade failed", exception);
        }

    }

    protected void execSqlFile(String sqlFile, SQLiteDatabase db) throws SQLException, IOException {
        Log.v(TAG, "  exec sql file: {}" + sqlFile);
        Toast.makeText(context, "Upgrading Database File", Toast.LENGTH_SHORT).show();
        for (String sqlInstruction : SqlParser.parseSqlFile(SQL_DIR + "/" + sqlFile, this.context.getAssets())) {
            Log.v(TAG, "sql: {}" + sqlFile + " And Instruction " + sqlInstruction);

            db.execSQL(sqlInstruction);
        }
    }


    public boolean restoreDB() {
        return true;
    }

    public boolean backupDB() {

        ExportDatabase exportDatabase = new ExportDatabase(context);
        exportDatabase.execute();
        return true;
    }
}