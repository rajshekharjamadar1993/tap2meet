package com.WeApplify.Tap2Meet.Services;


import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TimePicker;

import com.WeApplify.Tap2Meet.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class MyDialog extends PopupWindow implements View.OnClickListener {
    Activity activity;
    Context context;
    PopupWindow popW;
    View popUp;
    private DialogButtonClick mClickListener;
    Button cbOK, cbCancel;
    TextView tvTitle;
    Button cbSaveTime;
    String lsTitle = "", lsMessage = "";
    View blankView, blankViewOK;
    Boolean lbCancelVisible;
    EditText etParticipant;
    TextView tvDoseTime;
    String lsMedicines;
    String lsActive;
    ImageView ivCheck, ivUncheck;


    public MyDialog(Activity activity, Context context) {
        this.activity = activity;
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void showDialog() {
        popUp = LayoutInflater.from(context).inflate(R.layout.my_dialog, null);

        tvTitle = (TextView) popUp.findViewById(R.id.tvTitle);
        etParticipant = (EditText) popUp.findViewById(R.id.etParticipant);


        tvTitle.setText("Add Participant");

        cbOK = (Button) popUp.findViewById(R.id.cbOK);
        cbCancel = (Button) popUp.findViewById(R.id.cbCancel);
        blankView = (View) popUp.findViewById(R.id.blankView);
        blankViewOK = (View) popUp.findViewById(R.id.blankViewOK);

        cbOK.setOnClickListener(this);

        cbCancel.setOnClickListener(this);


        popW = new PopupWindow(popUp, 0, 0, true);
        Bundle b = CustomServices.getCoordinates(context, .80, .30);
        popW.setWidth(b.getInt("WIDTH"));
        popW.setHeight(b.getInt("HEIGHT"));
        //  popW.setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.app_color_blue)));
        popW.setOutsideTouchable(true);
        activity.setFinishOnTouchOutside(true);


        popW.showAtLocation(activity.getWindow().getDecorView(), Gravity.CENTER, 0, 0);


    }


    public void setDialogButtonClickListener(DialogButtonClick listener) {
        mClickListener = listener;
    }


    public interface DialogButtonClick {

        void DialogButtonClicked(View view, String lsValue);
    }

    @Override
    public void onClick(View v) {
        mClickListener.DialogButtonClicked(v, etParticipant.getText().toString());
        if (popW != null) {
            popW.dismiss();

        } else {
            ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).removeView(popUp);
            popUp = null;

        }
    }



    public void setTitle(String lsTitle) {
        this.lsTitle = lsTitle;
    }


    public void setCancel(boolean lbEnableCancel) {
        lbCancelVisible = lbEnableCancel;
    }

}