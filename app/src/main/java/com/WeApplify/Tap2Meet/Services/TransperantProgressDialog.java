package com.WeApplify.Tap2Meet.Services;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import com.WeApplify.Tap2Meet.R;

/**
 * Created by ADMIN on 16-11-2017.
 */


public class TransperantProgressDialog extends Dialog {

    public TransperantProgressDialog(Context context) {
        super(context,R.style.MyTheme);

        WindowManager.LayoutParams wlmp = getWindow().getAttributes();

        wlmp.gravity = Gravity.CENTER_HORIZONTAL;
        getWindow().setAttributes(wlmp);
        setTitle(null);
        setCancelable(true);
        setOnCancelListener(null);
        View view = LayoutInflater.from(context).inflate(
                R.layout.progress_bar, null);
        setContentView(view);
    }
}
