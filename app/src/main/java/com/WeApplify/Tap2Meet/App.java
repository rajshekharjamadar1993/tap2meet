package com.WeApplify.Tap2Meet;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.provider.DocumentFile;
import android.util.Log;

import com.wdullaer.materialdatetimepicker.time.Timepoint;
import org.w3c.dom.DocumentType;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
/*
@ReportsCrashes(
        mailTo = "Harsha.Gulati@weapplify.tech",
        mode = ReportingInteractionMode.DIALOG,
        resToastText = R.string.crash_toast_text,
        resDialogText = R.string.crash_dialog_text,
        resDialogIcon = android.R.drawable.ic_dialog_info,
        resDialogTitle = R.string.crash_dialog_title,
        resDialogCommentPrompt = R.string.crash_dialog_comment_prompt,
        resDialogOkToast = R.string.crash_dialog_ok_toast)*/

public class App extends Application {
    static String TAG = "App";
    static App myApp;

    public static final String IMPORT_DB = "import db";
    public static final String EXPORT_DB = "export db";

    public static final String FROM = "FROM";
    public static String DATABASE_NAME = "bmw.db";
    public static String USERID = "";
    public static String DISPLAY_DATE = "";
    public static String DATE = "";
    public static String ID = "";
    public static String IMAGEURL = "http://www.weapplify.tech/Tap2Meet/UploadMultipleFiles.php";
    public static String SQLURL = "http://www.weapplify.tech/Tap2Meet/SQLOperation.php";
    public static String URL = "http://www.weapplify.tech/Tap2Meet/GetResults.php";

    public static Calendar calendar;
    public static String lsAppName = "Tap2Meet";

    public static String lsImageDir = Environment.getExternalStorageDirectory() + "/" + App.getAppName() + "/Images";

    public static int RESULT_CODE_WIFI = 100;
    public static String STATUS = "N";
    public static String BOOKING_STATUS = "A";
    public static String DEVICETOKEN = "devicetoken";
    public static String MESSAGE_NOTIFICATION = "Meeting Scheduled on";
    public static String CANCEL_NOTIFICATION = "Your Meeting got cancelled";
    public static String RESCHEDULE_NOTIFICATION = "Your meeting got rescheduled";
    public static String MEETING_DATE = "meet_date";
    public static String MEETING_STARTTIME = "meet_startTime";
    public static String MEETING_ENDTIME = "meet_endTime";
    public static String NOTIFICATION = "message";

    public static String LOGINCREDENTIALS = "logindata";
    public static String SCHEDULEDATE = "scheduledate";

    @Override
    public void onCreate() {
        super.onCreate();
        //     ACRA.init(this);
    }

    public static App getInstance() {
        if (myApp == null) {
            myApp = new App();
        }
        return myApp;
    }

    public static Bundle getAppData() {

        Bundle companyData = new Bundle();
        return companyData;


    }

    public static String getAppName() {
        return lsAppName;
    }


    public static void setAppData(Bundle b) {

    }

    public static String getImageDir() {
        return lsImageDir;
    }


    public static String getDate() {
        calendar = Calendar.getInstance();

        if (DATE == "") {
            Date today = calendar.getTime();
            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
            DATE = sdf.format(today);

        }

        return DATE;
    }

    public static String getDataURL() {

        return URL;

    }

    public static void setDataURL(String lsURL) {
        URL = lsURL;
    }

    public static void setDate(String lsDate) {
        DATE = lsDate;
    }

    public static String getDisplayDate() {
        calendar = Calendar.getInstance();

        if (DISPLAY_DATE == "") {
            Date today = calendar.getTime();
            String myFormat = "dd-MMM"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
            DATE = sdf.format(today);

        }

        return DATE;
    }


    public static void setDisplayDate(String lsDate) {
        DATE = lsDate;
    }

    public static String getUserID() {
        return USERID;
    }

    public static void setUserID(String lsUser) {
        USERID = lsUser;
    }

    public static String getDBName() {
        return DATABASE_NAME;
    }

    public static void setDBName(String lsName) {
        DATABASE_NAME = lsName;
    }

    public static String getID() {
        return ID;
    }

    public static void setID(String lsId) {
        ID = lsId;
    }


    public static String getCurrentTimeStamp() {
        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        return ts;
    }

    public static Context getContext() {
        return getContext();
    }

    public static String getSQLURL() {
        return SQLURL;
    }

    public static String getImageURL() {
        return IMAGEURL;
    }

  /*  public static void uploadOKImage(String sourceFile, final long llProductId, final long llImageId) {


        OkHttpClient client = new OkHttpClient();
        File file = new File(App.getImageDir() + "/" + sourceFile);
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("ClientCode", "")
                .addFormDataPart("uploadedfile", App.getImageDir() + "/" + sourceFile,
                        RequestBody.create(MediaType.parse("image/png"), file))
                //RequestBody.create(MediaType.parse("image/png"), file))
                .build();

        Request request = new Request.Builder()
                .url(App.getImageURL())
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.v(TAG, "Image Upload Error " + e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String in = response.body().string();
                Log.v(TAG, "PHP Response " + in);
                if (response.code() == 200) {
                    //mProduct.updatePMUploaded(llProductId, true);
                    //imageFiles.updateImageUpload(llImageId, true);
                    //callImageSql(llProductId);
                }
            }
        });

    }*/

    public static void uploadOKImage(String sourceFile, final Context context, final long llProductId, final long llImageId) {

        OkHttpClient client = new OkHttpClient();
        File file = new File(App.getImageDir() + "/" + sourceFile);
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("ClientCode", "")
                .addFormDataPart("uploadedfile", App.getImageDir() + "/" + sourceFile,
                        RequestBody.create(MediaType.parse("*/*"), file))
                .build();


        Request request = new Request.Builder()
                .url(App.getImageURL())
                .post(requestBody)
                .build();


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.v(TAG, "Image Upload Error " + e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String in = response.body().string();
                Log.v(TAG, "PHP Response " + in);

                if (response.code() == 200) {


                }
            }
        });

    }

}