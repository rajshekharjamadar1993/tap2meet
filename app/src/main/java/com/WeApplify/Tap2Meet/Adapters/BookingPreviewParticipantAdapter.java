package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.Models.ParticipantModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.MeetingParticipants;
import com.WeApplify.Tap2Meet.Tables.UserLogin;

import java.util.ArrayList;

/**
 * Created by Admin on 29-03-2017.
 */

public class BookingPreviewParticipantAdapter extends SelectableAdapter<BookingPreviewParticipantAdapter.ViewHolder> {


    private static final String TAG = "BookingPreviewParticipantAdapter";
    static Context context;
    static BookingPreviewParticipantAdapter adapter;
    static ArrayList<ParticipantModel> arrayList;
    static itemClickIterface mItemClick;
    static Cursor cursor;
    //


    public BookingPreviewParticipantAdapter(Context context, ArrayList<ParticipantModel> arrayList ) {

        this.context = context;
        adapter = this;
        this.arrayList = arrayList;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvParticipant;



        public ViewHolder(View view) {
            super(view);

            tvParticipant = (TextView) view.findViewById(R.id.tvParticipant);

        }

        @Override
        public void onClick(View v) {

        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.booking_preview_participant_row, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;

    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder,final int position) {
        Bundle b = arrayList.get(position).getData();

        String lsName = b.getString(MeetingParticipants.MP_EMPLOYEEEMAIL);
        String lsHost = b.getString(UserLogin.UL_EMAIL);

        if(lsName.equals(lsHost))
        {
            viewHolder.tvParticipant.setText(lsName+"   ( Host )");
        }
        else
        {
            viewHolder.tvParticipant.setText(lsName);
        }






    /*    if(position % 2 == 0)
        {
            viewHolder.tvParticipant.setBackgroundResource(R.color.alColor);

        }
        else
        {
            viewHolder.tvParticipant.setBackgroundResource(R.color.alColor_1);
        }*/


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public interface itemClickIterface
    {
        public void itemClick(int position);
    }

    public void setItemClickIterface(itemClickIterface mItemListener)
    {
        mItemClick = mItemListener;
    }


    public void swapData(ArrayList list) {
        if (arrayList == null) {
            arrayList = new ArrayList<ParticipantModel>();
        }
        arrayList = list;
        notifyDataSetChanged();

    }



}


