package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.WeApplify.Tap2Meet.Models.FeedbackModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.Debugger;
import com.WeApplify.Tap2Meet.Tables.Constants;

import java.util.ArrayList;

/**
 * Created by Admin on 07-07-2017.
 */

public class FeedbackAdapter extends SelectableAdapter<FeedbackAdapter.ViewHolder> {

    private static final String TAG = "FeedbackAdapter";
    static Context context;
    static FeedbackAdapter adapter;
    static ArrayList<FeedbackModel> alFeedback;
    static Constants constants;
    static Cursor cursor;
    static itemClickIterface mItemClick;
    //


    public FeedbackAdapter(Context context, ArrayList<FeedbackModel> alFeedback) {

        this.context = context;
        adapter = this;
        this.alFeedback = alFeedback;
        constants = new Constants(context);

    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvQuestion;
        RatingBar rbFeedback;
        LinearLayout llFeedback;

        public ViewHolder(View view) {
            super(view);
            tvQuestion = (TextView) view.findViewById(R.id.tvQuestion);
            rbFeedback = (RatingBar) view.findViewById(R.id.rbFeedback);
            llFeedback = (LinearLayout) view.findViewById(R.id.llFeedback);

            rbFeedback.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mItemClick != null)
            {
                mItemClick.itemClick(getAdapterPosition());
            }
        }
    }

    @Override
    public FeedbackAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.feebback_row, parent, false);
        FeedbackAdapter.ViewHolder vh = new FeedbackAdapter.ViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(final FeedbackAdapter.ViewHolder viewHolder, final int position) {
        Bundle b = alFeedback.get(position).getData();
        String lsQuestion = b.getString(Constants.LOVS_DISPLAYVALUE);
        viewHolder.tvQuestion.setText(lsQuestion);

        viewHolder.rbFeedback.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                String rateValue = String.valueOf(viewHolder.rbFeedback.getRating());
                System.out.println("Rate for Module is"+rateValue);
                alFeedback.get(position).setResults(rateValue);
            }
        });
    }

    public interface itemClickIterface {
        public void itemClick(int position);
    }

    public void setItemClickInterface(itemClickIterface mItemListener) {
        mItemClick = mItemListener;
    }

    @Override
    public int getItemCount() {
        return alFeedback.size();
    }

    public void swapData(ArrayList list) {
        if (alFeedback == null) {
            alFeedback = new ArrayList<FeedbackModel>();
        }
        alFeedback = list;
        notifyDataSetChanged();

    }


}

