package com.WeApplify.Tap2Meet.Adapters;


import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;

import java.util.ArrayList;
import java.util.List;

public abstract class SelectableAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    private static final String TAG = "SelectableAdapter";
    private SparseBooleanArray selectedItems;
    private static Boolean lbMultiple = false;

    public SelectableAdapter() {
        selectedItems = new SparseBooleanArray();
    }

    public boolean isSelected(int position) {
        return getSelectedItems().contains(position);
    }

    public void removeSelection(int position) {
        if (getSelectedItems().contains(position)) {
            getSelectedItems().remove(position);
        }
        notifyItemChanged(position);
    }

    public void setMultiple(boolean value) {
        lbMultiple = value;
    }

    public void toggleSelection(int position) {
        boolean lbUncheck = false;
        if (selectedItems.get(position, false)) {
            lbUncheck = true;
        }
        if (!(lbMultiple)) {
            clearSelection();
        }

        if (selectedItems.get(position, false) || lbUncheck) {
            selectedItems.delete(position);
        } else {
            selectedItems.put(position, true);
        }
        notifyItemChanged(position);
    }


    public void clearSelection() {
        List<Integer> selection = getSelectedItems();
        selectedItems.clear();
        for (Integer i : selection) {
            notifyItemChanged(i);
        }
    }


    public int getSelectedCount() {
        return selectedItems.size();
    }

    public ArrayList<Integer> getSelectedItems() {
        ArrayList<Integer> items = new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }




}