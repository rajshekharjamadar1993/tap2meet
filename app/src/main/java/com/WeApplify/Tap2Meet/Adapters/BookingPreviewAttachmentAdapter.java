package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.Models.AttachmentModel;
import com.WeApplify.Tap2Meet.Models.ParticipantModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.MeetingPreread;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Admin on 29-03-2017.
 */

public class BookingPreviewAttachmentAdapter extends SelectableAdapter<BookingPreviewAttachmentAdapter.ViewHolder> {


    private static final String TAG = "BookingPreviewAttachmentAdapter";
    static Context context;
    static BookingPreviewAttachmentAdapter adapter;
    static ArrayList<AttachmentModel> arrayList;
    static itemClickIterface mItemClick;
    static Cursor cursor;


    public BookingPreviewAttachmentAdapter(Context context, ArrayList<AttachmentModel> arrayList ) {

        this.context = context;
        adapter = this;
        this.arrayList = arrayList;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvAttachment;



        public ViewHolder(View view) {
            super(view);

            tvAttachment = (TextView) view.findViewById(R.id.tvAttachment);


        }

        @Override
        public void onClick(View v) {

        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.booking_preview_attachment_row, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;

    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder,final int position) {
        Bundle b = arrayList.get(position).getData();

        String lsName = b.getString(MeetingPreread.MPR_FILE);

        File file = new File (lsName);

        String lsFileName = file.getName();


        viewHolder.tvAttachment.setText(lsFileName);


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    /*


      public interface dialerInterface
      {
          public void callDialer(int position);
      }

      public void setDialerListener(dialerInterface mListener)
      {
          mDialer = mListener;
      }
  */
    public interface itemClickIterface
    {
        public void itemClick(int position);
    }

    public void setItemClickIterface(itemClickIterface mItemListener)
    {
        mItemClick = mItemListener;
    }


    public void swapData(ArrayList list) {
        if (arrayList == null) {
            arrayList = new ArrayList<AttachmentModel>();
        }
        arrayList = list;
        notifyDataSetChanged();

    }



}


