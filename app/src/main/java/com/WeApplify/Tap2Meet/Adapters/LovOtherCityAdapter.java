package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Models.LovOtherCityModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.Lovs;

import java.util.ArrayList;

/**
 * Created by harshagulati on 08/06/17.
 */

public class LovOtherCityAdapter extends SelectableAdapter<LovOtherCityAdapter.ViewHolder> {

    private static final String TAG = "LovOtherCityAdapter";
    static Context context;
    static LovOtherCityAdapter adapter;
    static ArrayList<LovOtherCityModel> arrayList;
    static Lovs lovs;
    static itemClickIterface mItemClick;
    static Cursor cursor;
    //


    public LovOtherCityAdapter(Context context, ArrayList<LovOtherCityModel> arrayList) {

        this.context = context;
        adapter = this;
        this.arrayList = arrayList;
        lovs = new Lovs(context);

    }

    public LovOtherCityAdapter(Context context) {
        this.context = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvCity;
        ImageView ivCity;
        LinearLayout llCity;


        public ViewHolder(View view) {
            super(view);

            tvCity = (TextView) view.findViewById(R.id.tvCity);
            ivCity = (ImageView) view.findViewById(R.id.ivCity);
            llCity = (LinearLayout) view.findViewById(R.id.llCity);

            llCity.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            if(mItemClick != null)
            {
                mItemClick.itemClick(getAdapterPosition());
            }

        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.other_city_row, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;

    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Bundle b = arrayList.get(position).getData();


        String lsDisplayvalue = b.getString(Lovs.LOV_DISPLAYVALUE);

        viewHolder.tvCity.setText(lsDisplayvalue);

        if(lsDisplayvalue == "No match found...")
        {
            viewHolder.llCity.setBackgroundResource(R.color.White);
            viewHolder.llCity.setClickable(false);
        }
        else if (position % 2 == 0) {
            viewHolder.llCity.setBackgroundResource(R.color.alColor);

        } else {
            viewHolder.llCity.setBackgroundResource(R.color.alColor_1);
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public interface itemClickIterface {
        public void itemClick(int position);
    }

    public void setItemClickIterface(itemClickIterface mItemListener) {
        mItemClick = mItemListener;
    }


    public void swapData(ArrayList list) {
        if (arrayList == null) {
            arrayList = new ArrayList<LovOtherCityModel>();
        }
        arrayList = list;
        notifyDataSetChanged();

    }


}
