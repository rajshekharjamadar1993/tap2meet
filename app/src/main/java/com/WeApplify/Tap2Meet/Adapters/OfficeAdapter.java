package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Models.OfficeModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.Constants;

import java.util.ArrayList;

/**
 * Created by harshagulati on 08/06/17.
 */

public class OfficeAdapter extends SelectableAdapter<OfficeAdapter.ViewHolder> {

    private static final String TAG = "OfficeAdapter";
    static Context context;
    static OfficeAdapter adapter;
    static ArrayList<OfficeModel> arrayList;
    static Constants constants;
    static itemClickInterface mItemClick;
    static itemClickInterface1 mItemClick1;
    static Cursor cursor;
    static officeChanged mOfficeChanged;

    //


    public OfficeAdapter(Context context, ArrayList<OfficeModel> arrayList) {

        this.context = context;
        adapter = this;
        this.arrayList = arrayList;
        constants = new Constants(context);

    }

    public OfficeAdapter(Context context) {
        this.context = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvOffice;
       // RecyclerView rcvAmeneties;
        LinearLayout llOffice;
        ImageView ivCheck,ivUncheck;


        public ViewHolder(View view) {
            super(view);

            tvOffice = (TextView) view.findViewById(R.id.tvOffices);
            llOffice = (LinearLayout) view.findViewById(R.id.llOffice);
            ivCheck = (ImageView)view.findViewById(R.id.ivCheck);
            ivUncheck = (ImageView)view.findViewById(R.id.ivUncheck);


            llOffice.setOnClickListener(this);
            /*ivCheck.setOnClickListener(this);
            ivUncheck.setOnClickListener(this);
*/
        }

        @Override
        public void onClick(View v) {

            adapter.setMultiple(false);
            adapter.toggleSelection(getAdapterPosition());
            adapter.notifyItemChanged(getAdapterPosition());

            if (mItemClick != null) {
                mItemClick.itemClick(getAdapterPosition());
            }


        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.office_row, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;

    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Bundle b = arrayList.get(position).getData();

        long llOfficeID = b.getLong(Constants.MW_ID);
        String lsOffice = b.getString(Constants.MW_OFFICE);

        viewHolder.tvOffice.setText(lsOffice);

        if (lsOffice == "No office found...") {
            viewHolder.llOffice.setBackgroundResource(R.color.White);
            viewHolder.llOffice.setClickable(false);
        } /*else if (position % 2 == 0) {
            viewHolder.llOffice.setBackgroundResource(R.color.alColor);

        } else {
            viewHolder.llOffice.setBackgroundResource(R.color.alColor_1);
        }*/

        if (isSelected(position)) {

            viewHolder.llOffice.setBackgroundResource(R.color.LightGray);


            /*viewHolder.ivCheck.setVisibility(View.VISIBLE);
            viewHolder.ivUncheck.setVisibility(View.GONE);
            viewHolder.ivCheck.setColorFilter(R.color.app_color_red);
            viewHolder.ivCheck.setColorFilter(context.getResources().getColor(R.color.app_color_red));
*/

        } else {

            viewHolder.llOffice.setBackgroundResource(R.color.White);

            /*viewHolder.ivCheck.setVisibility(View.GONE);
            viewHolder.ivUncheck.setVisibility(View.VISIBLE);
            viewHolder.ivCheck.setColorFilter(R.color.GrayTextColor);
*/
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public interface itemClickInterface {
        public void itemClick(int position);
    }

    public void setItemClickIterface(itemClickInterface mItemListener) {
        mItemClick = mItemListener;
    }

    public interface itemClickInterface1 {
        public void itemClick1(int position);
    }

    public void setItemClickIterface1(itemClickInterface1 mItemListener) {
        mItemClick1 = mItemListener;
    }


    public void swapData(ArrayList list) {
        if (arrayList == null) {
            arrayList = new ArrayList<OfficeModel>();
        }
        arrayList = list;
        notifyDataSetChanged();

    }



    public interface officeChanged
    {
        public void getAmeneties(int position,long officeid);
    }

    public void setOfficeChangedListener(officeChanged listener)
    {
        mOfficeChanged = listener;
    }


}
