package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.Models.AttachmentModel;
import com.WeApplify.Tap2Meet.Models.ParticipantModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.MeetingPreread;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Admin on 29-03-2017.
 */

public class AttachmentAdapter extends SelectableAdapter<AttachmentAdapter.ViewHolder> {


    private static final String TAG = "AttachmentAdapter";
    static Context context;
    static AttachmentAdapter adapter;
    static ArrayList<AttachmentModel> arrayList;
    static itemClickIterface mItemClick;
    static Cursor cursor;
    //


    public AttachmentAdapter(Context context, ArrayList<AttachmentModel> arrayList ) {

        this.context = context;
        adapter = this;
        this.arrayList = arrayList;

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvAttachment;
        ImageView ivRemove;



        public ViewHolder(View view) {
            super(view);

            tvAttachment = (TextView) view.findViewById(R.id.tvAttachment);
            ivRemove = (ImageView) view.findViewById(R.id.ivRemove);

            ivRemove.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

            removeAt(getAdapterPosition());
        }

    }

    public void removeAt(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arrayList.size());
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.attachment_row, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;

    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder,final int position) {
        Bundle b = arrayList.get(position).getData();

        String lsName = b.getString(MeetingPreread.MPR_FILE);

        File file = new File (lsName);

        String lsFileName = file.getName();


        viewHolder.tvAttachment.setText(lsFileName);


        if(position % 2 == 0)
        {
            viewHolder.tvAttachment.setBackgroundResource(R.color.alColor);
            viewHolder.ivRemove.setBackgroundResource(R.color.alColor);

        }
        else
        {
            viewHolder.tvAttachment.setBackgroundResource(R.color.alColor_1);
            viewHolder.ivRemove.setBackgroundResource(R.color.alColor_1);
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    /*


      public interface dialerInterface
      {
          public void callDialer(int position);
      }

      public void setDialerListener(dialerInterface mListener)
      {
          mDialer = mListener;
      }
  */
    public interface itemClickIterface
    {
        public void itemClick(int position);
    }

    public void setItemClickIterface(itemClickIterface mItemListener)
    {
        mItemClick = mItemListener;
    }


    public void swapData(ArrayList list) {
        if (arrayList == null) {
            arrayList = new ArrayList<AttachmentModel>();
        }
        arrayList = list;
        notifyDataSetChanged();

    }



}


