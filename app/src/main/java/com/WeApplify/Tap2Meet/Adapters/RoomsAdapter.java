package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Models.AmenitiesModel;
import com.WeApplify.Tap2Meet.Models.RoomsModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.Lovs;

import java.util.ArrayList;

/**
 * Created by harshagulati on 08/06/17.
 */

public class RoomsAdapter extends SelectableAdapter<RoomsAdapter.ViewHolder> {

    private static final String TAG = "RoomsAdapter";
    static Context context;
    static RoomsAdapter adapter;
    static ArrayList<RoomsModel> arrayList;
    static Lovs lovs;
    static Cursor cursor;
    static officeChanged mOfficeChanged;
    LinearLayoutManager llm;
    ArrayList<AmenitiesModel> alAmenities;
    static itemClickInterface mItemClick;
    static itemClickInterface1 mItemClick1;


    public RoomsAdapter(Context context, ArrayList<RoomsModel> arrayList) {

        this.context = context;
        adapter = this;
        this.arrayList = arrayList;
        lovs = new Lovs(context);
        alAmenities = new ArrayList<>();

    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvCapacity, tvOffice, tvType,tvRoomName;
        RecyclerView rcvAmeneties;
        ImageView ivCheck,ivUncheck;
        CardView cvMain;
        LinearLayout llMain;
        //Button cbViewDetails;


        public ViewHolder(View view) {
            super(view);

            tvCapacity = (TextView) view.findViewById(R.id.tvCapacity);
            tvOffice = (TextView) view.findViewById(R.id.tvOffice);
            tvType = (TextView) view.findViewById(R.id.tvType);
            rcvAmeneties = (RecyclerView) view.findViewById(R.id.rcvOffAm);
            ivCheck = (ImageView)view.findViewById(R.id.ivCheck);
            ivUncheck = (ImageView)view.findViewById(R.id.ivUncheck);
            cvMain = (CardView)view.findViewById(R.id.cvMain);
            llMain =(LinearLayout)view.findViewById(R.id.llMain);
            tvRoomName =(TextView)view.findViewById(R.id.tvRoomName);

          //  ivCheck.setVisibility(View.GONE);

            cvMain.setOnClickListener(this);
            llMain.setOnClickListener(this);
            //ivCheck.setOnClickListener(this);
            //ivUncheck.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            adapter.setMultiple(false);
            adapter.toggleSelection(getAdapterPosition());
            adapter.notifyItemChanged(getAdapterPosition());


            if (mItemClick != null) {
                mItemClick.itemClick(getAdapterPosition());
            }

        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rooms_row, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;

    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Bundle b = arrayList.get(position).getData();
        ArrayList<AmenitiesModel> alAmenities = arrayList.get(position).getAmenities();

        String lsCapacity = b.getString(Constants.MW_CAPACITY);
        String lsType = b.getString(Constants.MW_TYPE);
        String lsOffice = b.getString(Constants.MW_OFFICE);
        String lsRoomName = b.getString(Constants.MW_NAME);

        int liId = b.getInt(Constants.MW_ID);

        viewHolder.tvCapacity.setText(lsCapacity);
        viewHolder.tvOffice.setText(lsOffice);
        //viewHolder.tvType.setText(lsType);
        viewHolder.tvRoomName.setText(lsRoomName +" :");



        if (arrayList.get(position).getAmenities() == null || arrayList.get(position).getAmenities().size() == 0) {
            mOfficeChanged.getAmeneties(position, liId);
            viewHolder.rcvAmeneties.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.rcvAmeneties.setVisibility(View.VISIBLE);
            llm = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            viewHolder.rcvAmeneties.setLayoutManager(llm);
            AmenitiesAdapter amenitiesAdapter = new AmenitiesAdapter(context, alAmenities);
            viewHolder.rcvAmeneties.setAdapter(amenitiesAdapter);
        }

       /* if(viewHolder.getAdapterPosition() == 0)
        {
            if(viewHolder.ivCheck.getVisibility() == View.GONE)
            {
                viewHolder.ivCheck.setVisibility(View.VISIBLE);
                viewHolder.ivUncheck.setVisibility(View.GONE);
            }
        }*/

        if (isSelected(position)) {
            viewHolder.cvMain.setBackgroundColor(context.getResources().getColor(R.color.LightGray));
            /*viewHolder.ivCheck.setVisibility(View.VISIBLE);
            viewHolder.ivUncheck.setVisibility(View.GONE);
            viewHolder.ivCheck.setColorFilter(R.color.app_color_red);
            viewHolder.ivCheck.setColorFilter(context.getResources().getColor(R.color.app_color_red));
*/

        } else {

          /*  if (mItemClick1 != null) {
                mItemClick1.itemClick1(viewHolder.getAdapterPosition());
            }
*/
            viewHolder.cvMain.setBackgroundColor(context.getResources().getColor(R.color.White));

            /*viewHolder.ivCheck.setVisibility(View.GONE);
            viewHolder.ivUncheck.setVisibility(View.VISIBLE);
            viewHolder.ivCheck.setColorFilter(R.color.White);
*/
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public interface officeChanged {
        public void getAmeneties(int position, int officeid);
    }

    public void setOfficeChangedListener(officeChanged listener) {
        mOfficeChanged = listener;
    }

    public interface itemClickInterface {
        public void itemClick(int position);
    }

    public void setItemClickIterface(itemClickInterface mItemListener) {
        mItemClick = mItemListener;
    }

    public interface itemClickInterface1 {
        public void itemClick1(int position);
    }

    public void setItemClickIterface1(itemClickInterface1 mItemListener) {
        mItemClick1 = mItemListener;
    }


    public void swapData(ArrayList list) {
        if (arrayList == null) {
            arrayList = new ArrayList<RoomsModel>();
        }
        arrayList = list;
        notifyDataSetChanged();

    }


}
