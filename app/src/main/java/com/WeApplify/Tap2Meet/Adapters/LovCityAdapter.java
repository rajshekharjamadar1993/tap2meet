package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Models.LovCityModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.Lovs;

import java.util.ArrayList;

/**
 * Created by harshagulati on 08/06/17.
 */

public class LovCityAdapter extends SelectableAdapter<LovCityAdapter.ViewHolder> {

    private static final String TAG = "ReportAdapter";
    static Context context;
    static LovCityAdapter adapter;
    static ArrayList<LovCityModel> arrayList;
    static Lovs lovs;
    static itemClickIterface mItemClick;
    static itemClickIterface1 mItemClick1;
    static Cursor cursor;
    //


    public LovCityAdapter(Context context, ArrayList<LovCityModel> arrayList) {

        this.context = context;
        adapter = this;
        this.arrayList = arrayList;
        lovs = new Lovs(context);

    }

    public LovCityAdapter(Context context) {
        this.context = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvCity;
        ImageView ivCity;
        LinearLayout llCity;


        public ViewHolder(View view) {
            super(view);

            tvCity = (TextView) view.findViewById(R.id.tvCity);
            ivCity = (ImageView) view.findViewById(R.id.ivCity);
            llCity = (LinearLayout) view.findViewById(R.id.llCity);

            llCity.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

            adapter.toggleSelection(getAdapterPosition());
            adapter.notifyItemChanged(getAdapterPosition());


        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.city_row, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;

    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Bundle b = arrayList.get(position).getData();


        String lsDisplayvalue = b.getString(Lovs.LOV_DISPLAYVALUE);

        viewHolder.tvCity.setText(lsDisplayvalue);



       if(lsDisplayvalue.equals("Mumbai"))
        {
            viewHolder.ivCity.setImageResource(R.drawable.mumbai);
        }
       else if(lsDisplayvalue.equals("Bangalore"))
       {
           viewHolder.ivCity.setImageResource(R.drawable.bangalore);
       }
        else if(lsDisplayvalue.equals("Hyderabad"))
        {
            viewHolder.ivCity.setImageResource(R.drawable.hyderabad);
        }
        else if(lsDisplayvalue.equals("Pune"))
       {
           viewHolder.ivCity.setImageResource(R.drawable.pune);
       }
       else if(lsDisplayvalue.equals("Noida"))
       {
           viewHolder.ivCity.setImageResource(R.drawable.delhi);
       }
       else if(lsDisplayvalue.equals("Chennai"))
       {
           viewHolder.ivCity.setImageResource(R.drawable.chennai);
       }
       else if(lsDisplayvalue.equals("Kolkata"))
       {
           viewHolder.ivCity.setImageResource(R.drawable.kolkata);
       }
        else
        {
            viewHolder.ivCity.setImageResource(R.drawable.location);
        }


        if (isSelected(position)) {
            viewHolder.ivCity.setBackgroundResource(R.drawable.circularshape1);
            viewHolder.ivCity.setColorFilter(context.getResources().getColor(R.color.White), PorterDuff.Mode.SRC_IN);

            if(mItemClick != null)
            {
                mItemClick.itemClick(viewHolder.getAdapterPosition());
            }

        }
        else
        {
            viewHolder.ivCity.setColorFilter(context.getResources().getColor(R.color.app_color_red), PorterDuff.Mode.SRC_IN);
            viewHolder.ivCity.setBackgroundResource(R.drawable.circularshape);


            if(mItemClick1 != null)
            {
                mItemClick1.itemClick1(viewHolder.getAdapterPosition());
              //  removeSelection(viewHolder.getAdapterPosition());
            }
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public interface itemClickIterface {
        public void itemClick(int position);
    }

    public void setItemClickInterface(itemClickIterface mItemListener) {
        mItemClick = mItemListener;
    }

    public interface itemClickIterface1 {
        public void itemClick1(int position);
    }

    public void setItemClickInterface1(itemClickIterface1 mItemListener) {
        mItemClick1 = mItemListener;
    }


    public void swapData(ArrayList list) {
        if (arrayList == null) {
            arrayList = new ArrayList<LovCityModel>();
        }
        arrayList = list;
        notifyDataSetChanged();

    }

}
