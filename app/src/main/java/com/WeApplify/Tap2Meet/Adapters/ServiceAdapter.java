package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Models.ServicesModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.Constants;

import java.util.ArrayList;

/**
 * Created by Admin on 04-07-2017.
 */

public class ServiceAdapter extends SelectableAdapter<ServiceAdapter.ViewHolder> {

    private static final String TAG = "ServiceAdapter";
    static Context context;
    static ServiceAdapter adapter;
    static ArrayList<ServicesModel> alService;
    static Constants constants;
    static Cursor cursor;
    static itemClickIterface mItemClick;

    public ServiceAdapter(Context context, ArrayList<ServicesModel> alService) {
        this.context = context;
        adapter = this;
        this.alService = alService;
        constants = new Constants(context);

    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvServices;
        ImageView ivUncheck, ivCheck;
        LinearLayout llServices;

        public ViewHolder(View view) {
            super(view);

            tvServices = (TextView) view.findViewById(R.id.tvServices);
        /*ivUncheck = (ImageView) view.findViewById(R.id.ivUncheck);
        ivCheck = (ImageView) view.findViewById(R.id.ivCheck);
        */
            llServices = (LinearLayout) view.findViewById(R.id.llServices);

        /*ivCheck.setVisibility(View.GONE);
        ivCheck.setOnClickListener(this);
        ivUncheck.setOnClickListener(this);
        */
            llServices.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            adapter.toggleSelection(getAdapterPosition());
            adapter.notifyItemChanged(getAdapterPosition());
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.services_row, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;

    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Bundle b = alService.get(position).getData();

        String lsService = b.getString(Constants.SV_DESC);

        viewHolder.tvServices.setText(lsService);

        if (isSelected(position)) {
            viewHolder.llServices.setBackgroundResource(R.color.LightGray);
        } else {
            viewHolder.llServices.setBackgroundResource(R.color.White);
        }


    }

    public interface itemClickIterface {
        public void itemClick(int position);
    }

    public void setItemClickInterface(itemClickIterface mItemListener) {
        mItemClick = mItemListener;
    }


    @Override
    public int getItemCount() {
        return alService.size();
    }

    public void swapData(ArrayList list) {
        if (alService == null) {
            alService = new ArrayList<ServicesModel>();
        }
        alService = list;
        notifyDataSetChanged();

    }


}