package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Models.Feedback_MeetingDone_Model;
import com.WeApplify.Tap2Meet.Models.NotificationModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.NotificationTable;

import java.util.ArrayList;

/**
 * Created by ADMIN on 04-12-2017.
 */

public class NotificationAdapter extends SelectableAdapter<NotificationAdapter.ViewHolder> {

    private static final String TAG = "Feedback_MeetingDone_Adapter";
    static Context context;
    static NotificationAdapter adapter;
    static ArrayList<NotificationModel> alNotificationModel;
    static Constants constants;
    static Cursor cursor;
    static Feedback_MeetingDone_Adapter.itemClickIterface mItemClick;
    //


    public NotificationAdapter(Context context, ArrayList<NotificationModel> alNotificationModel) {

        this.context = context;
        adapter = this;
        this.alNotificationModel = alNotificationModel;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvNotification;

        public ViewHolder(View view) {
            super(view);
            tvNotification = (TextView) view.findViewById(R.id.tvNotification);

            tvNotification.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.tvNotification:

                    if (mItemClick != null) {
                        mItemClick.itemClick(getAdapterPosition());
                    }

                    break;

            }
        }
    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_row, parent, false);
        NotificationAdapter.ViewHolder vh = new NotificationAdapter.ViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.ViewHolder viewHolder, final int position) {
        Bundle b = alNotificationModel.get(position).getData();

        String lsNotification = b.getString(NotificationTable.NOT_TEXT);


        viewHolder.tvNotification.setText(lsNotification);
    }


    @Override
    public int getItemCount() {
        return alNotificationModel.size();
    }

    public void swapData(ArrayList list) {
        if (alNotificationModel == null) {
            alNotificationModel = new ArrayList<NotificationModel>();
        }
        alNotificationModel = list;
        notifyDataSetChanged();

    }
    public interface itemClickIterface
    {
        public void itemClick(int position);
    }

    public void setItemClickIterface(Feedback_MeetingDone_Adapter.itemClickIterface mItemListener)
    {
        mItemClick = mItemListener;
    }
}
