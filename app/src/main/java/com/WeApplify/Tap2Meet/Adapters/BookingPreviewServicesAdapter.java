package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Models.AttachmentModel;
import com.WeApplify.Tap2Meet.Models.ServicesModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.MeetingPreread;

import java.util.ArrayList;



public class BookingPreviewServicesAdapter extends SelectableAdapter<BookingPreviewServicesAdapter.ViewHolder> {


    private static final String TAG = "BookingPreviewServicesAdapter";
    static Context context;
    static BookingPreviewServicesAdapter adapter;
    static ArrayList<ServicesModel> arrayList;
    static itemClickIterface mItemClick;
    static Cursor cursor;


    public BookingPreviewServicesAdapter(Context context, ArrayList<ServicesModel> arrayList ) {

        this.context = context;
        adapter = this;
        this.arrayList = arrayList;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvServices;



        public ViewHolder(View view) {
            super(view);

            tvServices = (TextView) view.findViewById(R.id.tvServices);


        }

        @Override
        public void onClick(View v) {

        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.booking_preview_services_row, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;

    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder,final int position) {
        Bundle b = arrayList.get(position).getData();

        String lsName = b.getString(Constants.SV_DESC);

        viewHolder.tvServices.setText(lsName);


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    /*


      public interface dialerInterface
      {
          public void callDialer(int position);
      }

      public void setDialerListener(dialerInterface mListener)
      {
          mDialer = mListener;
      }
  */
    public interface itemClickIterface
    {
        public void itemClick(int position);
    }

    public void setItemClickIterface(itemClickIterface mItemListener)
    {
        mItemClick = mItemListener;
    }


    public void swapData(ArrayList list) {
        if (arrayList == null) {
            arrayList = new ArrayList<ServicesModel>();
        }
        arrayList = list;
        notifyDataSetChanged();

    }



}


