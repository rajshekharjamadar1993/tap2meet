package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Models.UpComingMeeting_Model;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.UserLogin;
import com.WeApplify.Tap2Meet.Tables.WorkspaceBooked;

import java.util.ArrayList;

/**
 * Created by apple on 26/07/17.
 */

public class UpComingMeeting_Adapter extends SelectableAdapter<UpComingMeeting_Adapter.ViewHolder> {

    private static final String TAG = "UpComingMeeting_Adapter";
    static Context context;
    static UpComingMeeting_Adapter adapter;
    static ArrayList<UpComingMeeting_Model> alMeetings;
    static Constants constants;
    static Cursor cursor;
    static UpComingMeeting_Adapter.itemClickIterface mItemClick;
    //


    public UpComingMeeting_Adapter(Context context, ArrayList<UpComingMeeting_Model> alMeetings) {

        this.context = context;
        adapter = this;
        this.alMeetings = alMeetings;
        constants = new Constants(context);
    }
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName, tvTime,tvDate,tvRoomName,tvHostedBy;

        public ViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvTime = (TextView) view.findViewById(R.id.tvTime);
            tvDate =(TextView)view.findViewById(R.id.tvDate);
            tvRoomName =(TextView)view.findViewById(R.id.tvRoomName);
            tvHostedBy = (TextView)view.findViewById(R.id.tvHostedBy);

            tvName.setOnClickListener(this);
            tvTime.setOnClickListener(this);
            tvDate.setOnClickListener(this);
            tvRoomName.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.tvName:
                case R.id.tvTime:
                case R.id.tvDate:
                case R.id.tvRoomName:

                    if (mItemClick != null) {
                        mItemClick.itemClick(getAdapterPosition());
                    }

                    break;

            }
        }
    }

    @Override
    public UpComingMeeting_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.feedback_meetingdone_row, parent, false);
        UpComingMeeting_Adapter.ViewHolder vh = new UpComingMeeting_Adapter.ViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(UpComingMeeting_Adapter.ViewHolder viewHolder, final int position) {
        Bundle b = alMeetings.get(position).getData();

        String lsMeetingTitle = b.getString(Constants.MEETING_TITLE);
        String lsStartTime = b.getString(Constants.MEETING_STARTTIME);
        String lsEndTime = b.getString(Constants.MEETING_ENDTIME);
        String lsDate = b.getString(WorkspaceBooked.WB_MEETINGDATE);
        String lsRoomName =b.getString(Constants.MW_NAME);
        String lsEmail = b.getString(UserLogin.UL_EMAIL);
        String lsLoginUser = b.getString("Login_user");


        viewHolder.tvName.setText(lsMeetingTitle);
        viewHolder.tvTime.setText(lsStartTime + " to "+ lsEndTime);
        viewHolder.tvDate.setText(lsDate);
        viewHolder.tvRoomName.setText(lsRoomName);
        if(lsEmail.equals(lsLoginUser))
        {
            viewHolder.tvHostedBy.setText("You");
        }
        else
        {
            viewHolder.tvHostedBy.setText(lsEmail);
        }
    }


    @Override
    public int getItemCount() {
        return alMeetings.size();
    }

    public void swapData(ArrayList list) {
        if (alMeetings == null) {
            alMeetings = new ArrayList<UpComingMeeting_Model>();
        }
        alMeetings = list;
        notifyDataSetChanged();
    }
    public interface itemClickIterface
    {
        public void itemClick(int position);
    }

    public void setItemClickIterface(UpComingMeeting_Adapter.itemClickIterface mItemListener)
    {
        mItemClick = mItemListener;
    }
}
