package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Models.AmenitiesModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.Constants;

import java.util.ArrayList;

/**
 * Created by harshagulati on 08/06/17.
 */

public class AmenitiesAdapter extends SelectableAdapter<AmenitiesAdapter.ViewHolder> {

    private static final String TAG = "AmenitiesAdapter";
    static Context context;
    static AmenitiesAdapter adapter;
    static ArrayList<AmenitiesModel> arrayList;
    static Constants constants;
    static Cursor cursor;
    //


    public AmenitiesAdapter(Context context, ArrayList<AmenitiesModel> arrayList) {

        this.context = context;
        adapter = this;
        this.arrayList = arrayList;
        constants = new Constants(context);

    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvAmenities;
        ImageView ivAmenities;
        LinearLayout llAmenities;


        public ViewHolder(View view) {
            super(view);

            tvAmenities = (TextView) view.findViewById(R.id.tvAmenities);
            ivAmenities = (ImageView) view.findViewById(R.id.ivAmenities);
            llAmenities = (LinearLayout) view.findViewById(R.id.llAmenities);

        }

        @Override
        public void onClick(View v) {


        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.amenities_row, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Bundle b = arrayList.get(position).getData();

        String lsName = b.getString(Constants.AM_NAME);

        viewHolder.tvAmenities.setText(lsName);


        if(lsName.equals("LCD TV"))
        {
            viewHolder.ivAmenities.setImageResource(R.drawable.lcd);
        }
        else if(lsName.equals("LED TV"))
        {
            viewHolder.ivAmenities.setImageResource(R.drawable.lcd);
        }
        else if(lsName.equals("Wifi"))
        {
            viewHolder.ivAmenities.setImageResource(R.drawable.wifi);
        }
        else if(lsName.equals("Projector"))
        {
            viewHolder.ivAmenities.setImageResource(R.drawable.projector);
        }
        else if(lsName.equals("Printer"))
        {
            viewHolder.ivAmenities.setImageResource(R.drawable.printer);
        }
        else
        {
            viewHolder.ivAmenities.setImageResource(R.drawable.wifi);
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }




    public void swapData(ArrayList list) {
        if (arrayList == null) {
            arrayList = new ArrayList<AmenitiesModel>();
        }
        arrayList = list;
        notifyDataSetChanged();

    }


}
