package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Models.ParticipantsList_Model;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.UserLogin;

import java.util.ArrayList;

/**
 * Created by apple on 07/09/17.c
 */

public class ParticipantsList_Adapter extends SelectableAdapter<ParticipantsList_Adapter.ViewHolder> {

private static final String TAG = "ParticipantsList_Adapter";
static Context context;
static ParticipantsList_Adapter adapter;
static ArrayList<ParticipantsList_Model> arrayList;
static Constants constants;
static Cursor cursor;
static itemClickIterface mItemClick;


public ParticipantsList_Adapter(Context context, ArrayList<ParticipantsList_Model> arrayList) {

        this.context = context;
        adapter = this;
        this.arrayList = arrayList;
        constants = new Constants(context);

        }

public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView tvSearchData;

    public ViewHolder(View view) {
        super(view);

        tvSearchData = (TextView) view.findViewById(R.id.tvSearchData);
       tvSearchData.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        tvSearchData.setTextColor(context.getResources().getColor(R.color.White));
        if (mItemClick != null) {
            mItemClick.itemClick(getAdapterPosition());
        }

    }

}

    @Override
    public ParticipantsList_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.participantlist_row, parent, false);
        ParticipantsList_Adapter.ViewHolder vh = new ParticipantsList_Adapter.ViewHolder(itemView);
        return vh;
    }


    @Override
    public void onBindViewHolder(ParticipantsList_Adapter.ViewHolder viewHolder, final int position) {
        Bundle b = arrayList.get(position).getData();

        String lsParticipants = b.getString(UserLogin.UL_EMAIL);
        viewHolder.tvSearchData.setText(lsParticipants);


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


public interface itemClickIterface {
    public void itemClick(int position);
}

    public void setItemClickInterface(ParticipantsList_Adapter.itemClickIterface mItemListener) {
        mItemClick = mItemListener;
    }

    public void swapData(ArrayList list) {
        if (arrayList == null) {
            arrayList = new ArrayList<ParticipantsList_Model>();
        }
        arrayList = list;
        notifyDataSetChanged();
    }


}

