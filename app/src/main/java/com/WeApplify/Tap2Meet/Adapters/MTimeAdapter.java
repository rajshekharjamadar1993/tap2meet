package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.WeApplify.Tap2Meet.Models.ETimeModel;
import com.WeApplify.Tap2Meet.Models.MTimeModel;
import com.WeApplify.Tap2Meet.Models.NTimeModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.CustomToast;
import com.WeApplify.Tap2Meet.Tables.Constants;

import java.util.ArrayList;

/**
 * Created by harshagulati on 08/06/17.
 */

public class MTimeAdapter extends SelectableAdapter<MTimeAdapter.ViewHolder> {

    private static final String TAG = "MTimeAdapter";
    static Context context;
    static MTimeAdapter adapter;
    static ArrayList<MTimeModel> arrayList;
    static Constants constants;
    static Cursor cursor;
    static itemClickIterface mItemClick;
    static itemClickIterface2 mItemClick2;
    static int a, aa = 0;
    public static final int MORNING = 0;
    public static final int NOON = 1;
    public static final int EVENING = 2;


    public MTimeAdapter(Context context, ArrayList<MTimeModel> arrayList) {

        this.context = context;
        adapter = this;
        this.arrayList = arrayList;

        constants = new Constants(context);

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTime;
        CardView cvTime;
        LinearLayout llTime;

        public ViewHolder(View view) {
            super(view);

            tvTime = (TextView) view.findViewById(R.id.tvTime);
            cvTime = (CardView) view.findViewById(R.id.cvTime);
            llTime = (LinearLayout) view.findViewById(R.id.llTime);

            tvTime.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (mItemClick != null) {
                mItemClick.itemClick(getAdapterPosition());
            }

        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        /*if (viewType == MORNING) {*/
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_row, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;
     /*   }
        else if(viewType == NOON) {
            final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_noon_row, parent, false);
            ViewHolder vh = new ViewHolder(itemView);
            return vh;

        }
        else
        {
            final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_eve_row, parent, false);
            ViewHolder vh = new ViewHolder(itemView);
            return vh;
        }*/
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        Bundle b = arrayList.get(position).getData();
        String lsName = b.getString(Constants.TIME_TEXT);
        boolean lbBooked = b.getBoolean(Constants.TIME_BOOKED);

        viewHolder.tvTime.setText(lsName);

        if (lbBooked) {
            viewHolder.tvTime.setBackgroundColor(context.getResources().getColor(R.color.LightGray));
            viewHolder.tvTime.setTextColor(context.getResources().getColor(R.color.GrayTextColor));

            viewHolder.tvTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "slot already booked..choose another one", Toast.LENGTH_SHORT).show();
                }
            });
        }
       /* else if(lbBooked1)
        {
            viewHolder.tvTime.setBackgroundColor(context.getResources().getColor(R.color.app_color_red));
            viewHolder.tvTime.setTextColor(context.getResources().getColor(R.color.White));
        }*/
        else {
            viewHolder.tvTime.setBackgroundColor(context.getResources().getColor(R.color.gradienthome_end));
            viewHolder.tvTime.setTextColor(context.getResources().getColor(R.color.GrayTextColor));
        }


        if (isSelected(position)) {
            viewHolder.cvTime.setCardElevation(4);
            viewHolder.tvTime.setBackgroundResource(R.drawable.border);
            viewHolder.tvTime.setBackgroundColor(context.getResources().getColor(R.color.app_color_red));
            viewHolder.tvTime.setTextColor(context.getResources().getColor(R.color.White));
        }
      /*  else
        {
            viewHolder.tvTime.setBackgroundColor(context.getResources().getColor(R.color.LightGray));
            viewHolder.tvTime.setTextColor(context.getResources().getColor(R.color.GrayTextColor));

        }*/
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public interface itemClickIterface {
        public void itemClick(int position);
    }

    public void setItemClickInterface(itemClickIterface mItemListener) {
        mItemClick = mItemListener;
    }

    public interface itemClickIterface2 {
        public void itemClick2(int position);
    }

    public void setItemClickInterface2(itemClickIterface2 mItemListener2) {
        mItemClick2 = mItemListener2;
    }

    public void swapData(ArrayList list) {
        if (arrayList == null) {
            arrayList = new ArrayList<MTimeModel>();
        }
        arrayList = list;
        notifyDataSetChanged();
    }


}
