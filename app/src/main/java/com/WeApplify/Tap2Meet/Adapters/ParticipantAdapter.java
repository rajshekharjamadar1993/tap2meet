package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.Models.ParticipantModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.MeetingParticipants;
import com.WeApplify.Tap2Meet.Tables.UserLogin;

import java.util.ArrayList;

/**
 * Created by Admin on 29-03-2017.
 */

public class ParticipantAdapter extends SelectableAdapter<ParticipantAdapter.ViewHolder> {


    private static final String TAG = "OrderHistoryAdapter";
    static Context context;
    static ParticipantAdapter adapter;
    static ArrayList<ParticipantModel> arrayList;
    static itemClickIterface mItemClick;
    static Cursor cursor;
    String lsEmail;
    SharedPreferences sp;
    //


    public ParticipantAdapter(Context context, ArrayList<ParticipantModel> arrayList ) {

        this.context = context;
        adapter = this;
        this.arrayList = arrayList;

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvParticipant;
        ImageView ivRemove;




        public ViewHolder(View view) {
            super(view);



            tvParticipant = (TextView) view.findViewById(R.id.tvParticipant);
            ivRemove = (ImageView) view.findViewById(R.id.ivRemove);

            ivRemove.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            removeAt(getAdapterPosition());

        }

    }

    public void removeAt(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arrayList.size());
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.participant_row, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;

    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder,final int position) {
        Bundle b = arrayList.get(position).getData();

        String lsName = b.getString(MeetingParticipants.MP_EMPLOYEEEMAIL);

        viewHolder.tvParticipant.setText(lsName);

      //  sp = getSharedPreferences(App.LOGINCREDENTIALS, context.MODE_PRIVATE);
        sp = context.getApplicationContext().getSharedPreferences(App.LOGINCREDENTIALS, context.MODE_PRIVATE);
        lsEmail = sp.getString(UserLogin.UL_EMAIL, "");


        if(lsName.equals(lsEmail))
        {
            viewHolder.ivRemove.setVisibility(View.GONE);
        }
        else
        {
            viewHolder.ivRemove.setVisibility(View.VISIBLE);
        }

        if(position % 2 == 0)
        {
            viewHolder.tvParticipant.setBackgroundResource(R.color.alColor);
            viewHolder.ivRemove.setBackgroundResource(R.color.alColor);

        }
        else
        {
            viewHolder.tvParticipant.setBackgroundResource(R.color.alColor_1);
            viewHolder.ivRemove.setBackgroundResource(R.color.alColor_1);
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



  public interface itemClickIterface
  {
      public void itemClick(int position);
  }

    public void setItemClickIterface(itemClickIterface mItemListener)
    {
        mItemClick = mItemListener;
    }


    public void swapData(ArrayList list) {
        if (arrayList == null) {
            arrayList = new ArrayList<ParticipantModel>();
        }
        arrayList = list;
        notifyDataSetChanged();

    }



}


