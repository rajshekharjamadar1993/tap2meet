package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Models.TodayMeeting_Model;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.UserLogin;

import java.util.ArrayList;

/**
 * Created by apple on 04/09/17.
 */

public class TodayMeeting_Adapter extends SelectableAdapter<TodayMeeting_Adapter.ViewHolder> {

    private static final String TAG = "TodayMeeting_Adapter";
    static Context context;
    static TodayMeeting_Adapter adapter;
    static ArrayList<TodayMeeting_Model> alMeetings;
    static Constants constants;
    static Cursor cursor;
    static TodayMeeting_Adapter.itemClickIterface mItemClick;
    //


    public TodayMeeting_Adapter(Context context, ArrayList<TodayMeeting_Model> alMeetings) {

        this.context = context;
        adapter = this;
        this.alMeetings = alMeetings;
        constants = new Constants(context);

    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName, tvTime,tvDate,tvTo,tvBullet,tvRoomName,tvHostedBy;
        LinearLayout llDate;

        public ViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvTime = (TextView) view.findViewById(R.id.tvTime);
            tvDate =(TextView)view.findViewById(R.id.tvDate);
            tvRoomName =(TextView)view.findViewById(R.id.tvRoomName);
            tvHostedBy = (TextView)view.findViewById(R.id.tvHostedBy);


            tvName.setOnClickListener(this);
            tvTime.setOnClickListener(this);
            tvRoomName.setOnClickListener(this);

            tvDate.setVisibility(View.GONE);

        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.tvName:
                case R.id.tvEndTime:
                case R.id.tvStartTime:

                    if (mItemClick != null) {
                        mItemClick.itemClick(getAdapterPosition());
                    }

                    break;

            }
        }
    }

    @Override
    public TodayMeeting_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.feedback_meetingdone_row, parent, false);
        TodayMeeting_Adapter.ViewHolder vh = new TodayMeeting_Adapter.ViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(TodayMeeting_Adapter.ViewHolder viewHolder, final int position) {
        Bundle b = alMeetings.get(position).getData();

        String lsMeetingTitle = b.getString(Constants.MEETING_TITLE);
        String lsStartTime = b.getString(Constants.MEETING_STARTTIME);
        String lsEndTime = b.getString(Constants.MEETING_ENDTIME);
        String lsRoomName =b.getString(Constants.MW_NAME);
        String lsLoginUser = b.getString("Login_user");
        String lsEmail = b.getString(UserLogin.UL_EMAIL);


        viewHolder.tvName.setText(lsMeetingTitle);
        viewHolder.tvTime.setText(lsStartTime + " to "+ lsEndTime);
        viewHolder.tvRoomName.setText(lsRoomName);
       // viewHolder.tvDate.setText("Today");
        if(lsEmail.equals(lsLoginUser))
        {
            viewHolder.tvHostedBy.setText("You");
        }
        else
        {
            viewHolder.tvHostedBy.setText(lsEmail);
        }

    }


    @Override
    public int getItemCount() {
        return alMeetings.size();
    }

    public void swapData(ArrayList list) {
        if (alMeetings == null) {
            alMeetings = new ArrayList<TodayMeeting_Model>();
        }
        alMeetings = list;
        notifyDataSetChanged();

    }
    public interface itemClickIterface
    {
        public void itemClick(int position);
    }

    public void setItemClickIterface(TodayMeeting_Adapter.itemClickIterface mItemListener)
    {
        mItemClick = mItemListener;
    }
}

