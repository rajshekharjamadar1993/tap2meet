package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Models.MTimeModel;
import com.WeApplify.Tap2Meet.Models.NTimeModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.Constants;

import java.util.ArrayList;

/**
 * Created by harshagulati on 08/06/17.
 */

public class NTimeAdapter extends SelectableAdapter<NTimeAdapter.ViewHolder> {

    private static final String TAG = "NTimeAdapter";
    static Context context;
    static NTimeAdapter adapter;
    static ArrayList<NTimeModel> arrayList;
    static Constants constants;
    static Cursor cursor;
    static itemClickIterface mItemClick;


    public NTimeAdapter(Context context, ArrayList<NTimeModel> arrayList) {

        this.context = context;
        adapter = this;
        this.arrayList = arrayList;
        constants = new Constants(context);

    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTime;
        CardView cvTime;
        LinearLayout llTime;

        public ViewHolder(View view) {
            super(view);

            tvTime = (TextView) view.findViewById(R.id.tvTime);
            cvTime = (CardView) view.findViewById(R.id.cvTime);
            llTime = (LinearLayout) view.findViewById(R.id.llTime);

            tvTime.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

          /*  cvTime.setCardElevation(4);
            tvTime.setBackgroundResource(R.drawable.border);
            tvTime.setBackgroundColor(context.getResources().getColor(R.color.app_color_red));
            tvTime.setTextColor(context.getResources().getColor(R.color.White));*/
            if (mItemClick != null) {
                mItemClick.itemClick(getAdapterPosition());
            }

        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_row, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;

    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Bundle b = arrayList.get(position).getData();


        String lsName = b.getString(Constants.TIME_TEXT);

        viewHolder.tvTime.setText(lsName);

        if (isSelected(position)) {
            viewHolder.cvTime.setCardElevation(4);
            viewHolder.tvTime.setBackgroundResource(R.drawable.border);
            viewHolder.tvTime.setBackgroundColor(context.getResources().getColor(R.color.app_color_red));
            viewHolder.tvTime.setTextColor(context.getResources().getColor(R.color.White));

        }
        else
        {
            viewHolder.tvTime.setBackgroundColor(context.getResources().getColor(R.color.LightGray));
            viewHolder.tvTime.setTextColor(context.getResources().getColor(R.color.GrayTextColor));

        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public interface itemClickIterface {
        public void itemClick(int position);
    }

    public void setItemClickInterface(itemClickIterface mItemListener) {
        mItemClick = mItemListener;
    }

    public void swapData(ArrayList list) {
        if (arrayList == null) {
            arrayList = new ArrayList<NTimeModel>();
        }
        arrayList = list;
        notifyDataSetChanged();
    }


}
