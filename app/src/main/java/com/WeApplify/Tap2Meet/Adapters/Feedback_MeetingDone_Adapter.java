package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Models.Feedback_MeetingDone_Model;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.Constants;

import java.util.ArrayList;

/**
 * Created by Admin on 21-07-17.
 */

public class Feedback_MeetingDone_Adapter extends SelectableAdapter<Feedback_MeetingDone_Adapter.ViewHolder> {

    private static final String TAG = "Feedback_MeetingDone_Adapter";
    static Context context;
    static Feedback_MeetingDone_Adapter adapter;
    static ArrayList<Feedback_MeetingDone_Model> alFeedback_MeetingDone;
    static Constants constants;
    static Cursor cursor;
    static Feedback_MeetingDone_Adapter.itemClickIterface mItemClick;
    //


    public Feedback_MeetingDone_Adapter(Context context, ArrayList<Feedback_MeetingDone_Model> alFeedback_MeetingDone) {

        this.context = context;
        adapter = this;
        this.alFeedback_MeetingDone = alFeedback_MeetingDone;
        constants = new Constants(context);

    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvName, tvStartTime, tvEndTime;

        public ViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvEndTime = (TextView) view.findViewById(R.id.tvEndTime);
            tvStartTime = (TextView) view.findViewById(R.id.tvStartTime);

            tvName.setOnClickListener(this);
            tvEndTime.setOnClickListener(this);
            tvStartTime.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.tvName:
                case R.id.tvEndTime:
                case R.id.tvStartTime:

                    if (mItemClick != null) {
                        mItemClick.itemClick(getAdapterPosition());
                    }

                    break;

            }
        }
    }

    @Override
    public Feedback_MeetingDone_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.feedback_meetingdone_row, parent, false);
        Feedback_MeetingDone_Adapter.ViewHolder vh = new Feedback_MeetingDone_Adapter.ViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(Feedback_MeetingDone_Adapter.ViewHolder viewHolder, final int position) {
        Bundle b = alFeedback_MeetingDone.get(position).getData();

        String lsMeetingTitle = b.getString(Constants.MEETING_TITLE);
        String lsStartTime = b.getString(Constants.MEETING_STARTTIME);
        String lsEndTime = b.getString(Constants.MEETING_ENDTIME);

        viewHolder.tvName.setText(lsMeetingTitle);
        viewHolder.tvStartTime.setText(lsStartTime);
        viewHolder.tvEndTime.setText(lsEndTime);


    }


    @Override
    public int getItemCount() {
        return alFeedback_MeetingDone.size();
    }

    public void swapData(ArrayList list) {
        if (alFeedback_MeetingDone == null) {
            alFeedback_MeetingDone = new ArrayList<Feedback_MeetingDone_Model>();
        }
        alFeedback_MeetingDone = list;
        notifyDataSetChanged();

    }
    public interface itemClickIterface
    {
        public void itemClick(int position);
    }

    public void setItemClickIterface(itemClickIterface mItemListener)
    {
        mItemClick = mItemListener;
    }
}
