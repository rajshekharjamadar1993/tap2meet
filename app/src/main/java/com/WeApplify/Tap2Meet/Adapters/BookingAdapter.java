package com.WeApplify.Tap2Meet.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.Models.AttachmentModel;
import com.WeApplify.Tap2Meet.Models.BookingModel;
import com.WeApplify.Tap2Meet.Models.ParticipantModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.UserLogin;
import com.WeApplify.Tap2Meet.Tables.WorkspaceBooked;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Admin on 29-03-2017.
 */

public class BookingAdapter extends SelectableAdapter<BookingAdapter.ViewHolder> {


    private static final String TAG = "BookingAdapter";
    static Context context;
    static BookingAdapter adapter;
    static ArrayList<BookingModel> arrayList;
    static itemClickIterface mItemClick;
    static itemDeleteInterface mItemDelete;
    static itemEditInterface mItemEdit;
    static Cursor cursor;


    public BookingAdapter(Context context, ArrayList<BookingModel> arrayList) {

        this.context = context;
        adapter = this;
        this.arrayList = arrayList;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvName, tvStartTime, tvEndTime, tvBullet;
        CardView cvBookings;
        ImageView ivBookings,ivCancel,ivReschedule;
        LinearLayout llBookings, llTime, llButtons;
        FrameLayout cbCancel, cbReschedule;



        public ViewHolder(View view) {
            super(view);

            tvName = (TextView) view.findViewById(R.id.tvName);
            tvStartTime = (TextView) view.findViewById(R.id.tvStartTime);
            tvEndTime = (TextView) view.findViewById(R.id.tvEndTime);
            cvBookings = (CardView) view.findViewById(R.id.cvBookings);
            llBookings = (LinearLayout) view.findViewById(R.id.llBookings);
            //llButtons = (LinearLayout) view.findViewById(R.id.llButtons);


            ivBookings = (ImageView) view.findViewById(R.id.ivBookings);
            llTime = (LinearLayout) view.findViewById(R.id.llTime);
            tvBullet = (TextView) view.findViewById(R.id.tvBullet);

            cbCancel = (FrameLayout) view.findViewById(R.id.cbCancel);
            cbReschedule = (FrameLayout) view.findViewById(R.id.cbReschedule);

            ivCancel =(ImageView)view.findViewById(R.id.ivCancel);
            ivReschedule =(ImageView)view.findViewById(R.id.ivReschedule);


            tvName.setOnClickListener(this);
            llTime.setOnClickListener(this);
            ivBookings.setOnClickListener(this);
            cbCancel.setOnClickListener(this);
            cbReschedule.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.tvName:
                case R.id.ivBookings:
                case R.id.llTime:
                    if (mItemClick != null) {
                        mItemClick.itemClick(getAdapterPosition());
                    }
                    break;

                case R.id.cbCancel:
            if (mItemDelete != null) {
                        mItemDelete.itemDelete(getAdapterPosition());
                    }
                    break;

                case R.id.cbReschedule:
                    if (mItemEdit != null) {
                        mItemEdit.itemEdit(getAdapterPosition());
                    }
                    break;


            }

        }

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.booking_row, parent, false);
        ViewHolder vh = new ViewHolder(itemView);
        return vh;

    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Bundle b = arrayList.get(position).getData();

        String lsName = b.getString(WorkspaceBooked.WB_WORKSPACEID);
        String lsStartTime = b.getString(WorkspaceBooked.WB_PLANNEDSTARTTIME);
        String lsEndTime = b.getString(WorkspaceBooked.WB_PLANNEDENDTIME);
        String lsTitle = b.getString(WorkspaceBooked.WB_TITLE);
        String lsBookingStatus = b.getString(WorkspaceBooked.WB_BOOKING_STATUS);
        String lsMeetingDate = b.getString(WorkspaceBooked.WB_MEETINGDATE);
        String lsLoggedInUserEmail = b.getString("LoggedInUserEmail");
        String lsBookedEmail = b.getString(UserLogin.UL_EMAIL);

        if (lsTitle.equals("No Meetings scheduled")) {
            viewHolder.ivBookings.setVisibility(View.GONE);
            viewHolder.llTime.setVisibility(View.GONE);
            viewHolder.tvBullet.setVisibility(View.GONE);
            viewHolder.cbCancel.setVisibility(View.GONE);
            viewHolder.cbReschedule.setVisibility(View.GONE);
        } else if (lsBookingStatus.equals("C")) {
            viewHolder.cbCancel.setEnabled(false);
            viewHolder.cbCancel.setBackgroundColor(context.getResources().getColor(R.color.Gray));
            viewHolder.cbReschedule.setVisibility(View.GONE);

            viewHolder.ivBookings.setVisibility(View.VISIBLE);
            viewHolder.llTime.setVisibility(View.VISIBLE);
            viewHolder.tvBullet.setVisibility(View.VISIBLE);
        } else {

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try {
                if(sdf.parse(lsMeetingDate).before(sdf.parse(App.getDate())))
                {
                    viewHolder.cbCancel.setVisibility(View.INVISIBLE);
                    viewHolder.ivBookings.setVisibility(View.VISIBLE);
                    viewHolder.llTime.setVisibility(View.VISIBLE);
                    viewHolder.tvBullet.setVisibility(View.VISIBLE);
                }
                else
                {
                    if(lsLoggedInUserEmail.equals(lsBookedEmail))
                    {
                        viewHolder.cbCancel.setEnabled(true);
                        viewHolder.cbReschedule.setVisibility(View.VISIBLE);
                        viewHolder.ivBookings.setVisibility(View.VISIBLE);
                        viewHolder.llTime.setVisibility(View.VISIBLE);
                        viewHolder.tvBullet.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        viewHolder.ivReschedule.setColorFilter(context.getResources().getColor(R.color.Gray));
                        viewHolder.ivCancel.setColorFilter(context.getResources().getColor(R.color.Gray));
                    }
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        viewHolder.tvName.setText(lsTitle);
        viewHolder.tvStartTime.setText(lsStartTime);
        viewHolder.tvEndTime.setText(lsEndTime);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public interface itemClickIterface {
        public void itemClick(int position);
    }

    public void setItemClickIterface(itemClickIterface mItemListener) {
        mItemClick = mItemListener;
    }


    public interface itemDeleteInterface {
        public void itemDelete(int position);
    }

    public void setItemDeleteIterface(itemDeleteInterface mItemListener) {
        mItemDelete = mItemListener;
    }

    public interface itemEditInterface {
        public void itemEdit(int position);
    }

    public void setItemEditIterface(itemEditInterface mItemListener) {
        mItemEdit = mItemListener;
    }

    public void swapData(ArrayList list) {
        if (arrayList == null) {
            arrayList = new ArrayList<BookingModel>();
        }
        arrayList = list;
        notifyDataSetChanged();

    }


}


