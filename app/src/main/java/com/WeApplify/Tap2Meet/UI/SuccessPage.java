package com.WeApplify.Tap2Meet.UI;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.RegisterForPushNotificationsAsync;
import com.WeApplify.Tap2Meet.Tables.UserLogin;

import me.pushy.sdk.Pushy;

/**
 * Created by apple on 01/09/17.
 */

public class SuccessPage extends Activity {

    Context context;
    Activity activity;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.successpage);

        context = this;
        activity = this;


        final ImageView iv = (ImageView) findViewById(R.id.ivSuccess);
        final Animation an = AnimationUtils.loadAnimation(getBaseContext(), R.anim.zoom_in);
        final Animation an2 = AnimationUtils.loadAnimation(getBaseContext(), R.anim.abc_fade_out);



       iv.startAnimation(an);
        an.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

           @Override
            public void onAnimationEnd(Animation animation) {
                iv.startAnimation(an2);
                finish();
               Intent intent = new Intent(context,Home.class);
               startActivity(intent);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }
}
