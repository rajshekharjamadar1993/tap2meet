package com.WeApplify.Tap2Meet.UI;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.LinearLayout;

import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.R;

/**
 * Created by Admin on 08-06-2017.
 */

public class Workspace extends BaseActivity implements View.OnClickListener {

    public static final String TAG = "Workspace";
    Activity activity;
    Context context;
    AppCompatImageView ivMeetingHall, ivTrainingHall, ivConferenceHall, ivBoardRoom,ivMeetingHallPressed,ivTrainingHallPressed,ivConferenceHallPressed,ivBoardRoomPressed;
    LinearLayout llMeetingRoom, llConferenceHall, llBoardRoom, llTrainingHall;
    String Hall;
    boolean change=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.workspace);
        setTitle("Workspace");

        activity = this;
        context = this;

        initlayout();

        //cbNext.setOnClickListener(this);
        llMeetingRoom.setOnClickListener(this);
        llConferenceHall.setOnClickListener(this);
        llTrainingHall.setOnClickListener(this);
        llBoardRoom.setOnClickListener(this);

    }

    public void initlayout() {
        ivMeetingHall = (AppCompatImageView) findViewById(R.id.ivMeetingHall);
        ivConferenceHall = (AppCompatImageView) findViewById(R.id.ivConferenceHall);
        ivBoardRoom = (AppCompatImageView) findViewById(R.id.ivBoardRoom);
        ivTrainingHall = (AppCompatImageView) findViewById(R.id.ivTrainingHall);

        ivMeetingHallPressed = (AppCompatImageView) findViewById(R.id.ivMeetingHallPressed);
        ivConferenceHallPressed = (AppCompatImageView) findViewById(R.id.ivConferenceHallPressed);
        ivBoardRoomPressed = (AppCompatImageView) findViewById(R.id.ivBoardRoomPressed);
        ivTrainingHallPressed = (AppCompatImageView) findViewById(R.id.ivTrainingHallPressed);

        //cbNext = (Button) findViewById(R.id.cbNext);
        llMeetingRoom = (LinearLayout) findViewById(R.id.llMeetingRoom);
        llConferenceHall = (LinearLayout) findViewById(R.id.llConferenceHall);
        llBoardRoom = (LinearLayout) findViewById(R.id.llBoardRoom);
        llTrainingHall = (LinearLayout) findViewById(R.id.llTrainingHall);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
           /* case R.id.cbNext:
                Intent i = new Intent(this, Filter.class);
                startActivity(i);
                break;*/

            case R.id.llMeetingRoom:
                DefaultColor();
                ivMeetingHallPressed.setVisibility(View.VISIBLE);
                ivMeetingHall.setVisibility(View.GONE);
                break;

            case R.id.llConferenceHall:
                DefaultColor();
                ivConferenceHallPressed.setVisibility(View.VISIBLE);
                ivConferenceHall.setVisibility(View.GONE);
                break;

            case R.id.llBoardRoom:
                DefaultColor();
                ivBoardRoom.setVisibility(View.GONE);
                ivBoardRoomPressed.setVisibility(View.VISIBLE);
                break;

            case R.id.llTrainingHall:
                DefaultColor();
                ivTrainingHallPressed.setVisibility(View.VISIBLE);
                ivTrainingHall.setVisibility(View.GONE);
                break;
        }

    }

    public void DefaultColor() {

        ivMeetingHall.setVisibility(View.VISIBLE);
        ivConferenceHall.setVisibility(View.VISIBLE);
        ivBoardRoom.setVisibility(View.VISIBLE);
        ivTrainingHall.setVisibility(View.VISIBLE);
        ivConferenceHallPressed.setVisibility(View.GONE);
        ivMeetingHallPressed.setVisibility(View.GONE);
        ivTrainingHallPressed.setVisibility(View.GONE);
        ivBoardRoomPressed.setVisibility(View.GONE);

       /*AppCompatImageView iv = null;
       ivMeetingHall.setBackgroundTintList(this.getResources().getColorStateList(R.color.White));
       ivConferenceHall.setBackgroundTintList(this.getResources().getColorStateList(R.color.White));
       ivBoardRoom.setBackgroundTintList(this.getResources().getColorStateList(R.color.White));
       ivTrainingHall.setBackgroundTintList(this.getResources().getColorStateList(R.color.White));

       ivMeetingHall.setBackground(getDrawable(R.drawable.circularshape));
       ivConferenceHall.setBackground(getDrawable(R.drawable.circularshape));
       ivTrainingHall.setBackground(getDrawable(R.drawable.circularshape));
       ivBoardRoom.setBackground(getDrawable(R.drawable.circularshape));

       ivMeetingHall.setImageTintList(this.getResources().getColorStateList(R.color.app_color_red));
       ivConferenceHall.setImageTintList(this.getResources().getColorStateList(R.color.app_color_red));
       ivTrainingHall.setImageTintList(this.getResources().getColorStateList(R.color.app_color_red));
       ivBoardRoom.setImageTintList(this.getResources().getColorStateList(R.color.app_color_red));
*/
    }
}

