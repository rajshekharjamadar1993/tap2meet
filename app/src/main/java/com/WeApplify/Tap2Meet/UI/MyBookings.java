package com.WeApplify.Tap2Meet.UI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.WeApplify.Tap2Meet.Adapters.BookingAdapter;
import com.WeApplify.Tap2Meet.Adapters.OfficeAdapter;
import com.WeApplify.Tap2Meet.Adapters.RoomsAdapter;
import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.Models.AttachmentModel;
import com.WeApplify.Tap2Meet.Models.BookingModel;
import com.WeApplify.Tap2Meet.Models.DateFromNotificationModel;
import com.WeApplify.Tap2Meet.Models.LovCityModel;
import com.WeApplify.Tap2Meet.Models.OfficeModel;
import com.WeApplify.Tap2Meet.Models.ParticipantModel;
import com.WeApplify.Tap2Meet.Models.ServicesModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.CustomDialog;
import com.WeApplify.Tap2Meet.Services.CustomServices;
import com.WeApplify.Tap2Meet.Services.CustomToast;
import com.WeApplify.Tap2Meet.Services.DataTransmitter;
import com.WeApplify.Tap2Meet.Services.Debugger;
import com.WeApplify.Tap2Meet.Services.PushyAPI;
import com.WeApplify.Tap2Meet.Services.ShowProgressDialog;
import com.WeApplify.Tap2Meet.Services.TransperantProgressDialog;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.Lovs;
import com.WeApplify.Tap2Meet.Tables.MeetingParticipants;
import com.WeApplify.Tap2Meet.Tables.MeetingPreread;
import com.WeApplify.Tap2Meet.Tables.UserLogin;
import com.WeApplify.Tap2Meet.Tables.WorkspaceBooked;
import com.WeApplify.Tap2Meet.Tables.WorkspaceServices;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by harshagulati on 06/07/17.
 */

public class MyBookings extends BaseActivity {

    public static final String TAG = "MyBookings";
    private static final int RESULT_SCHEDULE_CODE = 100;

    Activity activity;
    Context context;
    TextView tvDate, tvDay, tvTextDay, tvYear, tvBack;
    CalendarView cal;
    String lsTextDay, lsNumDay, lsMonthYear;
    Date finalDate;
    String lsDate1;
    SimpleDateFormat sdf, sdf1, sdf2, sdf3;
    ArrayList<BookingModel> alBooking;
    LinearLayoutManager llm;
    RecyclerView rcvBookings;
    BookingAdapter bookingAdapter;
    String lsId, lsAgenda, lsWorkspaceId, lsPlannedStarttime, lsPlannedEndtime, lsMeetingDate, lsEmployeeId, lsBookingDate, lsBookingStatus, lsStatus, lsTitle;
    String serviceName, fileName, employeeEmail;
    ArrayList<ServicesModel> alService;
    ArrayList<String> alParticipant,alDeviceKeys;
    ArrayList<AttachmentModel> alAttachment;
    String lsDate;
    String lsEmail;
    SharedPreferences sp,sp1;
    SharedPreferences.Editor editor;
    TransperantProgressDialog pd;
    String lsDateFromNot = "";
    DateFromNotificationModel notificationDateModel;
    List<String> deviceTokens = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.my_bookings);
        setTitle("Bookings");

        context = this;
        activity = this;

        initLayout();
    }


    public void initLayout() {
        alService = new ArrayList<>();
        alParticipant = new ArrayList<>();
        alAttachment = new ArrayList<>();
        alBooking = new ArrayList<>();
        alDeviceKeys = new ArrayList<>();
        notificationDateModel = new DateFromNotificationModel(context);

      //  lsDateFromNot = notificationDateModel.;

        pd = new TransperantProgressDialog(context);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvDay = (TextView) findViewById(R.id.tvDay);
        tvTextDay = (TextView) findViewById(R.id.tvTextDay);
        tvYear = (TextView) findViewById(R.id.tvYear);
        rcvBookings = (RecyclerView) findViewById(R.id.rcvBookings);
        tvBack = (TextView) findViewById(R.id.tvBack);

        sp = getSharedPreferences(App.LOGINCREDENTIALS, context.MODE_PRIVATE);
        sp1 = getSharedPreferences(App.SCHEDULEDATE, context.MODE_PRIVATE);
        lsEmail = sp.getString(UserLogin.UL_EMAIL, "");
        cal = (CalendarView) findViewById(R.id.calendarView1);

        lsDateFromNot = sp1.getString(App.SCHEDULEDATE,"");

        sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf1 = new SimpleDateFormat("EEEE");
        sdf2 = new SimpleDateFormat("dd");
        sdf3 = new SimpleDateFormat("MMMM yyyy");
        lsDate = tvDate.getText().toString();

        if ((lsDateFromNot == null) || (lsDateFromNot == "")) {
            if (lsDate == null || lsDate.equals("")) {
                lsDate = String.valueOf(sdf.format(new Date()));
                tvDate.setText(lsDate1);
            }

            lsTextDay = String.valueOf(sdf1.format(new Date()));
            lsNumDay = String.valueOf(sdf2.format(new Date()));
            lsMonthYear = String.valueOf(sdf3.format(new Date()));

            tvDay.setText(lsNumDay);
            tvTextDay.setText(lsTextDay);
            tvYear.setText(lsMonthYear);

            getBookings(lsDate, lsEmail);

        } else {
            tvDate.setText(lsDateFromNot);
            try {
                cal.setDate(sdf.parse(lsDateFromNot).getTime(), true, true);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Date lsFormat = null;
            try {
                lsFormat = sdf.parse(lsDateFromNot);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            lsTextDay = String.valueOf(sdf1.format(lsFormat));
            lsNumDay = String.valueOf(sdf2.format(lsFormat));
            lsMonthYear = String.valueOf(sdf3.format(lsFormat));

            tvDay.setText(lsNumDay);
            tvTextDay.setText(lsTextDay);
            tvYear.setText(lsMonthYear);

            getBookings(lsDateFromNot, lsEmail);
        }


        cal.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int monthOfYear,
                                            int dayOfMonth) {

                // TODO Auto-generated method stub

                String lsDay, lsYear;
                Date date1 = null;
                int liMonth;
                lsDay = String.valueOf(dayOfMonth);
                liMonth = Integer.valueOf(monthOfYear + 1);
                String lsMonth = String.valueOf(liMonth);
                lsYear = String.valueOf(year);
                if (dayOfMonth < 10) {
                    lsDay = "0" + dayOfMonth;
                }
                if (liMonth < 10) {
                    lsMonth = "0" + lsMonth;
                }
                lsDate = lsDay + "/" + lsMonth + "/" + lsYear;

                tvDate.setText(lsDate);

                try {
                    date1 = new SimpleDateFormat("dd/MM/yyyy").parse(lsDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                SimpleDateFormat sdf1 = new SimpleDateFormat("EEEE");
                String lsTextDay = String.valueOf(sdf1.format(date1));

                SimpleDateFormat sdf2 = new SimpleDateFormat("dd");
                String lsNumDay = String.valueOf(sdf2.format(date1));

                SimpleDateFormat sdf3 = new SimpleDateFormat("MMMM yyyy");
                String lsMonthYear = String.valueOf(sdf3.format(date1));

                tvDay.setText(lsNumDay);
                tvTextDay.setText(lsTextDay);
                tvYear.setText(lsMonthYear);

                getBookings(lsDate, lsEmail);
            }

        });

        tvBack.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.tvBack:
                Intent intent = new Intent(this, Home.class);
                editor = sp1.edit();
                editor.remove(App.SCHEDULEDATE);
                editor.commit();
                startActivity(intent);
                finish();
                break;
        }
    }

    public void getBookings(String lsDate, String email) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        final ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT * , "+UserLogin.UL_EMAIL+" FROM " + WorkspaceBooked.TABLENAME + " a," + Constants.MEETING_PARTICIPANTS_TABLE +
                " b ," + Constants.MTG_WORKSPACE_TABLENAME + " c , " + UserLogin.TABLENAME + " d"+
                " WHERE  a." + Constants.WB_BOOKINGID + "= b." + Constants.MEETING_BOOKINGID +
                " AND a."+WorkspaceBooked.WB_EMPLOYEE+" = d."+UserLogin.UL_EMPLOYEEID+
                " AND b." + Constants.EMPLOYEE_EMAIL + " = '" + email + "'" +
                " AND " + WorkspaceBooked.WB_BOOKING_STATUS + " = 'A'" +
                " AND " + WorkspaceBooked.WB_STATUS + " = 'N'" +
                " AND a." + WorkspaceBooked.WB_WORKSPACEID + " = c." + Constants.MW_ID +
                " AND " + WorkspaceBooked.WB_MEETINGDATE + " = '" + lsDate + "' " +
                "ORDER BY " + WorkspaceBooked.WB_PLANNEDSTARTTIME + " ASC";

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

            //   ShowProgressDialog.setTitle("Loading Data...");
            //   ShowProgressDialog.showProgress(this);
            pd.show();

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alBooking.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(WorkspaceBooked.WB_ID)) {

                                String lsId = groupData.getString(WorkspaceBooked.WB_BOOKINGID);
                                String lsName = groupData.getString(WorkspaceBooked.WB_WORKSPACEID);
                                String lsStartTime = groupData.getString(WorkspaceBooked.WB_PLANNEDSTARTTIME);
                                String lsEndTime = groupData.getString(WorkspaceBooked.WB_PLANNEDENDTIME);
                                String lsAgenda = groupData.getString(WorkspaceBooked.WB_AGENDA);
                                String lsMeetingDate = groupData.getString(WorkspaceBooked.WB_MEETINGDATE);
                                String lsTitle = groupData.getString(WorkspaceBooked.WB_TITLE);
                                String lsEmployeeId = groupData.getString(WorkspaceBooked.WB_EMPLOYEE);
                                String lsBookingDate = groupData.getString(WorkspaceBooked.WB_BOOKINGDATE);
                                String lsStatus = groupData.getString(WorkspaceBooked.WB_STATUS);
                                String lsBookingStatus = groupData.getString(WorkspaceBooked.WB_BOOKING_STATUS);
                                int liSso = groupData.getInt(WorkspaceBooked.WB_ST_SO);
                                int liEso = groupData.getInt(WorkspaceBooked.WB_ET_SO);
                                String lsBookedMail = groupData.getString(UserLogin.UL_EMAIL);


                                Bundle b = new Bundle();
                                b.putString(WorkspaceBooked.WB_WORKSPACEID, lsName);
                                b.putString(WorkspaceBooked.WB_PLANNEDSTARTTIME, lsStartTime);
                                b.putString(WorkspaceBooked.WB_PLANNEDENDTIME, lsEndTime);
                                b.putString(WorkspaceBooked.WB_BOOKINGID, lsId);
                                b.putString(WorkspaceBooked.WB_AGENDA, lsAgenda);
                                b.putString(WorkspaceBooked.WB_MEETINGDATE, lsMeetingDate);
                                b.putString(WorkspaceBooked.WB_TITLE, lsTitle);
                                b.putString(WorkspaceBooked.WB_EMPLOYEE, lsEmployeeId);
                                b.putString(WorkspaceBooked.WB_BOOKINGDATE, lsBookingDate);
                                b.putString(WorkspaceBooked.WB_STATUS, lsStatus);
                                b.putString(WorkspaceBooked.WB_BOOKING_STATUS, lsBookingStatus);
                                b.putInt(WorkspaceBooked.WB_ST_SO, liSso);
                                b.putInt(WorkspaceBooked.WB_ET_SO, liEso);
                                b.putString(UserLogin.UL_EMAIL,lsBookedMail);
                                b.putString("LoggedInUserEmail",lsEmail);


                                BookingModel model = new BookingModel(b);

                                alBooking.add(model);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            pd.dismiss();
                            showBookings();
                            // ShowProgressDialog.hideProgressDialog(context);
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }

    public void showBookings() {
        llm = new LinearLayoutManager(context);
        rcvBookings.setLayoutManager(llm);

        if (alBooking.size() == 0) {
            Bundle b = new Bundle();
            b.putString(WorkspaceBooked.WB_TITLE, "No Meetings scheduled");
            BookingModel model = new BookingModel(b);
            alBooking.add(model);
            bookingAdapter = new BookingAdapter(context, alBooking);
        } else {
            bookingAdapter = new BookingAdapter(context, alBooking);
        }
        rcvBookings.setAdapter(bookingAdapter);

        bookingAdapter.setItemClickIterface(new BookingAdapter.itemClickIterface() {
            @Override
            public void itemClick(int position) {
                lsId = alBooking.get(position).getId();
                lsAgenda = alBooking.get(position).getAgenda();
                lsWorkspaceId = alBooking.get(position).getName();
                lsPlannedStarttime = alBooking.get(position).getStartTime();
                lsPlannedEndtime = alBooking.get(position).getEndTime();
                lsMeetingDate = alBooking.get(position).getMeetingDate();
                lsEmployeeId = alBooking.get(position).getEmployeeId();
                lsBookingDate = alBooking.get(position).getBookingDate();
                lsStatus = alBooking.get(position).getStatus();
                lsBookingStatus = alBooking.get(position).getBookingStatus();
                lsTitle = alBooking.get(position).getTitle();

                if (lsTitle.equals("No Meetings scheduled")) {
                    Toast.makeText(context, "No meeting preview", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(context, BookingPreview.class);
                    intent.putExtra(WorkspaceBooked.WB_AGENDA, lsAgenda);
                    intent.putExtra(WorkspaceBooked.WB_WORKSPACEID, lsWorkspaceId);
                    intent.putExtra(WorkspaceBooked.WB_PLANNEDSTARTTIME, lsPlannedStarttime);
                    intent.putExtra(WorkspaceBooked.WB_PLANNEDENDTIME, lsPlannedEndtime);
                    intent.putExtra(WorkspaceBooked.WB_MEETINGDATE, lsMeetingDate);
                    intent.putExtra(WorkspaceBooked.WB_BOOKINGID, lsId);

                    startActivity(intent);
                }

            }
        });

        bookingAdapter.setItemDeleteIterface(new BookingAdapter.itemDeleteInterface() {
            @Override
            public void itemDelete(int position) {

                lsId = alBooking.get(position).getId();
                lsAgenda = alBooking.get(position).getAgenda();
                lsWorkspaceId = alBooking.get(position).getName();
                lsPlannedStarttime = alBooking.get(position).getStartTime();
                lsPlannedEndtime = alBooking.get(position).getEndTime();
                lsMeetingDate = alBooking.get(position).getMeetingDate();
                lsEmployeeId = alBooking.get(position).getEmployeeId();
                lsBookingDate = alBooking.get(position).getBookingDate();
                lsStatus = alBooking.get(position).getStatus();
                lsBookingStatus = alBooking.get(position).getBookingStatus();
                lsTitle = alBooking.get(position).getTitle();
                String lsLoggedInUser = alBooking.get(position).getLoggedInUser();
                String lsBookedUser = alBooking.get(position).getBookedMail();

                if(lsLoggedInUser.equals(lsBookedUser))
                {
                    showAlertDialog(position, alBooking.get(position).getMeetingDate() , lsPlannedStarttime , lsPlannedEndtime);
                }
                else
                {
                    CustomToast.showToast(activity,"You are not authorized to cancel this meeting.",50);
                }
            }
        });

        bookingAdapter.setItemEditIterface(new BookingAdapter.itemEditInterface() {
            @Override
            public void itemEdit(int position) {
                String lsId = alBooking.get(position).getId();
                String lsMeetingDate = alBooking.get(position).getMeetingDate();
                String lsStartTime = alBooking.get(position).getStartTime();
                String lsEndTime = alBooking.get(position).getEndTime();
                String lsAgenda = alBooking.get(position).getAgenda();
                String lsEmployeeId = alBooking.get(position).getEmployeeId();
                String lsWorkspaceId = alBooking.get(position).getName();
                String lsStatus = alBooking.get(position).getStatus();
                String lsTitle = alBooking.get(position).getTitle();
                int liSso = alBooking.get(position).getStartSo();
                int liEso = alBooking.get(position).getEndSo();
                String lsLoggedInUser = alBooking.get(position).getLoggedInUser();
                String lsBookedUser = alBooking.get(position).getBookedMail();


                if(lsLoggedInUser.equals(lsBookedUser))
                {
                    Intent intent = new Intent(context, Schedule.class);
                    intent.putExtra(WorkspaceBooked.WB_BOOKINGID, lsId);
                    intent.putExtra(WorkspaceBooked.WB_MEETINGDATE, lsMeetingDate);
                    intent.putExtra(WorkspaceBooked.WB_PLANNEDSTARTTIME, lsStartTime);
                    intent.putExtra(WorkspaceBooked.WB_PLANNEDENDTIME, lsEndTime);
                    intent.putExtra(WorkspaceBooked.WB_AGENDA, lsAgenda);
                    intent.putExtra(WorkspaceBooked.WB_EMPLOYEE, lsEmployeeId);
                    intent.putExtra(WorkspaceBooked.WB_WORKSPACEID, lsWorkspaceId);
                    intent.putExtra(WorkspaceBooked.WB_STATUS, lsStatus);
                    intent.putExtra(WorkspaceBooked.WB_TITLE, lsTitle);
                    intent.putExtra(WorkspaceBooked.WB_ST_SO, liSso);
                    intent.putExtra(WorkspaceBooked.WB_ET_SO, liEso);
                    // startActivity(intent);
                    startActivityForResult(intent, RESULT_SCHEDULE_CODE);

                    bookingAdapter.notifyItemChanged(position);
                }
                else
                {
                    CustomToast.showToast(activity,"You are not authorized to reschedule this meeting.",50);
                }
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_SCHEDULE_CODE && resultCode == RESULT_OK) {
            getBookings(lsDate, lsEmail);
        }
    }

    private void showAlertDialog(final int position , final String bookingDate, final String plannedStartTime, final String plannedEndTime) {

        final CustomDialog dialog = new CustomDialog(activity, context);
        CustomServices.hideSoftKeyboard(this);
        dialog.setCancel(false);
        dialog.setOutsideTouchable(false);
        dialog.setTitle("Confirm cancel...");
        dialog.setMessage("Are you sure you want to cancel " + alBooking.get(position).getTitle() + "?");
        dialog.showDialog();
        dialog.setDialogButtonClickListener(new CustomDialog.DialogButtonClick() {
            @Override
            public void DialogButtonClicked(View view) {
                switch (view.getId()) {
                    case R.id.cbOK:
                        if (deleteMeeting(lsId) > 0) {
                            String lsMessage = "Your Meeting got cancelled";
                            sendSamplePush(lsId,lsMessage,bookingDate,plannedStartTime,plannedEndTime);
                            getBookings(lsDate, lsEmail);
                            bookingAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(context, "Invalid Data", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case R.id.cbCancel:
                        dialog.dismiss();
                        break;
                }
            }
        });
    }

    public void sendSamplePush(String lsBookingId , final String lsMessage , final String bookingDate, final String plannedStartTime, final String plannedEndTime)
    {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        final ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT * FROM "+MeetingParticipants.TABLENAME+" WHERE "+MeetingParticipants.MP_BOOKINGID+" = '"+lsBookingId+"'";

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alParticipant.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(MeetingParticipants.MP_BOOKINGID)) {

                                String participantMail = groupData.getString(MeetingParticipants.MP_EMPLOYEEEMAIL);

                                alParticipant.add(participantMail);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                           getDeviceIds(lsMessage,bookingDate,plannedStartTime,plannedEndTime);
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });
    }

    public void getDeviceIds(final String lsMessage , final String bookingDate, final String plannedStartTime, final String plannedEndTime) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();
        String lsSql;

        for(int i= 0; i<= alParticipant.size()-1; i++)
        {
            String email = alParticipant.get(i);
            /*if(!email.equals(lsEmail))
            {*/
            lsSql = "SELECT "+UserLogin.UL_DEVICEKEY+" FROM " + UserLogin.TABLENAME + " WHERE " + UserLogin.UL_EMAIL + " = '" + email + "'";
            alSql.add(lsSql);
             //}
        }


        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    alDeviceKeys.clear();

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        JSONArray resultsArray = dataArray.getJSONArray(i);
                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(UserLogin.UL_DEVICEKEY)) {

                                String lsDeviceKey = groupData.getString(UserLogin.UL_DEVICEKEY);

                                alDeviceKeys.add(lsDeviceKey);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            for (int i = 0; i <= alDeviceKeys.size()-1;i++)
                            {
                                deviceTokens.add(alDeviceKeys.get(i));
                            }

                            // Convert to String[] array
                            String[] to = deviceTokens.toArray(new String[deviceTokens.size()]);

                            Map<String, String> payload = new HashMap<>();

                            // Add "message" parameter to payload
                            payload.put(App.NOTIFICATION, lsMessage);
                            payload.put(App.MEETING_DATE,bookingDate);
                            payload.put(App.MEETING_STARTTIME,plannedStartTime);
                            payload.put(App.MEETING_ENDTIME,plannedEndTime);
                        //    payload.put(App.MEETING_NOTIFICATION,lsMeetingDate);


                            // iOS notification fields
                            Map<String, Object> notification = new HashMap<>();

                            notification.put("badge", 1);
                            notification.put("sound", "ping.aiff");
                            notification.put("body", lsMessage);

                            // Prepare the push request
                            final PushyAPI.PushyPushRequest push = new PushyAPI.PushyPushRequest(payload, to, notification);

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        PushyAPI.sendPush(push);
                                    } catch (Exception ex) {
                                        //ex.printStackTrace();
                                        System.out.println("Exception called exception number is 1001 : " + ex.toString());
                                    }
                                }
                            }).start();
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }



    public int deleteMeeting(String lsId) {
        ArrayList<String> alSql = new ArrayList<>();
        String lsSql;


        lsSql = "DELETE FROM " + WorkspaceBooked.TABLENAME + " WHERE " + WorkspaceBooked.WB_BOOKINGID + " = '" + lsId + "'";
        alSql.add(lsSql);

        lsSql = "INSERT INTO " + WorkspaceBooked.TABLENAME + "(" + WorkspaceBooked.WB_BOOKINGID + "," + WorkspaceBooked.WB_EMPLOYEE + "," + WorkspaceBooked.WB_WORKSPACEID + "," +
                WorkspaceBooked.WB_MEETINGDATE + "," + WorkspaceBooked.WB_BOOKINGDATE + "," + WorkspaceBooked.WB_PLANNEDSTARTTIME + "," + WorkspaceBooked.WB_PLANNEDENDTIME + "," + WorkspaceBooked.WB_AGENDA + "," + WorkspaceBooked.WB_STATUS + "," + WorkspaceBooked.WB_BOOKING_STATUS + "," + WorkspaceBooked.WB_TITLE + ") VALUES " +
                "('" + lsId + "'" + "," + "'" + lsEmployeeId + "'" + "," + "'" + lsWorkspaceId + "'" + "," + "'" + lsMeetingDate + "'" + "," + "'" + lsBookingDate + "'" + "," + "'" + lsPlannedStarttime + "'" + "," + "'" + lsPlannedEndtime + "'" + "," + "'" + lsAgenda + "'" + "," + "'" + lsStatus + "'," + "'C'," + "'" + lsTitle + "'" + ")";
        alSql.add(lsSql);

        callCloud(alSql);
        return 1;
    }

    public void callCloud(ArrayList alSql) {

        DataTransmitter dt = new DataTransmitter();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        String lsSql;
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);

            dt.setLink(App.getSQLURL());
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            } else {
                /*
                CloudPending cp = new CloudPending(context);
                ContentValues cv = new ContentValues();
                cv.put(CloudPending.CP_OPERATION, jsonArray.toString());
                cv.put(CloudPending.CP_TIME, CustomServices.getCurrentTimeStamp());
                cv.put(CloudPending.CP_FILE, "N");
                cv.put(CloudPending.CP_UPLOADED, "N");
                cv.put(CloudPending.CP_URL, Posita.getSQLURL(context));
                cp.CRUD(cv);
                */
            }
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, Home.class);
        editor = sp1.edit();
        editor.remove(App.SCHEDULEDATE);
        editor.commit();
        startActivity(intent);
        finish();
    }
}
