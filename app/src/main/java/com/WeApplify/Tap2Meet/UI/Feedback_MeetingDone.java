package com.WeApplify.Tap2Meet.UI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Adapters.Feedback_MeetingDone_Adapter;
import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.Models.Feedback_MeetingDone_Model;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.DataTransmitter;
import com.WeApplify.Tap2Meet.Services.Debugger;
import com.WeApplify.Tap2Meet.Services.ShowProgressDialog;
import com.WeApplify.Tap2Meet.Services.TransperantProgressDialog;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.FeedbackAnsTable;
import com.WeApplify.Tap2Meet.Tables.UserLogin;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 21-07-17.
 */

public class Feedback_MeetingDone extends BaseActivity {

    public static final String TAG = "Feedback_MeetingDone";
    RecyclerView rcvMeetingDone;
    ArrayList<Feedback_MeetingDone_Model> alFeedback_MeetingDone;
    LinearLayoutManager llm;
    Context context;
    Activity activity;
    SharedPreferences sp;
    Feedback_MeetingDone_Adapter feedback_meetingDone_adapter;
    String lsEmail;
    static int FEEDBACK = 100;
    TextView tvBack;
    LinearLayout llMessage;
    TransperantProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_meetingdone);
        setTitle("Feedback");

        context = this;
        activity = this;


        initlayout();


    }

    public void initlayout() {

        sp = getSharedPreferences(App.LOGINCREDENTIALS, context.MODE_PRIVATE);

        lsEmail = sp.getString(UserLogin.UL_EMAIL, "");

        rcvMeetingDone = (RecyclerView) findViewById(R.id.rcvMeetingDone);
        llMessage = (LinearLayout)findViewById(R.id.llMessage);
        tvBack = (TextView)findViewById(R.id.tvBack);

        alFeedback_MeetingDone = new ArrayList<>();

        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Home.class);
                startActivity(intent);
                finish();
            }
        });

        pd = new TransperantProgressDialog(context);

        getMeetingTitle(lsEmail);

    }

    public void getMeetingTitle(String email) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();


        String lsSql = "SELECT * FROM " + Constants.MEETING_WORKSPACE_TABLE + " a," + Constants.MEETING_PARTICIPANTS_TABLE +
                " b  WHERE  a." + Constants.WB_BOOKINGID + "= b." + Constants.MEETING_BOOKINGID +
                " AND b." + Constants.EMPLOYEE_EMAIL + " = '" + email + "' AND " + Constants.MEETING_STATUS + " = 'C'" +
                " AND a."+Constants.WB_BOOKINGID+" NOT IN (SELECT "+ FeedbackAnsTable.FB_BOOKINGID+" FROM "+FeedbackAnsTable.TABLENAME+")";


        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

          //  ShowProgressDialog.setTitle("Loading Meetings...");
          //  ShowProgressDialog.showProgress(this);
            pd.show();

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alFeedback_MeetingDone.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.WB_BOOKINGID)) {

                                String lsMeetingTitle = groupData.getString(Constants.MEETING_TITLE);
                                String lsStartTime = groupData.getString(Constants.MEETING_STARTTIME);
                                String lsEndTime = groupData.getString(Constants.MEETING_ENDTIME);
                                String lsBookingId = groupData.getString(Constants.WB_BOOKINGID);

                                Bundle b = new Bundle();

                                b.putString(Constants.MEETING_TITLE, lsMeetingTitle);
                                b.putString(Constants.MEETING_STARTTIME, lsStartTime);
                                b.putString(Constants.MEETING_ENDTIME, lsEndTime);
                                b.putString(Constants.WB_BOOKINGID, lsBookingId);


                                Feedback_MeetingDone_Model model = new Feedback_MeetingDone_Model(b);

                                alFeedback_MeetingDone.add(model);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {

                            pd.dismiss();
                            show();
                           // ShowProgressDialog.hideProgressDialog(context);
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }

    public void show() {
        llm = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rcvMeetingDone.setLayoutManager(llm);

        if(alFeedback_MeetingDone.size() <= 0)
        {
            rcvMeetingDone.setVisibility(View.GONE);
            llMessage.setVisibility(View.VISIBLE);
        }
        else
        {
            llMessage.setVisibility(View.GONE);
            rcvMeetingDone.setVisibility(View.VISIBLE);
        }

        feedback_meetingDone_adapter = new Feedback_MeetingDone_Adapter(context, alFeedback_MeetingDone);
        rcvMeetingDone.setAdapter(feedback_meetingDone_adapter);

        feedback_meetingDone_adapter.setItemClickIterface(new Feedback_MeetingDone_Adapter.itemClickIterface() {
            @Override
            public void itemClick(int position) {
                String lsTitle = alFeedback_MeetingDone.get(position).getName();
                String lsBookingId = alFeedback_MeetingDone.get(position).getBookingId();

                Intent intent = new Intent(context,Feedback.class);
                Bundle b = alFeedback_MeetingDone.get(position).getData();
                intent.putExtra("results", b);
                intent.putExtra(Constants.MEETING_TITLE,lsTitle);
                intent.putExtra(Constants.WB_BOOKINGID,lsBookingId);
                startActivityForResult(intent,FEEDBACK);
                //finish();
            }

        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            getMeetingTitle(lsEmail);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(context, Home.class);
        startActivity(intent);
        finish();
    }
}
