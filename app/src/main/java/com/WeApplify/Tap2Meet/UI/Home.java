package com.WeApplify.Tap2Meet.UI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.Debugger;

import java.util.Timer;
import java.util.TimerTask;

import me.pushy.sdk.Pushy;

/**
 * Created by Admin on 22-06-2017.
 */

public class Home extends BaseActivity implements View.OnClickListener {

    public static final String TAG = "Home";
    Activity activity;
    Context context;
    Intent intent;
    ImageView ivSearch, icon;
    TextView tvBack;
    ImageView ivLogout;
    TextView tvBookMeeting, tvMyBookings, tvFeedback, tvInsights;
    private Handler handler = new Handler();
    ImageView ivHome;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Pushy.listen(this);
        setContentView(R.layout.home);
        setTitle("Welcome To TAP2MEET");

        activity = this;
        context = this;

        ivHome = (ImageView) findViewById(R.id.ivHome);
        initlayout();

        //   ivSearch.setVisibility(View.GONE);
        ivHome.setVisibility(View.GONE);
        tvBack.setVisibility(View.GONE);
        icon.setVisibility(View.VISIBLE);

    }

    public void initlayout() {
        //  ivSearch =(ImageView)findViewById(R.id.ivSearch);
        tvBack = (TextView) findViewById(R.id.tvBack);
        ivLogout = (ImageView) findViewById(R.id.ivLogout);

        tvBookMeeting = (TextView) findViewById(R.id.tvBookMeeting);
        tvMyBookings = (TextView) findViewById(R.id.tvMyBookings);
        tvFeedback = (TextView) findViewById(R.id.tvFeedback);
        tvInsights = (TextView) findViewById(R.id.tvInsights);

        tvBookMeeting.setOnClickListener(this);
        tvMyBookings.setOnClickListener(this);
        tvFeedback.setOnClickListener(this);
        tvInsights.setOnClickListener(this);

        ivLogout.setVisibility(View.VISIBLE);
        icon = (ImageView) findViewById(R.id.icon);

    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tvBookMeeting:
                intent = new Intent(this, CaptureDetails.class);
                startActivity(intent);
                finish();
                break;
            case R.id.tvMyBookings:
                intent = new Intent(this, MyBookings.class);
                startActivity(intent);
                finish();
                break;
            case R.id.tvFeedback:
                intent = new Intent(this, Feedback_MeetingDone.class);
                startActivity(intent);
                finish();
                break;
            case R.id.tvInsights:
                intent = new Intent(this, Insights.class);
                startActivity(intent);
                //  finish();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
