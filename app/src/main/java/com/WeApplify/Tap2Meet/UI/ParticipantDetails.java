
package com.WeApplify.Tap2Meet.UI;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Adapters.ParticipantAdapter;
import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.Models.ParticipantModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.CustomDialog;
import com.WeApplify.Tap2Meet.Services.CustomServices;
import com.WeApplify.Tap2Meet.Services.CustomToast;
import com.WeApplify.Tap2Meet.Services.MyDialog;
import com.WeApplify.Tap2Meet.Tables.MeetingParticipants;

import java.util.ArrayList;

/**
 * Created by Admin on 31-05-2017.
 */

public class ParticipantDetails extends BaseActivity {

    Context context;
    Activity activity;

    FloatingActionButton fbAddParticipant;
    RecyclerView rcvParticipants;
    ArrayList<String> arrayList;
    LinearLayoutManager llm;
    ParticipantAdapter adapter;
    ArrayList<ParticipantModel> alParticipant;
    TextView tvAgenda, tvShareDocuments;
    EditText etAgenda;
    LinearLayout llAgendaEdit, llMain, llShareDocuments;
    ImageView  ivSearch;
    Button cbUpload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.participant_details);
        setTitle("Participants");

        context = this;
        activity = this;

        initLayout();
    }

    public void initLayout() {

        arrayList = new ArrayList<>();
        alParticipant = new ArrayList<>();
        fbAddParticipant = (FloatingActionButton) findViewById(R.id.fbAddParticipant);
        rcvParticipants = (RecyclerView) findViewById(R.id.rcvParticipants);
        tvAgenda = (TextView) findViewById(R.id.tvAgenda);
        etAgenda = (EditText) findViewById(R.id.etAgenda);
        llAgendaEdit = (LinearLayout) findViewById(R.id.llAgendaEdit);
        llMain = (LinearLayout) findViewById(R.id.llMain);
        //ivDone = (ImageView) findViewById(R.id.ivDone);
        //ivSearch = (ImageView) findViewById(R.id.ivSearch);
        tvShareDocuments = (TextView) findViewById(R.id.tvShareDocuments);
        cbUpload = (Button) findViewById(R.id.cbUpload);
        llShareDocuments = (LinearLayout) findViewById(R.id.llShareDocuments);

        tvAgenda.setOnClickListener(this);
        fbAddParticipant.setOnClickListener(this);
        //ivDone.setOnClickListener(this);
        tvShareDocuments.setOnClickListener(this);
        cbUpload.setOnClickListener(this);

        showData();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.fbAddParticipant:
                showAlertDialog();
                break;

            case R.id.tvAgenda:
                llAgendaEdit.setVisibility(View.VISIBLE);
                //ivDone.setVisibility(View.VISIBLE);
                llMain.setVisibility(View.GONE);
                //ivSearch.setVisibility(View.GONE);

                break;


/* case R.id.ivDone:
                CustomServices.hideSoftKeyboard(activity);
                llMain.setVisibility(View.VISIBLE);
                ivDone.setVisibility(View.GONE);
                setText();
                break;*/
            case R.id.cbUpload:
                break;

            case R.id.tvShareDocuments:
                llShareDocuments.setVisibility(View.VISIBLE);
                llMain.setVisibility(View.GONE);
                //ivSearch.setVisibility(View.GONE);
                llAgendaEdit.setVisibility(View.GONE);
                break;

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void showAlertDialog() {

        final MyDialog dialog = new MyDialog(activity, context);
        CustomServices.hideSoftKeyboard(this);
        dialog.setCancel(false);
        dialog.setOutsideTouchable(false);
        dialog.setTitle("Medicines");
        dialog.showDialog();

        dialog.setDialogButtonClickListener(new MyDialog.DialogButtonClick() {
            @Override
            public void DialogButtonClicked(View view, String lsParticipant) {
                switch (view.getId()) {
                    case R.id.cbOK:

                        if (lsParticipant.length() > 0) {
                            if ((lsParticipant.contains("@")))
                            {
                                Bundle b = new Bundle();

                                b.putString(MeetingParticipants.MP_EMPLOYEEEMAIL, lsParticipant);

                                ParticipantModel model = new ParticipantModel(b);
                                alParticipant.add(model);

                                adapter.notifyItemInserted(alParticipant.size() - 1);


                            }
                            else
                            {
                                CustomToast.showToast(activity, "Provide valid e-mail", 50);
                            }

                        } else {
                            CustomToast.showToast(activity, "Participant e-mail please", 50);
                        }

                        break;

                    case R.id.cbCancel:
                        dialog.dismiss();
                        break;
                }
            }
        });
    }



    public void showData() {
        llm = new LinearLayoutManager(context);
        rcvParticipants.setLayoutManager(llm);
        adapter = new ParticipantAdapter(context, alParticipant);
        rcvParticipants.setAdapter(adapter);
    }

    public void setText() {
        String lsAgenda = etAgenda.getText().toString();
        tvAgenda.setText(lsAgenda);
    }

    @Override
    public void onBackPressed() {
        if(llAgendaEdit.getVisibility() == View.VISIBLE) {
            llMain.setVisibility(View.VISIBLE);
           // ivDone.setVisibility(View.GONE);
            setText();
        }
        else if(llMain.getVisibility() == View.VISIBLE)
        {
            super.onBackPressed();
        }


    }
}


