package com.WeApplify.Tap2Meet.UI;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.Models.LovCityModel;
import com.WeApplify.Tap2Meet.Models.UserModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.CustomDialog;
import com.WeApplify.Tap2Meet.Services.CustomServices;
import com.WeApplify.Tap2Meet.Services.CustomToast;
import com.WeApplify.Tap2Meet.Services.DataTransmitter;
import com.WeApplify.Tap2Meet.Services.Debugger;
import com.WeApplify.Tap2Meet.Services.RegisterForPushNotificationsAsync;
import com.WeApplify.Tap2Meet.Services.ShowProgressDialog;
import com.WeApplify.Tap2Meet.Services.TransperantProgressDialog;
import com.WeApplify.Tap2Meet.Tables.Lovs;
import com.WeApplify.Tap2Meet.Tables.UserLogin;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import me.pushy.sdk.Pushy;
import okhttp3.Response;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * Created by Admin on 21-06-2017.
 */

public class Login extends BaseActivity implements View.OnClickListener {

    public final static String TAG = "Login";
    Activity activity;
    Context context;

    String lsUserName, lsUserPassword, lsEmployeeId, lsDeviceKey, lsEmail, lsConfirmDeviceKey;
    EditText etEmail, etPassword;
    TextView tvLogin, tvForgotPassword;
    SharedPreferences sp, sp1;
    String deviceToken, lsKey, lsTestEmail;
    SharedPreferences.Editor editor;
    UserLogin userLogin;
    ArrayList<UserModel> alUser;
    ArrayList<String> alEmails;
    UserModel userModel;
    TextView tvBack, tvLoggedIn;
    ImageView ivCheck, ivUncheck;
    ImageView ivSearch, ivNotification, ivHome, ivLogout;
    FrameLayout fmlProduct;
    TransperantProgressDialog pd;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // Check whether the user has granted us the READ/WRITE_EXTERNAL_STORAGE permissions
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PERMISSION_GRANTED) {
            // Request both READ_EXTERNAL_STORAGE and WRITE_EXTERNAL_STORAGE so that the
            // Pushy SDK will be able to persist the device token in the external storage
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }

        setContentView(R.layout.login);
        setTitle("Login");
        activity = this;
        context = this;

        sp1 = getSharedPreferences(App.DEVICETOKEN, context.MODE_PRIVATE);
        deviceToken = sp1.getString(UserLogin.UL_DEVICEKEY, "");
        Toast.makeText(context, "Device "+deviceToken, Toast.LENGTH_LONG).show();

        initlayout();

    }

    public void initlayout() {

        sp = getSharedPreferences(App.LOGINCREDENTIALS, context.MODE_PRIVATE);

        alEmails = new ArrayList<>();
        alUser = new ArrayList<>();
        userLogin = new UserLogin(context);
        userModel = new UserModel(context);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        tvLogin = (TextView) findViewById(R.id.tvLogin);
        tvBack = (TextView) findViewById(R.id.tvBack);

        tvForgotPassword = (TextView) findViewById(R.id.tvForgotPassword);
        ivCheck = (ImageView) findViewById(R.id.ivCheck);
        ivUncheck = (ImageView) findViewById(R.id.ivUncheck);
        tvLoggedIn = (TextView) findViewById(R.id.tvLoggedIn);
        //  ivSearch = (ImageView)findViewById(R.id.ivSearch);
        ivNotification = (ImageView) findViewById(R.id.ivNotification);
        fmlProduct = (FrameLayout) findViewById(R.id.fmlProduct);
        ivHome = (ImageView) findViewById(R.id.ivHome);
        ivLogout = (ImageView) findViewById(R.id.ivLogout);
        pd = new TransperantProgressDialog(context);

        //   ivNotification.setVisibility(View.INVISIBLE);
        //    ivSearch.setVisibility(View.GONE);
        fmlProduct.setVisibility(View.GONE);
        tvBack.setVisibility(View.INVISIBLE);
        ivHome.setVisibility(View.GONE);
        ivLogout.setVisibility(View.GONE);

        etEmail.setText("rushab@gmail.com");
        etPassword.setText("123456");

        ivCheck.setVisibility(View.GONE);


        ivUncheck.setOnClickListener(this);
        ivCheck.setOnClickListener(this);
        tvLoggedIn.setOnClickListener(this);


        tvLogin.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tvLogin:

                if (haveNetworkConnection()) {
                    if (etEmail.length() > 0 && etPassword.length() > 0) {

                        validateUser(etEmail.getText().toString(), etPassword.getText().toString());

                    } else {
                        CustomToast.showToast(this, "Please enter username and password", 50);
                    }

                } else {
                    showAlertDialog("Settings", "Enable WIFI");
                }

                break;

            case R.id.ivUncheck:
                ivUncheck.setVisibility(View.GONE);
                ivCheck.setVisibility(View.VISIBLE);
                break;

            case R.id.ivCheck:
                ivCheck.setVisibility(View.GONE);
                ivUncheck.setVisibility(View.VISIBLE);
                break;

            case R.id.tvLoggedIn:
                if (ivUncheck.getVisibility() == View.VISIBLE) {
                    ivUncheck.setVisibility(View.GONE);
                    ivCheck.setVisibility(View.VISIBLE);
                } else {
                    ivCheck.setVisibility(View.GONE);
                    ivUncheck.setVisibility(View.VISIBLE);
                }

                break;

            case R.id.tvForgotPassword:

                if (etEmail.length() > 0) {

                    validateEmail(etEmail.getText().toString());

                } else {
                    CustomToast.showToast(activity, "Please enter username", 50);
                }

                break;

        }
    }

    private void showAlertDialog(String lsTitle, String lsMessage) {

        final CustomDialog dialog = new CustomDialog(activity, context);
        dialog.setCancel(false);
        dialog.setTitle(lsTitle);
        dialog.setMessage(lsMessage);

        dialog.showDialog();
        dialog.setDialogButtonClickListener(new CustomDialog.DialogButtonClick() {
            @Override
            public void DialogButtonClicked(View view) {
                switch (view.getId()) {
                    case R.id.cbOK:
                        Intent wifiIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                        startActivityForResult(wifiIntent, App.RESULT_CODE_WIFI);
                        break;

                    case R.id.cbCancel:
                        dialog.dismiss();
                        break;
                }
            }
        });
    }


    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


    public boolean getUserData(String lsName, String lsPassword) {
        if ((etEmail.getText().toString()).equals(lsName) && (etPassword.getText().toString()).equals(lsPassword)) {
            return true;
        } else {
            return false;
        }
    }


    public void validateUser(final String lsName, String lsPassword) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT * FROM " + UserLogin.TABLENAME + " WHERE " + UserLogin.UL_EMAIL + " = '" + lsName + "' AND " + UserLogin.UL_PASSWORD + " = '" + lsPassword + "'";

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

            pd.show();
            // pd.setCancelable(true);

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alUser.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(UserLogin.UL_ID)) {

                                lsUserName = groupData.getString(UserLogin.UL_EMAIL);
                                lsUserPassword = groupData.getString(UserLogin.UL_PASSWORD);
                                lsEmployeeId = groupData.getString(UserLogin.UL_EMPLOYEEID);
                                lsKey = groupData.getString(UserLogin.UL_DEVICEKEY);


                                Bundle b = new Bundle();
                                b.putString(UserLogin.UL_EMAIL, lsUserName);
                                b.putString(UserLogin.UL_PASSWORD, lsUserPassword);
                                b.putString(UserLogin.UL_EMPLOYEEID, lsEmployeeId);
                                UserModel model = new UserModel(b);

                                alUser.add(model);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {

                            pd.dismiss();

                            if (getUserData(lsUserName, lsUserPassword)) {

                                if (deviceToken.length() <= 0 || deviceToken.equals("") || deviceToken.equals(null)) {
                                    if (lsKey.length() <= 0  || lsKey.equals("") || (lsKey.equals("null")) || lsKey.equals(null)) {
                                        RegisterForPushNotificationsAsync registerPushy = new RegisterForPushNotificationsAsync();
                                        registerPushy.setContext(context, lsUserName);
                                        registerPushy.execute();

                                        openHomeScreen();
                                    } else {
                                        showAlertDialogForDevice("Attention please.", lsUserName + " registered with other device! Do you want to update your device.");
                                    }

                                } else {
                                    validateDeviceToken(deviceToken,lsName);
                                }
                            } else {
                                CustomToast.showToast(activity, "invalid LoginId/Password", 50);
                            }

                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }

    public void openHomeScreen() {
        String lsUserEmail = null,lsUserPass = null,lsUserId = null;
        if (ivCheck.getVisibility() == View.VISIBLE) {
            Intent intent = new Intent(activity, Home.class);
            editor = sp.edit();
            for (int i = 0; i <= alUser.size()-1 ; i++)
            {
                 lsUserEmail = alUser.get(i).getName();
                 lsUserPass = alUser.get(i).getPassword();
                 lsUserId = alUser.get(i).getEmployeeId();
            }

            editor.putString(UserLogin.UL_EMAIL, lsUserEmail);
            editor.putString(UserLogin.UL_PASSWORD, lsUserPass);
            editor.putString(UserLogin.UL_EMPLOYEEID, lsUserId);
            editor.commit();
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(activity, Home.class);
            editor = sp.edit();
            editor.putString(UserLogin.UL_EMPLOYEEID, lsUserId);
            editor.putString(UserLogin.UL_EMAIL, lsUserEmail);
            editor.commit();
            startActivity(intent);
            finish();
        }
    }

    private void showAlertDialogForDevice(String lsTitle, String lsMessage) {

        final CustomDialog dialog = new CustomDialog(activity, context);
        dialog.setCancel(false);
        dialog.setTitle(lsTitle);
        dialog.setMessage(lsMessage);

        dialog.showDialog();
        dialog.setDialogButtonClickListener(new CustomDialog.DialogButtonClick() {
            @Override
            public void DialogButtonClicked(View view) {
                switch (view.getId()) {
                    case R.id.cbOK:
                        RegisterForPushNotificationsAsync registerPushy = new RegisterForPushNotificationsAsync();
                        registerPushy.setContext(context, lsUserName);
                        registerPushy.execute();
                        openHomeScreen();
                        break;

                    case R.id.cbCancel:
                        dialog.dismiss();
                        Toast.makeText(context, "Thank you..", Toast.LENGTH_SHORT).show();
                        openHomeScreen();
                        break;
                }
            }
        });
    }

    private void showAlertDialogForDevice1(String lsTitle, String lsMessage) {

        final CustomDialog dialog = new CustomDialog(activity, context);
        dialog.setCancel(false);
        dialog.setTitle(lsTitle);
        dialog.setMessage(lsMessage);

        dialog.showDialog();
        dialog.setDialogButtonClickListener(new CustomDialog.DialogButtonClick() {
            @Override
            public void DialogButtonClicked(View view) {
                switch (view.getId()) {
                    case R.id.cbOK:
                        dialog.dismiss();
                        Toast.makeText(context, "Thank you..", Toast.LENGTH_SHORT).show();
                        openHomeScreen();
                        break;

                    case R.id.cbCancel:
                        dialog.dismiss();
                        Toast.makeText(context, "Thank you..", Toast.LENGTH_SHORT).show();
                        openHomeScreen();
                        break;
                }
            }
        });
    }

    public void validateDeviceToken(String lsToken , final String lsName) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT * FROM " + UserLogin.TABLENAME + " WHERE " + UserLogin.UL_DEVICEKEY + " = '" + lsToken + "'";

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(UserLogin.UL_ID)) {

                                lsTestEmail = groupData.getString(UserLogin.UL_EMAIL);
                                lsEmployeeId = (groupData.getString(UserLogin.UL_EMPLOYEEID));
                                lsConfirmDeviceKey = (groupData.getString(UserLogin.UL_DEVICEKEY));

                                alEmails.add(lsTestEmail);

                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {

                            if (verifyDeviceKey(lsConfirmDeviceKey)) {

                                boolean lbVerifyEmail = false;

                                for(int i = 0 ; i<=alEmails.size()-1 ; i++)
                                {
                                    String email = alEmails.get(i);

                                    if(email.equals(lsName))
                                    {
                                        lbVerifyEmail = true;
                                        break;
                                    }
                                }

                                if(lbVerifyEmail == true)
                                {
                                    Toast.makeText(context, "Device registered with same acc.", Toast.LENGTH_SHORT).show();
                                    openHomeScreen();
                                }
                                else
                                {
                                    if((lsKey.length() <= 0) || (lsKey.equals(null)) || (lsKey.equals("null")) || (lsKey.equals("")) )
                                    {
                                        showAlertDialogForDevice("Confirmation",lsUserName+ " is not registered with any device. Confirm do you want to receive your notification on same device");
                                    }
                                    else
                                    {
                                        showAlertDialogForDevice("Attention please.", lsUserName + " registered with other device! Do you want to update your device.");
                                    }

                                }


                            } else {
                                if ((lsKey.length() <= 0) || (lsKey.equals(null)) || (lsKey.equals("null")) || (lsKey.equals(""))) {
                                    RegisterForPushNotificationsAsync registerPushy = new RegisterForPushNotificationsAsync();
                                    registerPushy.setContext(context, lsUserName);
                                    registerPushy.execute();
                                    openHomeScreen();
                                } else {
                                    showAlertDialogForDevice("Attention please.", lsUserName + " registered with other device! Do you want to update your device.");
                                }
                            }

                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }



    public void validateEmail(String lsName) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT * FROM " + UserLogin.TABLENAME + " WHERE " + UserLogin.UL_EMAIL + " = '" + lsName + "'";

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(UserLogin.UL_ID)) {

                                lsUserName = groupData.getString(UserLogin.UL_EMAIL);
                                lsEmployeeId = (groupData.getString(UserLogin.UL_EMPLOYEEID));
                                lsDeviceKey = (groupData.getString(UserLogin.UL_DEVICEKEY));

                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {

                            if (getUserEmail(lsUserName)) {

                                Intent intent = new Intent(activity, PasswordReset.class);
                                /*editor = sp.edit();
                                editor.putString(UserLogin.UL_EMAIL, etEmail.getText().toString());
                                editor.commit();*/
                                intent.putExtra(UserLogin.UL_EMAIL, lsUserName);
                                intent.putExtra(UserLogin.UL_EMPLOYEEID, lsEmployeeId);
                                intent.putExtra(UserLogin.UL_DEVICEKEY, lsDeviceKey);
                                startActivity(intent);
                                //  finish();

                            } else {
                                CustomToast.showToast(activity, "Username does not exists", 50);
                            }

                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }

    public boolean getUserEmail(String lsName) {
        if ((etEmail.getText().toString()).equals(lsName)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean verifyDeviceKey(String lsKey) {

        if (deviceToken.equals(lsKey)) {
            return true;
        } else {
            return false;
        }
    }

}

