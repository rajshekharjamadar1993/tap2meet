package com.WeApplify.Tap2Meet.UI;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.WeApplify.Tap2Meet.Adapters.FeedbackAdapter;
import com.WeApplify.Tap2Meet.Adapters.LovCityAdapter;
import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.Models.FeedbackModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.CustomToast;
import com.WeApplify.Tap2Meet.Services.DataTransmitter;
import com.WeApplify.Tap2Meet.Services.Debugger;
import com.WeApplify.Tap2Meet.Services.ShowProgressDialog;
import com.WeApplify.Tap2Meet.Services.TransperantProgressDialog;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.FeedbackAnsTable;
import com.WeApplify.Tap2Meet.Tables.MeetingParticipants;
import com.WeApplify.Tap2Meet.Tables.UserLogin;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 06-07-2017.
 */
public class Feedback extends BaseActivity implements View.OnClickListener {

    public static final String TAG = "Feedback";
    RecyclerView rcvQuestion;
    ArrayList<FeedbackModel> alFeedback;
    LinearLayoutManager llm;
    Context context;
    Activity activity;
    TextView tvTitle;
    FeedbackAdapter feedbackadapter;
    String newString,lsSuggestion,lsFeebackQuestion, lsEmployeeId;
    Button cbSubmit;
    EditText etSuggestion;
    SharedPreferences sp;
    String lsBookingId;
    ArrayList<String> arrayList;
    TextView tvBack;
    TransperantProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback);
        setTitle("Feedback");

        context = this;
        activity = this;

        Bundle b = getIntent().getExtras();
        if(b == null) {
             newString= null;
        } else {
            newString= b.getString(Constants.MEETING_TITLE);
        }

        initlayout();
        getQuestions();
        tvTitle.setText(newString);


    }

    public void initlayout(){

        sp = getSharedPreferences(App.LOGINCREDENTIALS, context.MODE_PRIVATE);

        pd = new TransperantProgressDialog(context);
      //  arrayList = new ArrayList<>();
        arrayList = new ArrayList<>();
        rcvQuestion =(RecyclerView)findViewById(R.id.rcvQuestion);
        alFeedback = new ArrayList<>();
        tvTitle =(TextView)findViewById(R.id.tvTitle);
        cbSubmit = (Button)findViewById(R.id.cbSubmit);
        etSuggestion = (EditText)findViewById(R.id.etSuggestion);
        tvBack = (TextView)findViewById(R.id.tvBack);

        lsEmployeeId = sp.getString(UserLogin.UL_EMPLOYEEID,"");

        lsBookingId = getIntent().getStringExtra(Constants.WB_BOOKINGID);

        /*SmileRating smileRating = (SmileRating) findViewById(R.id.smile_rating);

        smileRating.setOnSmileySelectionListener(new SmileRating.OnSmileySelectionListener() {
            @Override
            public void onSmileySelected(@BaseRating.Smiley int smiley, boolean reselected) {
                // reselected is false when user selects different smiley that previously selected one
                // true when the same smiley is selected.
                // Except if it first time, then the value will be false.
                switch (smiley) {
                    case SmileRating.BAD:
                        Log.i(TAG, "Bad");
                        break;
                    case SmileRating.GOOD:
                        Log.i(TAG, "Good");
                        break;
                    case SmileRating.GREAT:
                        Log.i(TAG, "Great");
                        break;
                    case SmileRating.OKAY:
                        Log.i(TAG, "Okay");
                        break;
                    case SmileRating.TERRIBLE:
                        Log.i(TAG, "Terrible");
                        break;
                }
            }
        });*/

        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cbSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId())
        {
            case R.id.cbSubmit:

               /* if(callSave() > 0)
                {
                    Toast.makeText(context,"Feedback given succesfully",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(context,"Invalid data",Toast.LENGTH_SHORT).show();
                }*/

                callSave();
                Toast.makeText(context,"Feedback given succesfully",Toast.LENGTH_SHORT).show();

                setResult(RESULT_OK);
                finish();

                break;
        }
    }


    public void callSave() {

        lsSuggestion = etSuggestion.getText().toString();

        for (int i = 0; i < alFeedback.size(); i++) {
            Bundle b = alFeedback.get(i).getValues();

            String lsQueId = b.getString(Constants.LOVS_STOREVALUE);
            String lsValue = b.getString(FeedbackAnsTable.FB_ANSWER);

            String lsSql = "INSERT INTO " + FeedbackAnsTable.TABLENAME +
                    " ( " + FeedbackAnsTable.FB_EMPLOYEE + "," + FeedbackAnsTable.FB_BOOKINGID + "," + FeedbackAnsTable.FB_QUESTION + "," + FeedbackAnsTable.FB_ANSWER + "," + FeedbackAnsTable.FB_SUGGESTION + " ) " +
                    "VALUES ( '" + lsEmployeeId + "','" + lsBookingId + "','" + lsQueId + "','" + lsValue + "','" + lsSuggestion + "' )";

            arrayList.add(lsSql);

        }


            callCloud(arrayList);

    }


    public void getQuestions() {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT * FROM " + Constants.LOVS_TABLENAME + " WHERE " + Constants.LOVS_TYPE + " = 'Feedback'"/* ORDER By " + Constants.LOVS_SORTINGORDER + " ASC "*/;


        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

          //  ShowProgressDialog.setTitle("Loading Questions...");
          //  ShowProgressDialog.showProgress(this);
            pd.show();
        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alFeedback.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.LOVS_ID)) {

                                String lsQuestion = groupData.getString(Constants.LOVS_DISPLAYVALUE);
                                String lsQueId = groupData.getString(Constants.LOVS_STOREVALUE);


                                Bundle b = new Bundle();
                                b.putString(Constants.LOVS_DISPLAYVALUE, lsQuestion);
                                b.putString(Constants.LOVS_STOREVALUE, lsQueId);


                                FeedbackModel model = new FeedbackModel(b);

                                alFeedback.add(model);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {

                            pd.dismiss();
                            show();
                          //  ShowProgressDialog.hideProgressDialog(context);
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }
    public void show() {
        llm = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false);
        rcvQuestion.setLayoutManager(llm);
        feedbackadapter = new FeedbackAdapter(context, alFeedback);
        rcvQuestion.setAdapter(feedbackadapter);
    }


    public void callCloud(ArrayList alSql) {
        DataTransmitter dt = new DataTransmitter();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        String lsSql;
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(App.getSQLURL());
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            } else {
                Toast.makeText(activity, "Network issue", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
