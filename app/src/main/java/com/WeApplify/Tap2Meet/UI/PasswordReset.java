package com.WeApplify.Tap2Meet.UI;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.CustomToast;
import com.WeApplify.Tap2Meet.Services.DataTransmitter;
import com.WeApplify.Tap2Meet.Services.PrepareSQL;
import com.WeApplify.Tap2Meet.Tables.MeetingParticipants;
import com.WeApplify.Tap2Meet.Tables.UserLogin;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by harshagulati on 05/07/17.
 */

public class PasswordReset extends BaseActivity {

    public static final String TAG = "PasswordReset";

    Activity activity;
    Context context;

    SharedPreferences sp;
    SharedPreferences.Editor editor;
    String lsUsername,lsDeviceKey,lsEmployeeId,lsPassword,lsConfirmPassword;
    TextView tvUsername,tvBack;
    ImageView ivNotification,ivSearch,ivLogout;
    EditText etPassword,etConfirmPassword;
    Button cbSubmit;
    ArrayList<String> arrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.password_reset);
        setTitle("Reset Password");

        context = this;
        activity = this;

        initLayout();
    }

    public void initLayout()
    {

        arrayList = new ArrayList<>();

        Bundle b = getIntent().getExtras();

        lsUsername = b.getString(UserLogin.UL_EMAIL);
        lsDeviceKey = b.getString(UserLogin.UL_DEVICEKEY);
        lsEmployeeId = b.getString(UserLogin.UL_EMPLOYEEID);
        sp = getSharedPreferences(App.LOGINCREDENTIALS, context.MODE_PRIVATE);
       // lsUsername = sp.getString(UserLogin.UL_EMAIL, "");

        tvBack = (TextView)findViewById(R.id.tvBack);
        ivNotification = (ImageView)findViewById(R.id.ivNotification);
       // ivSearch = (ImageView)findViewById(R.id.ivSearch);
        ivLogout = (ImageView)findViewById(R.id.ivLogout);

        tvUsername = (TextView)findViewById(R.id.tvUsername);
        etPassword = (EditText)findViewById(R.id.etPassword);
        etConfirmPassword = (EditText)findViewById(R.id.etConfirmPassword);
        cbSubmit = (Button)findViewById(R.id.cbSubmit);

        tvBack.setVisibility(View.INVISIBLE);
        ivNotification.setVisibility(View.GONE);
        ivLogout.setVisibility(View.GONE);
     //   ivSearch.setVisibility(View.GONE);

        tvUsername.setText(lsUsername);

        cbSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(callSave() > 0)
                {
                    Intent intent = new Intent(context,Home.class);
                    editor = sp.edit();
                    editor.putString(UserLogin.UL_EMPLOYEEID, lsEmployeeId);
                    editor.commit();
                    CustomToast.showToast(activity,"Password Changed Succesfully",50);
                    startActivity(intent);
                    finish();
                }
                else {
                    CustomToast.showToast(activity,"Invalid credentials",50);
                }
            }
        });
    }


    public int callSave() {

        lsPassword = etPassword.getText().toString();
        lsConfirmPassword = etConfirmPassword.getText().toString();


            String lsSql;

            lsSql = "DELETE FROM "+UserLogin.TABLENAME+" WHERE "+UserLogin.UL_EMAIL+" = '"+lsUsername+"'";
            arrayList.add(lsSql);

           lsSql = "INSERT INTO " + UserLogin.TABLENAME +
                " ( " + UserLogin.UL_DEVICEKEY + "," + UserLogin.UL_EMAIL + "," + UserLogin.UL_PASSWORD + ","  + UserLogin.UL_EMPLOYEEID + " ) " +
                "VALUES ( '" + lsDeviceKey + "','" + lsUsername +"','" + lsPassword + "','" + lsEmployeeId + "' )";

            arrayList.add(lsSql);


        if ((lsPassword.length() > 5)) {
        if ((lsPassword.equals(lsConfirmPassword))) {
            callCloud(arrayList);
            resetDE();
            return 1;
        } else {
            etConfirmPassword.setError("Confirm password should match with password");
            return -1;
        }
        } else {
            etPassword.setError("required minimum 6 digits");
            return -1;
        }


        }

    public void callCloud(ArrayList alSql) {
        DataTransmitter dt = new DataTransmitter();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        String lsSql;
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(App.getSQLURL());
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            } else {
                /*
                CloudPending cp = new CloudPending(context);
                ContentValues cv = new ContentValues();
                cv.put(CloudPending.CP_OPERATION, jsonArray.toString());
                cv.put(CloudPending.CP_TIME, CustomServices.getCurrentTimeStamp());
                cv.put(CloudPending.CP_FILE, "N");
                cv.put(CloudPending.CP_UPLOADED, "N");
                cv.put(CloudPending.CP_URL, Posita.getSQLURL(context));
                cp.CRUD(cv);
                */
            }
        }

    }

    public void resetDE()
    {
        etPassword.setText("");
        etConfirmPassword.setText("");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

