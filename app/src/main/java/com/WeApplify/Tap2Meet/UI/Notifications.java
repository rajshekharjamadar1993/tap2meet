package com.WeApplify.Tap2Meet.UI;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.WeApplify.Tap2Meet.Adapters.NotificationAdapter;
import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.Models.NotificationModel;
import com.WeApplify.Tap2Meet.R;

import java.util.ArrayList;

/**
 * Created by ADMIN on 04-12-2017.
 */

public class Notifications extends BaseActivity {

    public static final String TAG = "Notifications";
    Context context;
    Activity activity;
    RecyclerView rcvNotifications;
    NotificationAdapter notificationAdapter;
    ArrayList<NotificationModel> alNotificationModel;
    String lsId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification);
        context = this;
        activity = this;

        initLayout();
    }

    public void initLayout() {
        alNotificationModel = new ArrayList<>();
        rcvNotifications = (RecyclerView) findViewById(R.id.rcvNotifications);

        //   lsId = getIntent().getStringExtra("ABC");


        Toast.makeText(context, "Successfull", Toast.LENGTH_SHORT).show();

    }
}
