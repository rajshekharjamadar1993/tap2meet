package com.WeApplify.Tap2Meet.UI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Adapters.AmenitiesAdapter;
import com.WeApplify.Tap2Meet.Adapters.BookingPreviewAttachmentAdapter;
import com.WeApplify.Tap2Meet.Adapters.BookingPreviewParticipantAdapter;
import com.WeApplify.Tap2Meet.Adapters.BookingPreviewServicesAdapter;
import com.WeApplify.Tap2Meet.Adapters.PreviewAttachmentAdapter;
import com.WeApplify.Tap2Meet.Adapters.PreviewParticipantAdapter;
import com.WeApplify.Tap2Meet.Adapters.PreviewServicesAdapter;
import com.WeApplify.Tap2Meet.Adapters.ServiceAdapter;
import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.Models.AmenitiesModel;
import com.WeApplify.Tap2Meet.Models.AttachmentModel;
import com.WeApplify.Tap2Meet.Models.ParticipantModel;
import com.WeApplify.Tap2Meet.Models.ServicesModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.DataTransmitter;
import com.WeApplify.Tap2Meet.Services.Debugger;
import com.WeApplify.Tap2Meet.Services.TransperantProgressDialog;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.MeetingParticipants;
import com.WeApplify.Tap2Meet.Tables.MeetingPreread;
import com.WeApplify.Tap2Meet.Tables.UserLogin;
import com.WeApplify.Tap2Meet.Tables.WorkspaceBooked;
import com.WeApplify.Tap2Meet.Tables.WorkspaceServices;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 06-07-2017.
 */

public class BookingPreview extends BaseActivity {

    public static final String TAG = "BookingPreview";

    Context context;
    Activity activity;

    ArrayList<ServicesModel> alService;
    ArrayList<AttachmentModel> alAttachment;
    ArrayList<ParticipantModel> alParticipants;
    ArrayList<AmenitiesModel> alAmenities;
    String lsAgenda, lsStartTime, lsEndTime, lsMeetingDate, lsWorkspaceId, lsId;
    TextView tvDate, tvAgenda, tvStartTime, tvEndTime, tvCapacity, tvType, tvOffice, tvBack;
    LinearLayoutManager llmParticipant, llmAttachments;
    RecyclerView rcvParticipants, rcvAttachments, rcvAmenities, rcvServices;
    BookingPreviewParticipantAdapter participantAdapter;
    BookingPreviewAttachmentAdapter attachmentAdapter;
    String lsType, lsCapacity, lsOffice;
    LinearLayoutManager llmAmenities, llmServices;
    AmenitiesAdapter amenitiesAdapter;
    BookingPreviewServicesAdapter servicesAdapter;

    ImageView ivHome , ivLogout;
    FrameLayout fmlProduct;
    TransperantProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking_preview);
        setTitle("Preview");

        context = this;
        activity = this;

        ivHome = (ImageView)findViewById(R.id.ivHome);
        ivLogout = (ImageView)findViewById(R.id.ivLogout);
        fmlProduct = (FrameLayout)findViewById(R.id.fmlProduct);

        initLayout();

        ivHome.setVisibility(View.GONE);
        ivLogout.setVisibility(View.GONE);
        fmlProduct.setVisibility(View.GONE);
    }

    public void initLayout() {

        alService = new ArrayList<>();
        alAttachment = new ArrayList<>();
        alParticipants = new ArrayList<>();
        alAmenities = new ArrayList<>();

        pd = new TransperantProgressDialog(context);
        Bundle b = getIntent().getExtras();

        lsAgenda = b.getString(WorkspaceBooked.WB_AGENDA);
        lsMeetingDate = b.getString(WorkspaceBooked.WB_MEETINGDATE);
        lsStartTime = b.getString(WorkspaceBooked.WB_PLANNEDSTARTTIME);
        lsEndTime = b.getString(WorkspaceBooked.WB_PLANNEDENDTIME);
        lsWorkspaceId = b.getString(WorkspaceBooked.WB_WORKSPACEID);
        lsId = b.getString(WorkspaceBooked.WB_BOOKINGID);


        getWorkspaceDetails(lsWorkspaceId);
        getCloudAmeneties(lsWorkspaceId);

        getServiceData(lsId);
        getParticipantData(lsId);
        getAttachmentData(lsId);

        rcvParticipants = (RecyclerView) findViewById(R.id.rcvParticipants);
        rcvAttachments = (RecyclerView) findViewById(R.id.rcvAttachnments);
        rcvAmenities = (RecyclerView) findViewById(R.id.rcvAmenities);
        rcvServices = (RecyclerView) findViewById(R.id.rcvServices);

        tvDate = (TextView) findViewById(R.id.tvDate);
        tvAgenda = (TextView) findViewById(R.id.tvAgenda);
        tvStartTime = (TextView) findViewById(R.id.tvStartTime);
        tvEndTime = (TextView) findViewById(R.id.tvEndTime);
        tvOffice = (TextView) findViewById(R.id.tvOffice);
        tvCapacity = (TextView) findViewById(R.id.tvCapacity);
        tvType = (TextView) findViewById(R.id.tvType);
        tvBack = (TextView) findViewById(R.id.tvBack);

        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvDate.setText(lsMeetingDate);
        tvAgenda.setText(lsAgenda);
        tvStartTime.setText(lsStartTime);
        tvEndTime.setText(lsEndTime);


    }

    public void showServices() {
        llmServices = new LinearLayoutManager(context);
        rcvServices.setLayoutManager(llmServices);
        servicesAdapter = new BookingPreviewServicesAdapter(context, alService);
        rcvServices.setAdapter(servicesAdapter);
    }

    public void showParticipant() {
        llmParticipant = new LinearLayoutManager(context);
        rcvParticipants.setLayoutManager(llmParticipant);
        participantAdapter = new BookingPreviewParticipantAdapter(context, alParticipants);
        rcvParticipants.setAdapter(participantAdapter);
    }

    public void showAttachments() {
        llmAttachments = new LinearLayoutManager(context);
        rcvAttachments.setLayoutManager(llmAttachments);
        attachmentAdapter = new BookingPreviewAttachmentAdapter(context, alAttachment);
        rcvAttachments.setAdapter(attachmentAdapter);

    }

    public void getAttachmentData(String lsBookingId) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql;


        lsSql = "SELECT * FROM " + MeetingPreread.TABLENAME + " WHERE " + MeetingPreread.MPR_BOOKINGID + " = '" + lsBookingId + "'";
        alSql.add(lsSql);


        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

            pd.show();
        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");
                    alAttachment.clear();
                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(MeetingPreread.MPR_BOOKINGID)) {

                                String fileName = groupData.getString(MeetingPreread.MPR_FILE);

                                Bundle b = new Bundle();
                                b.putString(MeetingPreread.MPR_FILE, fileName);

                                AttachmentModel model = new AttachmentModel((b));

                                alAttachment.add(model);

                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            pd.dismiss();
                            showAttachments();
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }


    public void getParticipantData(String lsBookingId) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql;

        /* SELECT * FROM meeting_participants a , workspace_booked b , userlogin c
        WHERE a.mp_bookingid = b.wb_bookingid AND b.wb_employee = c.ul_employeeid AND  mp_bookingid = '1-1511346589' */

       // lsSql = "SELECT * FROM " + MeetingParticipants.TABLENAME + " WHERE " + MeetingParticipants.MP_BOOKINGID + " = '" + lsBookingId + "'";
        lsSql = "SELECT * FROM "+MeetingParticipants.TABLENAME+" a , "+WorkspaceBooked.TABLENAME+" b , "+ UserLogin.TABLENAME+" c " +
                "WHERE a."+MeetingParticipants.MP_BOOKINGID+" = b."+WorkspaceBooked.WB_BOOKINGID+
                " AND b."+WorkspaceBooked.WB_EMPLOYEE+" = c."+UserLogin.UL_EMPLOYEEID+
                " AND "+MeetingParticipants.MP_BOOKINGID+ " = '"+lsBookingId+"'";
        alSql.add(lsSql);


        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }
            pd.show();

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alParticipants.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(MeetingParticipants.MP_BOOKINGID)) {

                                String employeeEmail = groupData.getString(MeetingParticipants.MP_EMPLOYEEEMAIL);
                                String lsHost = groupData.getString(UserLogin.UL_EMAIL);

                                Bundle b = new Bundle();
                                b.putString(MeetingParticipants.MP_EMPLOYEEEMAIL, employeeEmail);
                                b.putString(UserLogin.UL_EMAIL,lsHost);
                                ParticipantModel model = new ParticipantModel((b));
                                alParticipants.add(model);

                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            pd.dismiss();
                            showParticipant();
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }


    public void getServiceData(String lsBookingId) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql;

        //  SELECt sv_desc from services a , workspace_services b WHERE a.sv_id = b.ws_serviceid AND ws_bookingid = '1-1499328718'

        lsSql = "SELECT " + Constants.SV_DESC + " FROM " + Constants.SERVICES_TABLENAME + " a , " + WorkspaceServices.TABLENAME + " b  WHERE a." + Constants.SV_ID + " = b." + WorkspaceServices.WS_SERVICEID + " AND " + WorkspaceServices.WS_BOOKINGID + " = '" + lsBookingId + "'";
        alSql.add(lsSql);


        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

            pd.show();

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");
                    alService.clear();
                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.SV_DESC)) {

                                String serviceName = groupData.getString(Constants.SV_DESC);

                                Bundle b = new Bundle();
                                b.putString(Constants.SV_DESC, serviceName);
                                ServicesModel model = new ServicesModel((b));

                                alService.add(model);

                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            pd.dismiss();
                            showServices();
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    public void getWorkspaceDetails(String lsWorkspaceId) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql;

        //  SELECt sv_desc from services a , workspace_services b WHERE a.sv_id = b.ws_serviceid AND ws_bookingid = '1-1499328718'

        lsSql = "SELECT * FROM " + Constants.MTG_WORKSPACE_TABLENAME + "  WHERE " + Constants.MW_ID + " = '" + lsWorkspaceId + "'";
        alSql.add(lsSql);


        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

            pd.show();

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");
                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.MW_ID)) {

                                lsCapacity = groupData.getString(Constants.MW_CAPACITY);
                                lsType = groupData.getString(Constants.MW_TYPE);
                                lsOffice = groupData.getString(Constants.MW_OFFICE);

                            }

                        }

                    }

                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            pd.dismiss();
                            tvCapacity.setText(lsCapacity);
                            tvType.setText(lsType);
                            tvOffice.setText(lsOffice);
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }


    public void getCloudAmeneties(String lsWorkspaceId) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT " + Constants.AM_NAME +
                " FROM "
                + Constants.AMENITIES_TABLENAME + " a , "
                + Constants.MTG_WORKSPACE_TABLENAME + " b , "
                + Constants.MTG_WORKSPACE_AMENITIES_TABLENAME + " c " +
                "WHERE b." + Constants.MW_ID + " = c." + Constants.WA_MWID +
                " AND c." + Constants.WA_AMID + " = a." + Constants.AM_ID +
                " AND b." + Constants.MW_ID + " = '" + lsWorkspaceId + "'";

        // alAmenities.clear();

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

            pd.show();

        }

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alAmenities.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);
                        for (int x = 0; x < resultsArray.length(); x++) {

                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.AM_NAME)) {

                                String lsName = groupData.getString(Constants.AM_NAME);


                                Bundle b = new Bundle();
                                b.putString(Constants.AM_NAME, lsName);


                                AmenitiesModel model = new AmenitiesModel(b);

                                alAmenities.add(model);

                            }

                        }

                    }

                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            pd.dismiss();
                            showAmenities();

                        }
                    }));


                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1109 " + e.toString());
                }

            }

        });

    }

    public void showAmenities() {
        llmAmenities = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        rcvAmenities.setLayoutManager(llmAmenities);
        amenitiesAdapter = new AmenitiesAdapter(context, alAmenities);
        rcvAmenities.setAdapter(amenitiesAdapter);
    }


}
