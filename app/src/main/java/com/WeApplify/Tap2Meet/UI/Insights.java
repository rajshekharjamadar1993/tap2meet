package com.WeApplify.Tap2Meet.UI;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Adapters.BookingAdapter;
import com.WeApplify.Tap2Meet.Adapters.TodayMeeting_Adapter;
import com.WeApplify.Tap2Meet.Adapters.UpComingMeeting_Adapter;
import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.Models.BookingModel;
import com.WeApplify.Tap2Meet.Models.TodayMeeting_Model;
import com.WeApplify.Tap2Meet.Models.UpComingMeeting_Model;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.CustomServices;
import com.WeApplify.Tap2Meet.Services.DataTransmitter;
import com.WeApplify.Tap2Meet.Services.Debugger;
import com.WeApplify.Tap2Meet.Services.ShowProgressDialog;
import com.WeApplify.Tap2Meet.Services.TransperantProgressDialog;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.MeetingParticipants;
import com.WeApplify.Tap2Meet.Tables.UserLogin;
import com.WeApplify.Tap2Meet.Tables.WorkspaceBooked;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Admin on 08-07-2017.
 */

public class Insights extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "Insights";

    Activity activity;
    Context context;
    LinearLayout llMeeetingAnalysis, llMeetingAnalysisActive, llUpcomingMeetingsActive, llUpcomingMeetings, llTodayMeetings, llTodayMeetingsActive;
    RecyclerView rcvUpcomingMeetings,rcvTodaysMeeting;
    ArrayList<UpComingMeeting_Model> alMeeting;
    ArrayList<TodayMeeting_Model> alTodayMeeting;
    LinearLayoutManager llm,llm1;
    UpComingMeeting_Adapter adapter;
    TodayMeeting_Adapter adapter1;
    SharedPreferences sp;
    String lsEmail,lsMeetingCount;
    TextView tvAnswer1,tvAnswer2,tvAnswer3,tvBack;
    String lsToday,lsTodayFormat;
    TextView tvNoMeetings;
    TransperantProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.insights);
        setTitle("Insights");

        context = this;
        activity = this;

        initlayout();

        getUpcomingMeetings(lsEmail);
        getMeetingcounts(lsEmail);
        getTotalTime(lsEmail);
        getTotalMeetingcount(lsEmail);
        TodayMeeting(lsEmail);
    }

    public void initlayout() {
        llMeeetingAnalysis = (LinearLayout) findViewById(R.id.llMeetingAnalysis);
        llMeetingAnalysisActive = (LinearLayout) findViewById(R.id.llMeetingAnalysisActive);
        llUpcomingMeetingsActive = (LinearLayout) findViewById(R.id.llUpcomingMeetingsActive);
        llUpcomingMeetings = (LinearLayout) findViewById(R.id.llUpcomingMeetings);
        llTodayMeetings = (LinearLayout) findViewById(R.id.llTodayMeetings);
        llTodayMeetingsActive = (LinearLayout) findViewById(R.id.llTodayMeetingsActive);
        rcvUpcomingMeetings = (RecyclerView) findViewById(R.id.rcvUpcomingMeetings);
        rcvTodaysMeeting =(RecyclerView)findViewById(R.id.rcvTodaysMeeting);
        tvAnswer1 =(TextView)findViewById(R.id.tvAnswer1);
        tvAnswer2 =(TextView)findViewById(R.id.tvAnswer2);
        tvAnswer3 =(TextView)findViewById(R.id.tvAnswer3);
        tvBack =(TextView)findViewById(R.id.tvBack);
        tvNoMeetings =(TextView)findViewById(R.id.tvNoMeetings);

        pd = new TransperantProgressDialog(context);


        llMeeetingAnalysis.setOnClickListener(this);
        llMeetingAnalysisActive.setOnClickListener(this);
        llUpcomingMeetings.setOnClickListener(this);
        llUpcomingMeetingsActive.setOnClickListener(this);
        llTodayMeetings.setOnClickListener(this);
        llTodayMeetingsActive.setOnClickListener(this);
        tvAnswer1.setOnClickListener(this);
        tvBack.setOnClickListener(this);


        alMeeting = new ArrayList<>();
        alTodayMeeting =new ArrayList<>();
        sp = getSharedPreferences(App.LOGINCREDENTIALS, context.MODE_PRIVATE);
        lsEmail = sp.getString(UserLogin.UL_EMAIL, "");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        lsTodayFormat = sdf.format(date);

        lsToday = CustomServices.getTodaysDate();

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.llMeetingAnalysis:
               defaultvisibilities();
                llMeetingAnalysisActive.setVisibility(View.VISIBLE);
                llMeeetingAnalysis.setVisibility(View.GONE);
                break;

            case R.id.llMeetingAnalysisActive:
                defaultvisibilities();
                llMeetingAnalysisActive.setVisibility(View.GONE);
                llMeeetingAnalysis.setVisibility(View.VISIBLE);
                break;

            case R.id.llUpcomingMeetings:
                defaultvisibilities();
                llUpcomingMeetingsActive.setVisibility(View.VISIBLE);
                llUpcomingMeetings.setVisibility(View.GONE);
                break;

            case R.id.llUpcomingMeetingsActive:
                defaultvisibilities();
                llUpcomingMeetingsActive.setVisibility(View.GONE);
                llUpcomingMeetings.setVisibility(View.VISIBLE);
                break;

            case R.id.llTodayMeetings:
                defaultvisibilities();
                llTodayMeetingsActive.setVisibility(View.VISIBLE);
                llTodayMeetings.setVisibility(View.GONE);
                break;

            case R.id.llTodayMeetingsActive:
                defaultvisibilities();
                llTodayMeetings.setVisibility(View.VISIBLE);
                llTodayMeetingsActive.setVisibility(View.GONE);
                break;

            case R.id.tvBack:
                super.onBackPressed();
                break;
        }
    }

    public void defaultvisibilities()
    {
        llMeeetingAnalysis.setVisibility(View.VISIBLE);
        llMeetingAnalysisActive.setVisibility(View.GONE);
        llTodayMeetingsActive.setVisibility(View.GONE);
        llUpcomingMeetingsActive.setVisibility(View.GONE);
        llTodayMeetings.setVisibility(View.VISIBLE);
        llUpcomingMeetings.setVisibility(View.VISIBLE);
    }

    public void getUpcomingMeetings(String email) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT * , d."+UserLogin.UL_EMAIL+" FROM " + WorkspaceBooked.TABLENAME + " a," + Constants.MEETING_PARTICIPANTS_TABLE +
                " b,"+ Constants.MTG_WORKSPACE_TABLENAME+" c , " +UserLogin.TABLENAME+
                " d WHERE a."+WorkspaceBooked.WB_EMPLOYEE+" = d."+UserLogin.UL_EMPLOYEEID+
                " AND a." + Constants.WB_BOOKINGID + "= b." + Constants.MEETING_BOOKINGID +
                " AND b." + Constants.EMPLOYEE_EMAIL + " = '" + email + "' " +
                "AND " + WorkspaceBooked.WB_BOOKING_STATUS + " = 'A' " +
                "AND a."+WorkspaceBooked.WB_WORKSPACEID+ " = c."+ Constants.MW_ID+
                " AND STR_TO_DATE("+WorkspaceBooked.WB_MEETINGDATE+" , '%d/%m/%Y') > '"+lsTodayFormat+"' " +
                "ORDER BY STR_TO_DATE("+WorkspaceBooked.WB_MEETINGDATE+" , '%d/%m/%Y') ASC";

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

          //  ShowProgressDialog.setTitle("Loading Meetings...");
          //  ShowProgressDialog.showProgress(this);
            pd.show();

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alMeeting.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.WB_BOOKINGID)) {

                                String lsMeetingTitle = groupData.getString(Constants.MEETING_TITLE);
                                String lsStartTime = groupData.getString(Constants.MEETING_STARTTIME);
                                String lsEndTime = groupData.getString(Constants.MEETING_ENDTIME);
                                String lsDate = groupData.getString(WorkspaceBooked.WB_MEETINGDATE);
                                String lsRoomName = groupData.getString(Constants.MW_NAME);
                                String email = groupData.getString(UserLogin.UL_EMAIL);


                                Bundle b = new Bundle();

                                b.putString(Constants.MEETING_TITLE, lsMeetingTitle);
                                b.putString(Constants.MEETING_STARTTIME, lsStartTime);
                                b.putString(Constants.MEETING_ENDTIME, lsEndTime);
                                b.putString(WorkspaceBooked.WB_MEETINGDATE, lsDate);
                                b.putString(Constants.MW_NAME,lsRoomName);
                                b.putString(UserLogin.UL_EMAIL, email);
                                b.putString("Login_user",lsEmail);


                                UpComingMeeting_Model model = new UpComingMeeting_Model(b);

                                alMeeting.add(model);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                          //  ShowProgressDialog.hideProgressDialog(context);
                           pd.dismiss();
                            show();

                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }
            }
        });
    }

    public void show() {
        llm = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rcvUpcomingMeetings.setLayoutManager(llm);
        adapter = new UpComingMeeting_Adapter(context, alMeeting);
        rcvUpcomingMeetings.setAdapter(adapter);
    }

    public void getMeetingcounts(String email) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "select count(*) FROM " + WorkspaceBooked.TABLENAME + " a LEFT JOIN " + UserLogin.TABLENAME + " b ON b." + UserLogin.UL_EMPLOYEEID + " = a." + WorkspaceBooked.WB_EMPLOYEE +
                " WHERE b." + UserLogin.UL_EMAIL + " = '" + email + " ' And "+WorkspaceBooked.WB_BOOKING_STATUS+" = 'A'";

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

          //  pd.show();

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                       // alMeeting.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has("count(*)")) {

                                lsMeetingCount = groupData.getString("count(*)");

                                Bundle b = new Bundle();

                                b.putString("count(*)", lsMeetingCount);

                               // UpComingMeeting_Model model = new UpComingMeeting_Model(b);
                                tvAnswer1.setText(lsMeetingCount);

                                //alMeeting.add(model);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }
            }
        });
    }
    public void getTotalTime(String email) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT sum(wb_totalspendtime) FROM "+ WorkspaceBooked.TABLENAME +
                       " a LEFT JOIN "+MeetingParticipants.TABLENAME+" b ON a."+WorkspaceBooked.WB_BOOKINGID+" = b."+MeetingParticipants.MP_BOOKINGID+
        " WHERE a."+WorkspaceBooked.WB_BOOKINGID+" = b."+MeetingParticipants.MP_BOOKINGID+" AND "+MeetingParticipants.MP_EMPLOYEEEMAIL+" = '"+email+"' And "+WorkspaceBooked.WB_BOOKING_STATUS+" = 'A'";


        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        // alMeeting.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.ANALYSIS_SUM)) {

                                lsMeetingCount = groupData.getString(Constants.ANALYSIS_SUM);

                                Bundle b = new Bundle();

                                b.putString(Constants.ANALYSIS_SUM, lsMeetingCount);

                                // UpComingMeeting_Model model = new UpComingMeeting_Model(b);
                                tvAnswer2.setText(lsMeetingCount +" Min");

                                //alMeeting.add(model);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }
            }
        });
    }
    public void getTotalMeetingcount(String email) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT count(*) FROM "+ WorkspaceBooked.TABLENAME +
                       " a LEFT JOIN "+ MeetingParticipants.TABLENAME+" b ON a."+WorkspaceBooked.WB_BOOKINGID+" = b."+MeetingParticipants.MP_BOOKINGID +
                       " WHERE a."+WorkspaceBooked.WB_BOOKINGID+" = b."+MeetingParticipants.MP_BOOKINGID+" AND "+MeetingParticipants.MP_EMPLOYEEEMAIL+" = '" +email+ "' And "+WorkspaceBooked.WB_BOOKING_STATUS+" = 'A'";


        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        // alMeeting.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.ANALYSIS_COUNT)) {

                                lsMeetingCount = groupData.getString(Constants.ANALYSIS_COUNT);

                                Bundle b = new Bundle();

                                b.putString(Constants.ANALYSIS_COUNT, lsMeetingCount);

                                // UpComingMeeting_Model model = new UpComingMeeting_Model(b);
                                tvAnswer3.setText(lsMeetingCount);

                                //alMeeting.add(model);
                            }
                        }
                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }
            }
        });
    }
    public void TodayMeeting(String email) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT * , d."+UserLogin.UL_EMAIL+
                " FROM " + WorkspaceBooked.TABLENAME + " a," + Constants.MEETING_PARTICIPANTS_TABLE +
                " b ,"+ Constants.MTG_WORKSPACE_TABLENAME+" c ," +UserLogin.TABLENAME+ " d "+
                "WHERE a."+WorkspaceBooked.WB_EMPLOYEE+" = d."+UserLogin.UL_EMPLOYEEID+
                " AND a." + Constants.WB_BOOKINGID + "= b." + Constants.MEETING_BOOKINGID +
                " AND b." + Constants.EMPLOYEE_EMAIL + " = '" + email + "'" +
                " AND " + WorkspaceBooked.WB_BOOKING_STATUS + " = 'A'" +
                " AND a."+WorkspaceBooked.WB_WORKSPACEID+ " = c."+ Constants.MW_ID+
                " AND "+WorkspaceBooked.WB_MEETINGDATE+" = '"+lsToday+"'" +
                " ORDER BY "+WorkspaceBooked.WB_PLANNEDSTARTTIME+" ASC";


        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

           // ShowProgressDialog.setTitle("Loading Meetings...");
           // ShowProgressDialog.showProgress(this);
            pd.show();

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alTodayMeeting.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.WB_BOOKINGID)) {
                                String lsMeetingTitle = groupData.getString(Constants.MEETING_TITLE);
                                String lsStartTime = groupData.getString(Constants.MEETING_STARTTIME);
                                String lsEndTime = groupData.getString(Constants.MEETING_ENDTIME);
                                //String lsDate = groupData.getString(WorkspaceBooked.WB_MEETINGDATE);
                                String lsRoomName = groupData.getString(Constants.MW_NAME);
                                String email = groupData.getString(UserLogin.UL_EMAIL);



                                Bundle b = new Bundle();

                                b.putString(Constants.MEETING_TITLE, lsMeetingTitle);
                                b.putString(Constants.MEETING_STARTTIME, lsStartTime);
                                b.putString(Constants.MEETING_ENDTIME, lsEndTime);
                                //b.putString(WorkspaceBooked.WB_MEETINGDATE, lsDate);
                                b.putString(Constants.MW_NAME,lsRoomName);
                                b.putString(UserLogin.UL_EMAIL,email);
                                b.putString("Login_user",lsEmail);


                                TodayMeeting_Model model = new TodayMeeting_Model(b);
                                alTodayMeeting.add(model);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                         //   ShowProgressDialog.hideProgressDialog(context);
                            pd.dismiss();
                            showTodaysMeetings();

                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }
            }
        });
    }

    public void showTodaysMeetings() {
        llm1 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        rcvTodaysMeeting.setLayoutManager(llm1);

        if (alTodayMeeting.size() == 0) {
            Bundle b = new Bundle();
            //b.putString(WorkspaceBooked.WB_TITLE, "No Meetings scheduled");
            //TodayMeeting_Model model = new TodayMeeting_Model(b);
            //alTodayMeeting.add(model);
            tvNoMeetings.setVisibility(View.VISIBLE);
            //adapter1 = new TodayMeeting_Adapter(context, alTodayMeeting);

        } else {
            adapter1 = new TodayMeeting_Adapter(context, alTodayMeeting);

        }
        rcvTodaysMeeting.setAdapter(adapter1);
    }
}
