package com.WeApplify.Tap2Meet.UI;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.WeApplify.Tap2Meet.Adapters.AttachmentAdapter;
import com.WeApplify.Tap2Meet.Adapters.LovCityAdapter;
import com.WeApplify.Tap2Meet.Adapters.LovOtherCityAdapter;
import com.WeApplify.Tap2Meet.Adapters.OfficeAdapter;
import com.WeApplify.Tap2Meet.Adapters.ParticipantAdapter;
import com.WeApplify.Tap2Meet.Adapters.ParticipantsList_Adapter;
import com.WeApplify.Tap2Meet.Adapters.RoomsAdapter;
import com.WeApplify.Tap2Meet.Adapters.MTimeAdapter;
import com.WeApplify.Tap2Meet.Adapters.ServiceAdapter;
import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.Models.AmenitiesModel;
import com.WeApplify.Tap2Meet.Models.AttachmentModel;
import com.WeApplify.Tap2Meet.Models.ETimeModel;
import com.WeApplify.Tap2Meet.Models.LovCityModel;
import com.WeApplify.Tap2Meet.Models.LovOtherCityModel;
import com.WeApplify.Tap2Meet.Models.MTimeModel;
import com.WeApplify.Tap2Meet.Models.NTimeModel;
import com.WeApplify.Tap2Meet.Models.OfficeModel;
import com.WeApplify.Tap2Meet.Models.ParticipantModel;
import com.WeApplify.Tap2Meet.Models.ParticipantsList_Model;
import com.WeApplify.Tap2Meet.Models.RoomsModel;
import com.WeApplify.Tap2Meet.Models.ServicesModel;
import com.WeApplify.Tap2Meet.Models.SortingOrderModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.CalendarView;
import com.WeApplify.Tap2Meet.Services.CustomServices;
import com.WeApplify.Tap2Meet.Services.CustomToast;
import com.WeApplify.Tap2Meet.Services.DataTransmitter;
import com.WeApplify.Tap2Meet.Services.Debugger;
import com.WeApplify.Tap2Meet.Services.MyDialog;
import com.WeApplify.Tap2Meet.Services.PushyAPI;
import com.WeApplify.Tap2Meet.Services.ShowProgressDialog;
import com.WeApplify.Tap2Meet.Services.TransperantProgressDialog;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.Lovs;
import com.WeApplify.Tap2Meet.Tables.MeetingParticipants;
import com.WeApplify.Tap2Meet.Tables.MeetingPreread;
import com.WeApplify.Tap2Meet.Tables.UserLogin;
import com.WeApplify.Tap2Meet.Tables.WorkspaceBooked;
import com.WeApplify.Tap2Meet.Tables.WorkspaceServices;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * Created by Admin on 15-06-2017.
 */

public class CaptureDetails extends BaseActivity {

    public static final String TAG = "CaptureDetails";

    Context context;
    Activity activity;

    ArrayList<SortingOrderModel> alSortingOrderData;
    RecyclerView rcvServices, rcvParticipantsList;
    ArrayList<ServicesModel> alServices;
    LinearLayoutManager llmServices, llParticipantsList;
    ServiceAdapter serviceAdapter;
    ArrayList<Integer> serviceList;
    ArrayList alInts;
    EditText etTitle, etParticipantsList;
    String lsEmail;
    View vwPopularCity, vwOtherCity;
    SharedPreferences sp;
    ScrollView svParticipant;
    Switch switchButton;
    long strtTime;
    final int callbackId = 42;
    ArrayList<String> alDeviceKeys;

    int liOfficeId;
    String lsOfficeCapacity, lsOfficeCity, lsOfficeName, lsEmployeeId, lsEmployee,
            lsTodaysDate, lsMeetingDate, startTime = "", endTime = "";

    String lsAgenda = "", lsTitle = "", lsAttachment;
    TextView tvPopularCities, tvOtherCity, tvReschedule;
    TextInputLayout tilCity;
    FloatingActionButton fbAddAttachments;
    AttachmentAdapter attachmentAdapter;
    RecyclerView rcvParticipants, rcvAttachments;
    ArrayList<String> arrayListP, arrayListA, arrayListS;
    LinearLayoutManager llmParticipant, llmAttachment;
    ParticipantAdapter participantAdapter;
    ArrayList<ParticipantModel> alParticipant;
    ArrayList<AttachmentModel> alAttachment;
    EditText etAgenda;
    LinearLayout llAgendaEdit, llMain, llShareDocuments, llAgenda, llAttachment, llServices;
    ImageView ivLogout, ivNotification;
    ArrayList<ServicesModel> alServicesPreview;
    String lsConfidential = "n";
    List<String> deviceTokens = new ArrayList<>();


    int liRSId;
    String gDate, gNxtDate, gNxtDate1, gNxtDate2, gNxtDate3,
            lsDate = "", lsRoomName = "", lsRoomCapacity = "";
    LinearLayout llHome, llFilter, llRoomDetails, llBooking, llCities, llParticipant;
    TextView tvBack, tvNext, tvSubmit, tvSelectedDate, tvPreview;
    String lsType;
    TextView tvCurrentDate, tvCurrentDay, tvNextDate, tvNextDay,
            tvNxt1Dt, tvNxt1Day, tvNxt2Dt, tvNxt2Day, tvNxt3Dt, tvNxt3Day;
    FrameLayout flCurrentDt, flNextDt, flNxt1Dt, flNxt2Dt, flNxt3Dt, flNxt4Dt;

    AppCompatImageView ivMeetingHall, ivTrainingHall, ivConferenceHall, ivBoardRoom,
            ivMeetingHallPressed, ivTrainingHallPressed, ivConferenceHallPressed, ivBoardRoomPressed;

    LinearLayout llMeetingRoom, llConferenceHall, llBoardRoom, llTrainingHall;

    RecyclerView rcvCity, rcvOtherCity, rcvOffices, rcvRooms, rcvMTime;
    GridLayoutManager glm;
    ArrayList<ParticipantsList_Model> alParticipantList;
    ArrayList<LovCityModel> alLovCity;
    ArrayList<AmenitiesModel> alAmenities;
    ArrayList<OfficeModel> alOffice;
    ArrayList<LovOtherCityModel> alLovOtherCity;
    ParticipantsList_Adapter ParticipantsList_Adapter;
    LovCityAdapter lovCityAdapter;
    LovOtherCityAdapter lovOtherCityAdapter;
    EditText etCity;
    View vwView;
    String lsCityValue = "", lsOfficeValue = "", lsCityDisplayValue = "";
    LinearLayoutManager llm, llm1, llm2;
    GridLayoutManager glmMorning;
    OfficeAdapter officeAdapter;
    RoomsAdapter roomsAdapter;
    Bundle b;


    ArrayList<RoomsModel> alRooms;
    ImageView ivCurrent, ivNext, ivNxt1, ivNxt2, ivNxt3, ivNxt4, ivRefresh;
    ImageView ivDCurrent, ivDNext, ivDNxt1, ivDNxt2, ivDNxt3, ivDNxt4, ivCalender;
    ArrayList<MTimeModel> alMTime;
    MTimeAdapter mtimeAdapter;
    ArrayList<MTimeModel> alMTime1;
    ArrayList<MTimeModel> alMTime2;
    ArrayList<String> alString;
    ArrayList<NTimeModel> alNTime;
    ArrayList<ETimeModel> alETime;

    TextView tvStartTime, tvEndTime;
    int stSortingOrder, etSortingOrder;
    FrameLayout fmlProduct;

    int liSelectedFirst = -1,
            liSelected = -1;
    private static final int PICKFILE_RESULT_CODE = 50;
    TransperantProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.capture_details);

        context = this;
        activity = this;

        fmlProduct = (FrameLayout) findViewById(R.id.fmlProduct);
        ivLogout = (ImageView) findViewById(R.id.ivLogout);

        ivLogout.setVisibility(View.VISIBLE);
        fmlProduct.setVisibility(View.GONE);

        initLayout();
    }

    public void initLayout() {

        lovCityAdapter = new LovCityAdapter(context);
        lovOtherCityAdapter = new LovOtherCityAdapter(context);
        officeAdapter = new OfficeAdapter(context);

        alDeviceKeys = new ArrayList<>();
        alSortingOrderData = new ArrayList<>();
        alString = new ArrayList<>();
        alLovCity = new ArrayList<>();
        alLovOtherCity = new ArrayList<>();
        alOffice = new ArrayList<>();
        alAmenities = new ArrayList<>();
        alRooms = new ArrayList<>();
        alMTime = new ArrayList<>();
        alNTime = new ArrayList<>();
        alETime = new ArrayList<>();
        alMTime1 = new ArrayList<>();
        alMTime2 = new ArrayList<>();
        arrayListP = new ArrayList<>();
        arrayListA = new ArrayList<>();
        arrayListS = new ArrayList<>();
        alParticipant = new ArrayList<>();
        alAttachment = new ArrayList<>();
        alServices = new ArrayList<>();
        alServicesPreview = new ArrayList<>();
        alParticipantList = new ArrayList<>();
        alInts = new ArrayList();

        pd = new TransperantProgressDialog(context);

        // getServices();

        switchButton = (Switch) findViewById(R.id.switchButton);
        rcvParticipantsList = (RecyclerView) findViewById(R.id.rcvParticipantsList);
        etParticipantsList = (EditText) findViewById(R.id.etParticipantsList);
        tvReschedule = (TextView) findViewById(R.id.tvReschedule);
        rcvServices = (RecyclerView) findViewById(R.id.rcvServices);
        vwPopularCity = (View) findViewById(R.id.vwPopularCity);
        vwOtherCity = (View) findViewById(R.id.vwOtherCity);
        tvPopularCities = (TextView) findViewById(R.id.tvPopularCities);
        tilCity = (TextInputLayout) findViewById(R.id.tilCity);
        tvOtherCity = (TextView) findViewById(R.id.tvOtherCity);
        etCity = (EditText) findViewById(R.id.etCity);
        etTitle = (EditText) findViewById(R.id.etTitle);
        fbAddAttachments = (FloatingActionButton) findViewById(R.id.fbAddAttachments);
        rcvParticipants = (RecyclerView) findViewById(R.id.rcvParticipants);
        rcvAttachments = (RecyclerView) findViewById(R.id.rcvAttachnments);
        etAgenda = (EditText) findViewById(R.id.etAgenda);
        llAgendaEdit = (LinearLayout) findViewById(R.id.llAgendaEdit);
        llMain = (LinearLayout) findViewById(R.id.llMain);
        llShareDocuments = (LinearLayout) findViewById(R.id.llShareDocuments);
        ivNotification = (ImageView) findViewById(R.id.ivNotification);

        tvPopularCities.setTextColor(getResources().getColor(R.color.font_color));
        vwOtherCity.setVisibility(View.GONE);
        tilCity.setVisibility(View.GONE);

        showParticipant();
        showAttachment();

        llCities = (LinearLayout) findViewById(R.id.llCities);
        ivCurrent = (ImageView) findViewById(R.id.ivCurrent);
        ivNext = (ImageView) findViewById(R.id.ivNext);
        ivNxt1 = (ImageView) findViewById(R.id.ivNxt1);
        ivNxt2 = (ImageView) findViewById(R.id.ivNxt2);
        ivNxt3 = (ImageView) findViewById(R.id.ivNxt3);
        ivNxt4 = (ImageView) findViewById(R.id.ivNxt4);
        ivDCurrent = (ImageView) findViewById(R.id.ivDCurrent);
        ivDNext = (ImageView) findViewById(R.id.ivDNext);
        ivDNxt1 = (ImageView) findViewById(R.id.ivDNxt1);
        ivDNxt2 = (ImageView) findViewById(R.id.ivDNxt2);
        ivDNxt3 = (ImageView) findViewById(R.id.ivDNxt3);
        ivDNxt4 = (ImageView) findViewById(R.id.ivDNxt4);
        flCurrentDt = (FrameLayout) findViewById(R.id.flCurrentDt);
        flNextDt = (FrameLayout) findViewById(R.id.flNextDt);
        flNxt1Dt = (FrameLayout) findViewById(R.id.flNxt1Dt);
        flNxt2Dt = (FrameLayout) findViewById(R.id.flNxt2Dt);
        flNxt3Dt = (FrameLayout) findViewById(R.id.flNxt3Dt);
        flNxt4Dt = (FrameLayout) findViewById(R.id.flNxt4Dt);
        tvCurrentDate = (TextView) findViewById(R.id.tvCurrentDate);
        tvCurrentDay = (TextView) findViewById(R.id.tvCurrentDay);
        tvNextDate = (TextView) findViewById(R.id.tvNextDate);
        tvNextDay = (TextView) findViewById(R.id.tvNextDay);
        tvNxt1Dt = (TextView) findViewById(R.id.tvNxt1Dt);
        tvNxt1Day = (TextView) findViewById(R.id.tvNxt1Day);
        tvNxt2Dt = (TextView) findViewById(R.id.tvNxt2Dt);
        tvNxt2Day = (TextView) findViewById(R.id.tvNxt2Day);
        tvNxt3Dt = (TextView) findViewById(R.id.tvNxt3Dt);
        tvNxt3Day = (TextView) findViewById(R.id.tvNxt3Day);
        ivCalender = (ImageView) findViewById(R.id.ivCalender);
        tvSelectedDate = (TextView) findViewById(R.id.tvSelectedDate);
        tvStartTime = (TextView) findViewById(R.id.tvStartTime);
        tvEndTime = (TextView) findViewById(R.id.tvEndTime);
        rcvCity = (RecyclerView) findViewById(R.id.rcvCity);
        rcvOtherCity = (RecyclerView) findViewById(R.id.rcvOtherCity);

        /*From Workspace.java*/
        ivMeetingHall = (AppCompatImageView) findViewById(R.id.ivMeetingHall);
        ivConferenceHall = (AppCompatImageView) findViewById(R.id.ivConferenceHall);
        ivBoardRoom = (AppCompatImageView) findViewById(R.id.ivBoardRoom);
        ivTrainingHall = (AppCompatImageView) findViewById(R.id.ivTrainingHall);
        ivMeetingHallPressed = (AppCompatImageView) findViewById(R.id.ivMeetingHallPressed);
        ivConferenceHallPressed = (AppCompatImageView) findViewById(R.id.ivConferenceHallPressed);
        ivBoardRoomPressed = (AppCompatImageView) findViewById(R.id.ivBoardRoomPressed);
        ivTrainingHallPressed = (AppCompatImageView) findViewById(R.id.ivTrainingHallPressed);
        llMeetingRoom = (LinearLayout) findViewById(R.id.llMeetingRoom);
        llConferenceHall = (LinearLayout) findViewById(R.id.llConferenceHall);
        llBoardRoom = (LinearLayout) findViewById(R.id.llBoardRoom);
        llTrainingHall = (LinearLayout) findViewById(R.id.llTrainingHall);
        tvPreview = (TextView) findViewById(R.id.tvPreview);
        rcvMTime = (RecyclerView) findViewById(R.id.rcvMTime);
        vwView = (View) findViewById(R.id.vwView);
        llHome = (LinearLayout) findViewById(R.id.llHome);
        llFilter = (LinearLayout) findViewById(R.id.llFilter);
        llRoomDetails = (LinearLayout) findViewById(R.id.llRoomDetails);
        llBooking = (LinearLayout) findViewById(R.id.llBooking);
        llAgenda = (LinearLayout) findViewById(R.id.llAgenda);
        llAttachment = (LinearLayout) findViewById(R.id.llAttachment);
        llParticipant = (LinearLayout) findViewById(R.id.llParticipant);
        llServices = (LinearLayout) findViewById(R.id.llServices);
        tvBack = (TextView) findViewById(R.id.tvBack);
        tvNext = (TextView) findViewById(R.id.tvNext);
        tvSubmit = (TextView) findViewById(R.id.tvSubmit);
        rcvOffices = (RecyclerView) findViewById(R.id.rcvOffices);
        rcvRooms = (RecyclerView) findViewById(R.id.rcvRooms);
        ivRefresh = (ImageView) findViewById(R.id.ivRefresh);

        rcvOffices.setVisibility(View.GONE);
        setTitle("Select Workspace Type");
        llServices.setVisibility(View.GONE);
        llAgenda.setVisibility(View.GONE);
        llAttachment.setVisibility(View.GONE);
        llFilter.setVisibility(View.GONE);
        llRoomDetails.setVisibility(View.GONE);
        llBooking.setVisibility(View.GONE);
        tvBack.setVisibility(View.VISIBLE);
        tvPreview.setVisibility(View.GONE);
        vwView.setVisibility(View.GONE);
        llParticipant.setVisibility(View.GONE);
        tvNext.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f));
        rcvOtherCity.setVisibility(View.GONE);

        detSet();

        sp = getSharedPreferences(App.LOGINCREDENTIALS, context.MODE_PRIVATE);
        lsEmployeeId = sp.getString(UserLogin.UL_EMPLOYEEID, "");
        lsEmail = sp.getString(UserLogin.UL_EMAIL, "");

        etParticipantsList.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String lsText = editable.toString();

                if (lsText.length() > 0) {
                    rcvParticipantsList.setVisibility(View.VISIBLE);
                    getParticipantsList(lsText);
                } else {
                    rcvParticipantsList.setVisibility(View.GONE);
                    alParticipantList.clear();
                }

            }
        });

        etCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String lsText = editable.toString();
                if (lsText.length() > 2) {
                    rcvOtherCity.setVisibility(View.VISIBLE);
                    getOtherCities(lsText);
                    getOtherCities(lsText);
                } else {
                    rcvOffices.setVisibility(View.GONE);
                    alLovOtherCity.clear();
                    rcvOtherCity.setVisibility(View.GONE);
                }

            }
        });

        switchButton.setChecked(false);

        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (switchButton.isChecked()) {
                    lsConfidential = "y";
                    // Toast.makeText(context,"yes",Toast.LENGTH_SHORT).show();
                    // Toast.makeText(context,"Confidential "+lsConfidential,Toast.LENGTH_SHORT).show();
                    switchButton.setSwitchTextAppearance(context, R.style.switchTextAppearance);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        switchButton.getThumbDrawable().setColorFilter(context.getResources().getColor(R.color.app_color_red), PorterDuff.Mode.MULTIPLY);
                        switchButton.getTrackDrawable().setColorFilter(context.getResources().getColor(R.color.red_100), PorterDuff.Mode.MULTIPLY);
                    }
                    //   changeColor(bChecked);
                } else {
                    lsConfidential = "n";
                    // Toast.makeText(context,"no",Toast.LENGTH_SHORT).show();
                    //  Toast.makeText(context,"Confidential "+lsConfidential,Toast.LENGTH_SHORT).show();
                    switchButton.setSwitchTextAppearance(context, context.getResources().getColor(R.color.LightGray));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        switchButton.getThumbDrawable().setColorFilter(context.getResources().getColor(R.color.White), PorterDuff.Mode.MULTIPLY);
                        switchButton.getTrackDrawable().setColorFilter(context.getResources().getColor(R.color.LightGray), PorterDuff.Mode.MULTIPLY);
                    }
                    //   changeColor(bChecked);
                }
            }
        });

        flCurrentDt.setOnClickListener(this);
        flNextDt.setOnClickListener(this);
        flNxt1Dt.setOnClickListener(this);
        flNxt2Dt.setOnClickListener(this);
        flNxt3Dt.setOnClickListener(this);
        flNxt4Dt.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);
        ivRefresh.setOnClickListener(this);
        llMeetingRoom.setOnClickListener(this);
        llConferenceHall.setOnClickListener(this);
        llTrainingHall.setOnClickListener(this);
        llBoardRoom.setOnClickListener(this);
        fbAddAttachments.setOnClickListener(this);
        tvPopularCities.setOnClickListener(this);
        tvOtherCity.setOnClickListener(this);

        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((ivMeetingHallPressed.getVisibility() == View.VISIBLE) || (ivConferenceHallPressed.getVisibility() == View.VISIBLE) || (ivTrainingHallPressed.getVisibility() == View.VISIBLE) || (ivBoardRoomPressed.getVisibility() == View.VISIBLE)) {
                    setButtonViews(view);
                } else {
                    CustomToast.showToast(activity, "Please Select Workspace", 50);
                }
            }
        });


        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setButtonViews(view);
            }
        });

        tvPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setButtonViews(view);
            }
        });

    }

    private void changeColor(boolean checked) {

    }

   /* private void changeColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            int thumbColor = 0;
            int trackColor = 0;

            if(switchButton.isChecked()) {
                thumbColor = Color.argb(235, 220, 143, 0);
                trackColor = thumbColor;
            }*//* else {
                thumbColor = Color.argb(255, 236, 236, 236);
                trackColor = Color.argb(255, 0, 0, 0);
            }*//*

            try {
                switchButton.getThumbDrawable().setColorFilter(thumbColor, PorterDuff.Mode.MULTIPLY);
                switchButton.getTrackDrawable().setColorFilter(trackColor, PorterDuff.Mode.MULTIPLY);
            }
            catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }*/

    public void detSet() {
        tvReschedule.setVisibility(View.GONE);

        String date = CustomServices.getFormattedDate();
        SimpleDateFormat sdf1 = new SimpleDateFormat("EEE");
        Date date1 = new Date();
        String day = sdf1.format(date1);

        gDate = CustomServices.getTodaysDate();

        tvCurrentDate.setText(date);
        tvCurrentDay.setText(day);

        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, 1); // <--
        Date tomorrow = cal.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");

        String nxtDate = sdf.format(tomorrow);
        String nxtDay = sdf1.format(tomorrow);

        gNxtDate = sdf2.format(tomorrow);

        tvNextDate.setText(nxtDate);
        tvNextDay.setText(nxtDay);

        cal.add(Calendar.DAY_OF_YEAR, 1); // <--
        Date tomorrow1 = cal.getTime();
        String nxtDate1 = sdf.format(tomorrow1);
        String nxtDay1 = sdf1.format(tomorrow1);

        gNxtDate1 = sdf2.format(tomorrow1);

        tvNxt1Dt.setText(nxtDate1);
        tvNxt1Day.setText(nxtDay1);

        cal.add(Calendar.DAY_OF_YEAR, 1); // <--
        Date tomorrow2 = cal.getTime();
        String nxtDate2 = sdf.format(tomorrow2);
        String nxtDay2 = sdf1.format(tomorrow2);

        gNxtDate2 = sdf2.format(tomorrow2);

        tvNxt2Dt.setText(nxtDate2);
        tvNxt2Day.setText(nxtDay2);

        cal.add(Calendar.DAY_OF_YEAR, 1); // <--
        Date tomorrow3 = cal.getTime();
        String nxtDate3 = sdf.format(tomorrow3);
        String nxtDay3 = sdf1.format(tomorrow3);

        gNxtDate3 = sdf2.format(tomorrow3);

        tvNxt3Dt.setText(nxtDate3);
        tvNxt3Day.setText(nxtDay3);

        tvSelectedDate.setText(gDate);

        ivDCurrent.setVisibility(View.GONE);
        ivDNext.setVisibility(View.GONE);
        ivDNxt1.setVisibility(View.GONE);
        ivDNxt2.setVisibility(View.GONE);
        ivDNxt3.setVisibility(View.GONE);
        ivDNxt4.setVisibility(View.GONE);

        ivCurrent.setColorFilter(getResources().getColor(R.color.app_color_red));
        tvCurrentDay.setTextColor(getResources().getColor(R.color.White));
        tvCurrentDate.setTextColor(getResources().getColor(R.color.White));
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.llMeetingRoom:
                DefaultSelection();
                ivMeetingHallPressed.setVisibility(View.VISIBLE);
                ivMeetingHall.setVisibility(View.GONE);
                lsType = "meeting";
                break;

            case R.id.llConferenceHall:
                DefaultSelection();
                ivConferenceHallPressed.setVisibility(View.VISIBLE);
                ivConferenceHall.setVisibility(View.GONE);
                lsType = "conference";
                break;

            case R.id.llBoardRoom:
                DefaultSelection();
                ivBoardRoom.setVisibility(View.GONE);
                ivBoardRoomPressed.setVisibility(View.VISIBLE);
                lsType = "board";
                break;

            case R.id.llTrainingHall:
                DefaultSelection();
                ivTrainingHallPressed.setVisibility(View.VISIBLE);
                ivTrainingHall.setVisibility(View.GONE);
                lsType = "training";
                break;

            case R.id.flCurrentDt:

                setDate(gDate, ivDCurrent, ivCurrent, tvCurrentDay, tvCurrentDate);

                break;

            case R.id.flNextDt:

                setDate(gNxtDate, ivDNext, ivNext, tvNextDay, tvNextDate);

                break;

            case R.id.flNxt1Dt:

                setDate(gNxtDate1, ivDNxt1, ivNxt1, tvNxt1Day, tvNxt1Dt);

                break;

            case R.id.flNxt2Dt:

                setDate(gNxtDate2, ivDNxt2, ivNxt2, tvNxt2Day, tvNxt2Dt);

                break;

            case R.id.flNxt3Dt:

                setDate(gNxtDate3, ivDNxt3, ivNxt3, tvNxt3Day, tvNxt3Dt);

                break;

            case R.id.flNxt4Dt:

                setUpDate();
                defaultDatesColor();
                ivDNxt4.setVisibility(View.GONE);
                ivNxt4.setColorFilter(getResources().getColor(R.color.app_color_red));
                ivCalender.setColorFilter(getResources().getColor(R.color.White));

                if (mtimeAdapter.getSelectedCount() > 0) {
                    mtimeAdapter.clearSelection();
                    tvEndTime.setText("");
                    tvStartTime.setText("");
                    startTime = "";
                    endTime = "";
                } else {
                    //  Toast.makeText(context, "nothing selected", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.fbAddAttachments:

                showFileChooser();

                break;

            case R.id.tvOtherCity:
                tvOtherCity.setTextColor(getResources().getColor(R.color.font_color));
                tvPopularCities.setTextColor(getResources().getColor(R.color.Gray));
                vwPopularCity.setVisibility(View.GONE);
                vwOtherCity.setVisibility(View.VISIBLE);
                rcvCity.setVisibility(View.GONE);
                tilCity.setVisibility(View.VISIBLE);

                if (lovCityAdapter.getSelectedCount() > 0) {
                    lovCityAdapter.clearSelection();
                    lsCityValue = "";
                    lsCityDisplayValue = "";
                }

                if (rcvOffices.getVisibility() == View.VISIBLE) {
                    rcvOffices.setVisibility(View.GONE);
                }
                break;

            case R.id.tvPopularCities:
                tvPopularCities.setTextColor(getResources().getColor(R.color.font_color));
                tvOtherCity.setTextColor(getResources().getColor(R.color.Gray));
                vwOtherCity.setVisibility(View.GONE);
                vwPopularCity.setVisibility(View.VISIBLE);
                rcvCity.setVisibility(View.VISIBLE);
                tilCity.setVisibility(View.GONE);
                etCity.setText("");

                if (rcvOffices.getVisibility() == View.VISIBLE) {
                    rcvOffices.setVisibility(View.GONE);
                }

                lsCityValue = "";
                lsOfficeValue = "";


                break;

            case R.id.tvSubmit:
                if (callSave() > 0) {
                    if (callParticipantsSave() > 0) {
                        if (callServicesSave() > 0) {

                            if (alAttachment.size() > 0) {
                                uploadFiles();
                            }

                            String lsMessage = "Meeting Scheduled on "+lsMeetingDate;
                            sendSamplePush(lsMessage);

                            if (lsDate == gDate) {
                                sendTabNotification();
                            }

                            SimpleDateFormat f = new SimpleDateFormat("dd-MMM-yyyy");
                            try {
                                Date d = f.parse(lsMeetingDate);
                                strtTime = d.getTime();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            if(checkPermissions(callbackId, Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR))
                            {
                              //  pushAppointmentsToCalender(this,lsTitle,lsAgenda,"Mumbai",1,strtTime,true,false);
                              //  pushAppointmentToCalender();
                                addEvents();
                            }

                            Intent intent = new Intent(this, SuccessPage.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(context, "Invalid Services", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "Invalid Participants", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(context, "Invalid", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.ivRefresh:
                if (mtimeAdapter.getSelectedCount() > 0) {
                    mtimeAdapter.clearSelection();
                    tvStartTime.setText("");
                    tvEndTime.setText("");
                    startTime = "";
                    endTime = "";
                }
                break;
        }

    }

    private boolean checkPermissions(int callbackId, String... permissionsId) {
        boolean permissions = true;
        for (String p : permissionsId) {
            permissions = permissions && ContextCompat.checkSelfPermission(this, p) == PERMISSION_GRANTED;
        }

        if (!permissions)
            ActivityCompat.requestPermissions(this, permissionsId, callbackId);
        return permissions;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setDate(String date, ImageView ivImg1, ImageView ivImg2, TextView tvText1, TextView tvText2) {
        tvSelectedDate.setText(date);
        defaultDatesColor();

        getMTimeLovs(date);
        lsDate = date;

        ivImg1.setVisibility(View.GONE);
        ivImg2.setColorFilter(getResources().getColor(R.color.app_color_red));
        tvText1.setTextColor(getResources().getColor(R.color.White));
        tvText2.setTextColor(getResources().getColor(R.color.White));

        if (mtimeAdapter.getSelectedCount() > 0) {
            mtimeAdapter.clearSelection();
            tvEndTime.setText("");
            tvStartTime.setText("");
            startTime = "";
            endTime = "";
        } else {

        }
    }

    private void showFileChooser() {

        Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
        chooseFile.setType("*/*");
        Intent intentCozi = Intent.createChooser(chooseFile, "Choose a file");

        try {
            startActivityForResult(intentCozi, PICKFILE_RESULT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case PICKFILE_RESULT_CODE:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    String filePath = uri.getPath();
                    File file = new File(filePath);
                    String fname = file.getName();

                    if (alString.contains(fname)) {
                        //  Toast.makeText(context, fname + " already exist , Select another one.", Toast.LENGTH_SHORT).show();
                        CustomToast.showToast(this, fname + " already exist , Select another one.", 50);
                    } else {
                        Bundle b = new Bundle();
                        b.putString(MeetingPreread.MPR_FILE, fname);

                        AttachmentModel model = new AttachmentModel(b);

                        alAttachment.add(model);
                        alString.add(fname);

                        attachmentAdapter.notifyItemInserted(alAttachment.size() - 1);
                    }

                    try {
                        zip(uri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    public void zip(Uri filepath) throws IOException {

        String filePath = filepath.getPath();
        File file = new File(filePath);
        String content = "hello world";

        String fName = file.getName();
        FileOutputStream fos = null;

        File exportDir = new File(App.getImageDir());
        if (!exportDir.exists()) {
            boolean lbFolderCreated = exportDir.mkdirs();
            Debugger.debug(TAG, "Folder Created " + lbFolderCreated);
        }

        File file1 = new File(exportDir, fName);

        try {
            fos = new FileOutputStream(file1);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Debugger.debug(TAG, "Exception in OpenFileOutput " + e.toString());
        }

/* if (b != null) {
            if (b.compress(Bitmap.CompressFormat.PNG, 95, fos)) {

            }
        }
*/

        try {
            fos.flush();
            fos.close();
        } catch (Exception e) {
            Debugger.debug(TAG, "Exception in Flushing " + e.toString());
        }
    }


    public int callServicesSave() {

        ArrayList<Integer> selectedItems = serviceAdapter.getSelectedItems();
        serviceList = new ArrayList<>();
        for (int i = 0; i < selectedItems.size(); i++) {
            int liServiceId = alServices.get(selectedItems.get(i)).getId();

            String lsSql = "INSERT INTO " + WorkspaceServices.TABLENAME +
                    " ( " + WorkspaceServices.WS_BOOKINGID + "," + WorkspaceServices.WS_SERVICEID + " ) " +
                    "VALUES ( '" + lsEmployee + "','" + liServiceId + "' )";

            arrayListS.add(lsSql);
        }


        callCloud(arrayListS);
        return 1;
    }

    public int callParticipantsSave() {
        for (int i = 0; i < alParticipant.size(); i++) {
            Bundle b = alParticipant.get(i).getData();

            String lsParticipantsEmail = b.getString(MeetingParticipants.MP_EMPLOYEEEMAIL);

            String lsSql = "INSERT INTO " + MeetingParticipants.TABLENAME +
                    " ( " + MeetingParticipants.MP_BOOKINGID + "," + MeetingParticipants.MP_EMPLOYEEEMAIL + " ) " +
                    "VALUES ( '" + lsEmployee + "','" + lsParticipantsEmail + "' )";

            arrayListP.add(lsSql);

        }

        if (alParticipant.size() > 0) {
            callCloud(arrayListP);
            return 1;
        } else {
            CustomToast.showToast(activity, "Please add Participants", 50);
            return -1;
        }

    }

    public int callAttachmentSave() {
        for (int i = 0; i < alAttachment.size(); i++) {
            Bundle b = alAttachment.get(i).getData();

            lsAttachment = b.getString(MeetingPreread.MPR_FILE);

            String lsSql = "INSERT INTO " + MeetingPreread.TABLENAME +
                    " ( " + MeetingPreread.MPR_BOOKINGID + "," + MeetingPreread.MPR_FILE + " ) " +
                    "VALUES ( '" + lsEmployee + "','" + lsAttachment + "' )";

            arrayListA.add(lsSql);

            //  App.uploadOKVideos(lsAttachment, 0, 0);
        }


        callCloud(arrayListA);
        return 1;
    }

    public void uploadFiles() {
        ArrayList<String> arrayList = new ArrayList<>();
        String lsSql;

        for (int i = 0; i < alString.size(); i++) {
            String lsImage = (String) alString.get(i);

            lsSql = "INSERT INTO " + MeetingPreread.TABLENAME +
                    " ( " + MeetingPreread.MPR_BOOKINGID + "," + MeetingPreread.MPR_FILE + " ) " +
                    "VALUES ( '" + lsEmployee + "','" + lsImage + "' )";

            arrayList.add(lsSql);
        }

        //   ShowProgressDialog.setTitle("uploading...");
        //   ShowProgressDialog.showProgress(context);

        callCloud(arrayList, 0);
    }

    public void callCloud(ArrayList alSql, final long llProductId) {
        DataTransmitter dt = new DataTransmitter();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        String lsSql;
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);

            dt.setLink(App.getSQLURL());
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            } else {

            }
        }

        for (int i = 0; i < alString.size(); i++) {
            String lsImage = (String) alString.get(i);
            App.uploadOKImage(lsImage, context, 0, 0);
        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ShowProgressDialog.hideProgressDialog(context);
                // Toast.makeText(context, "Files uploaded successfully", Toast.LENGTH_SHORT).show();
                alAttachment.clear();
                alString.clear();
                // alLong.clear();
                // etDescription.setText("");
                attachmentAdapter.notifyDataSetChanged();
            }
        }, 2000);

    }

    public int callSave() {
        ArrayList<String> arrayList = new ArrayList<>();
        String lsSql;

        lsEmployee = lsEmployeeId + "-" + App.getCurrentTimeStamp();
        lsTodaysDate = App.getDate();
        lsMeetingDate = tvSelectedDate.getText().toString();

        lsSql = "INSERT INTO " + WorkspaceBooked.TABLENAME + "(" + WorkspaceBooked.WB_BOOKINGID + "," + WorkspaceBooked.WB_EMPLOYEE + "," + WorkspaceBooked.WB_WORKSPACEID + "," +
                WorkspaceBooked.WB_MEETINGDATE + "," + WorkspaceBooked.WB_BOOKINGDATE + "," + WorkspaceBooked.WB_PLANNEDSTARTTIME + "," + WorkspaceBooked.WB_PLANNEDENDTIME + "," + WorkspaceBooked.WB_ST_SO + "," + WorkspaceBooked.WB_ET_SO + "," + WorkspaceBooked.WB_AGENDA + "," + WorkspaceBooked.WB_STATUS + "," + WorkspaceBooked.WB_BOOKING_STATUS + "," + WorkspaceBooked.WB_CONFIDENTIAL + "," + WorkspaceBooked.WB_TITLE + ") VALUES " +
                "('" + lsEmployee + "'" + "," + "'" + lsEmployeeId + "'" + "," + "'" + liRSId + "'" + "," + "'" + lsMeetingDate + "'" + "," + "'" + lsTodaysDate + "'" + "," + "'" + startTime + "'" + "," + "'" + endTime + "'" + "," + "'" + stSortingOrder + "'" + "," + "'" + etSortingOrder + "'" + "," + "'" + lsAgenda + "'" + "," + "'" + App.STATUS + "'," + "'" + App.BOOKING_STATUS + "'," + "'" + lsConfidential + "'," + "'" + lsTitle + "'" + ")";


        arrayList.add(lsSql);


        if (lsAgenda.length() > 0) {
            callCloud(arrayList);
            return 1;
        } else {
            CustomToast.showToast(activity, "fill agenda", 50);
            return -1;
        }
    }

    public void callCloud(ArrayList alSql) {

        DataTransmitter dt = new DataTransmitter();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        String lsSql;
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);

            dt.setLink(App.getSQLURL());
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            } else {
                /*
                CloudPending cp = new CloudPending(context);
                ContentValues cv = new ContentValues();
                cv.put(CloudPending.CP_OPERATION, jsonArray.toString());
                cv.put(CloudPending.CP_TIME, CustomServices.getCurrentTimeStamp());
                cv.put(CloudPending.CP_FILE, "N");
                cv.put(CloudPending.CP_UPLOADED, "N");
                cv.put(CloudPending.CP_URL, Posita.getSQLURL(context));
                cp.CRUD(cv);
                */
            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void showAlertDialog() {

        final MyDialog dialog = new MyDialog(activity, context);
        CustomServices.hideSoftKeyboard(this);
        dialog.setCancel(false);
        dialog.setOutsideTouchable(false);
        dialog.setTitle("Medicines");
        dialog.showDialog();
        //svParticipant.setBackgroundColor(context.getResources().getColor(R.color.transparent_background));


        dialog.setDialogButtonClickListener(new MyDialog.DialogButtonClick() {
            @Override
            public void DialogButtonClicked(View view, String lsParticipant) {
                switch (view.getId()) {
                    case R.id.cbOK:
                        svParticipant.setBackgroundColor(context.getResources().getColor(R.color.White));

                        if (lsParticipant.length() > 0) {
                            if ((lsParticipant.contains("@"))) {
                                Bundle b = new Bundle();

                                b.putString(MeetingParticipants.MP_EMPLOYEEEMAIL, lsParticipant);

                                ParticipantModel model = new ParticipantModel(b);
                                alParticipant.add(model);

                                // participantAdapter.notifyItemInserted(alParticipant.size() - 1);


                            } else {
                                CustomToast.showToast(activity, "Provide valid e-mail", 50);
                            }

                        } else {
                            CustomToast.showToast(activity, "Participant e-mail please", 50);
                        }

                        break;

                    case R.id.cbCancel:
                        dialog.dismiss();
                        svParticipant.setBackgroundColor(context.getResources().getColor(R.color.White));
                        break;
                }
            }
        });
    }

    public void showParticipant() {
        llmParticipant = new LinearLayoutManager(context);
        rcvParticipants.setLayoutManager(llmParticipant);
        participantAdapter = new ParticipantAdapter(context, alParticipant);
        rcvParticipants.setAdapter(participantAdapter);
    }

    public void showAttachment() {
        llmAttachment = new LinearLayoutManager(context);
        rcvAttachments.setLayoutManager(llmAttachment);
        attachmentAdapter = new AttachmentAdapter(context, alAttachment);
        rcvAttachments.setAdapter(attachmentAdapter);
    }

    public void setUpDate() {
        CalendarView calendarPop = new CalendarView(activity, context, true);
        String lsDate = tvSelectedDate.getText().toString();
        if (lsDate == null || lsDate.equals("")) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            lsDate = String.valueOf(sdf.format(new Date()));
        }

        CalendarView.CVOnDateChanged mOnDateChanged = new CalendarView.CVOnDateChanged() {
            @Override
            public void CVDateChanged(TextView tvDate, String lsDate) {
                Debugger.debug(TAG, "DAte selected is " + lsDate);
                tvSelectedDate.setText(lsDate);
                getMTimeLovs(lsDate);
            }
        };
        calendarPop.setOnDateChanged(mOnDateChanged);
        calendarPop.setupPopUp(tvSelectedDate, lsDate, context.getResources().getColor(R.color.colorPrimary));
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void defaultDatesColor() {

        ivDCurrent.setVisibility(View.VISIBLE);
        ivDNext.setVisibility(View.VISIBLE);
        ivDNxt1.setVisibility(View.VISIBLE);
        ivDNxt2.setVisibility(View.VISIBLE);
        ivDNxt3.setVisibility(View.VISIBLE);
        ivDNxt4.setVisibility(View.VISIBLE);


        ivCurrent.setColorFilter(getResources().getColor(R.color.White));
        tvCurrentDay.setTextColor(getResources().getColor(R.color.app_color_red));
        tvCurrentDate.setTextColor(getResources().getColor(R.color.app_color_red));

        ivNext.setColorFilter(getResources().getColor(R.color.White));
        tvNextDay.setTextColor(getResources().getColor(R.color.app_color_red));
        tvNextDate.setTextColor(getResources().getColor(R.color.app_color_red));

        ivNxt1.setColorFilter(getResources().getColor(R.color.White));
        tvNxt1Dt.setTextColor(getResources().getColor(R.color.app_color_red));
        tvNxt1Day.setTextColor(getResources().getColor(R.color.app_color_red));

        ivNxt2.setColorFilter(getResources().getColor(R.color.White));
        tvNxt2Day.setTextColor(getResources().getColor(R.color.app_color_red));
        tvNxt2Dt.setTextColor(getResources().getColor(R.color.app_color_red));

        ivNxt3.setColorFilter(getResources().getColor(R.color.White));
        tvNxt3Day.setTextColor(getResources().getColor(R.color.app_color_red));
        tvNxt3Dt.setTextColor(getResources().getColor(R.color.app_color_red));

        ivNxt4.setColorFilter(getResources().getColor(R.color.White));
        ivCalender.setColorFilter(getResources().getColor(R.color.app_color_red));
    }

    public void sendTabNotification() {
        final String devicetoken = "5960c94a17c750ec821d58";
        // final String deviceToken2 = "fec39755f325b37c6633b1";
        final String deviceToken2 = "55701cba44df849ae682c7";


        sendNotTab1(devicetoken);
        sendNotTab2(deviceToken2);
    }

    public void sendNotTab1(final String devicetoken) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.POST_NOTIFICATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "Response" + response);
                        if (response.contains("no_message")) ;
                        JSONObject jsonObject = null;

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("devicetoken", devicetoken);
                param.put("message", "Refresh");
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    public void sendNotTab2(final String devicetoken) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.POST_NOTIFICATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "Response" + response);
                        if (response.contains("no_message")) ;
                        JSONObject jsonObject = null;

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("devicetoken", devicetoken);
                param.put("message", "Refresh");
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    public void setButtonViews(View view) {
        if (view.getId() == R.id.tvNext) {
            if (llHome.getVisibility() == View.VISIBLE) {
                setTitle("Select Workspace Location");
                getCity();

                tvBack.setVisibility(View.VISIBLE);
                tvNext.setVisibility(View.VISIBLE);
                tvPreview.setVisibility(View.VISIBLE);
                vwView.setVisibility(View.VISIBLE);
                tvNext.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1 / 2f));
                tvPreview.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1 / 2f));
                llHome.setVisibility(View.GONE);
                llFilter.setVisibility(View.VISIBLE);
                llCities.setVisibility(View.VISIBLE);

            } else if (llFilter.getVisibility() == View.VISIBLE) {
                if (lsCityValue.length() > 0) {
                    lsOfficeValue = "";
                    if (officeAdapter.getSelectedCount() > 0) {
                        ArrayList<Integer> selectedItems = officeAdapter.getSelectedItems();
                        for (int i = 0; i < selectedItems.size(); i++) {
                            lsOfficeValue = alOffice.get(selectedItems.get(i)).getOffice();
                            break;
                        }
                        setTitle("Select Meeting Workspace");
                        CustomServices.hideSoftKeyboard(this);
                        tvBack.setVisibility(View.VISIBLE);
                        tvNext.setVisibility(View.VISIBLE);
                        tvPreview.setVisibility(View.VISIBLE);
                        vwView.setVisibility(View.VISIBLE);
                        tvNext.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1 / 2f));
                        tvPreview.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1 / 2f));
                        llFilter.setVisibility(View.GONE);
                        llRoomDetails.setVisibility(View.VISIBLE);
                        getRooms(lsCityValue, lsType, lsOfficeValue);
                    } else {
                        CustomToast.showToast(this, "Select Office", 50);
                    }
                } else {
                    CustomToast.showToast(this, "Select City", 50);
                }

            } else if (llRoomDetails.getVisibility() == View.VISIBLE) {
                if (roomsAdapter.getSelectedCount() > 0) {

                    lsRoomName = "";
                    lsRoomCapacity = "";
                    ArrayList<Integer> selectedItems = roomsAdapter.getSelectedItems();
                    for (int i = 0; i < selectedItems.size(); i++) {
                        lsRoomName = alRooms.get(selectedItems.get(i)).getName();
                        lsRoomCapacity = alRooms.get(selectedItems.get(i)).getCapacity();
                        break;
                    }

                    String lsDate = CustomServices.getTodaysDate();
                    getMTimeLovs(lsDate);
                    setTitle("Select Meeting Schedule");

                    CustomServices.hideSoftKeyboard(this);
                    tvBack.setVisibility(View.VISIBLE);
                    tvPreview.setVisibility(View.VISIBLE);
                    tvNext.setVisibility(View.VISIBLE);
                    vwView.setVisibility(View.VISIBLE);
                    tvSubmit.setVisibility(View.GONE);
                    tvNext.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1 / 2f));
                    llRoomDetails.setVisibility(View.GONE);
                    llBooking.setVisibility(View.VISIBLE);
                } else {
                    CustomToast.showToast(activity, "Please Select the Room", 50);
                }

            } else if (llBooking.getVisibility() == View.VISIBLE) {
                if (tvStartTime.length() > 0) {
                    if (tvEndTime.length() > 0) {
                        lsDate = tvSelectedDate.getText().toString();

                        setTitle("Add Agenda");
                        CustomServices.hideSoftKeyboard(activity);

                        if (lsType.equals("meeting")) {
                            etTitle.setHint("Meeting Title");
                        } else if (lsType.equals("conference")) {
                            etTitle.setHint("Conference Title");
                        } else if (lsType.equals("board")) {
                            etTitle.setHint("Board Title");
                        } else if (lsType.equals("training")) {
                            etTitle.setHint("Training Title");
                        }

                        // startTime = tvStartTime.getText().toString();
                        // lsEndTime = tvEndTime.getText().toString();

                        tvBack.setVisibility(View.VISIBLE);
                        tvPreview.setVisibility(View.VISIBLE);
                        tvNext.setVisibility(View.VISIBLE);
                        vwView.setVisibility(View.VISIBLE);
                        tvSubmit.setVisibility(View.GONE);
                        llBooking.setVisibility(View.GONE);
                        llAgenda.setVisibility(View.VISIBLE);

                    } else {
                        CustomToast.showToast(activity, "select end-time of meeting", 50);
                    }
                } else {
                    CustomToast.showToast(activity, "select start-time of meeting", 50);
                }

            } else if (llAgenda.getVisibility() == View.VISIBLE) {
                if (etTitle.length() > 0 && etAgenda.length() > 0) {
                    setTitle("Add Participant");

                    Bundle b = new Bundle();
                    b.putString(MeetingParticipants.MP_EMPLOYEEEMAIL, lsEmail);
                    ParticipantModel modelForMe = new ParticipantModel(b);
                    alParticipant.add(modelForMe);

                    lsAgenda = etAgenda.getText().toString();
                    lsTitle = etTitle.getText().toString();
                    CustomServices.hideSoftKeyboard(this);
                    tvBack.setVisibility(View.VISIBLE);
                    tvPreview.setVisibility(View.VISIBLE);
                    tvNext.setVisibility(View.VISIBLE);
                    vwView.setVisibility(View.VISIBLE);
                    tvSubmit.setVisibility(View.GONE);
                    llAgenda.setVisibility(View.GONE);
                    llParticipant.setVisibility(View.VISIBLE);
                } else {
                    CustomToast.showToast(activity, "Provide Title and Agenda", 50);
                }
            } else if (llParticipant.getVisibility() == View.VISIBLE) {
                if (alParticipant.size() > 1) {
                    setTitle("Add Attachment");
                    CustomServices.hideSoftKeyboard(this);
                    tvBack.setVisibility(View.VISIBLE);
                    tvPreview.setVisibility(View.VISIBLE);
                    tvNext.setVisibility(View.VISIBLE);
                    vwView.setVisibility(View.VISIBLE);
                    tvSubmit.setVisibility(View.GONE);
                    llParticipant.setVisibility(View.GONE);
                    llAttachment.setVisibility(View.VISIBLE);
                } else {
                    CustomToast.showToast(activity, "Please add at least one participant", 50);
                }
            } else if (llAttachment.getVisibility() == View.VISIBLE) {
                setTitle("Select Services");
                getServices();
                CustomServices.hideSoftKeyboard(this);
                tvBack.setVisibility(View.VISIBLE);
                tvPreview.setVisibility(View.VISIBLE);
                tvNext.setVisibility(View.GONE);
                vwView.setVisibility(View.VISIBLE);
                tvSubmit.setVisibility(View.VISIBLE);
                tvSubmit.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1 / 2f));
                llAttachment.setVisibility(View.GONE);
                llServices.setVisibility(View.VISIBLE);
            }

        } else if (view.getId() == R.id.tvBack) {

            backClick();

        } else if (view.getId() == R.id.tvPreview) {
            if (llFilter.getVisibility() == View.VISIBLE) {
                if (lsCityValue.length() > 0) {

                    lsOfficeValue = "";
                    if (officeAdapter.getSelectedCount() > 0) {
                        ArrayList<Integer> selectedItems = officeAdapter.getSelectedItems();
                        for (int i = 0; i < selectedItems.size(); i++) {
                            lsOfficeValue = alOffice.get(selectedItems.get(i)).getOffice();
                            break;
                        }
                        Intent intent = new Intent(this, Preview.class);
                        CustomServices.hideSoftKeyboard(this);

                        putintentExtra(intent);

                        startActivity(intent);
                    } else {
                        CustomToast.showToast(activity, "Select office", 50);
                    }

                } else {
                    CustomToast.showToast(activity, "Select City", 50);
                }

            } else if (llRoomDetails.getVisibility() == View.VISIBLE) {
                if (roomsAdapter.getSelectedCount() > 0) {

                    lsRoomName = "";
                    lsRoomCapacity = "";
                    ArrayList<Integer> selectedItems = roomsAdapter.getSelectedItems();
                    for (int i = 0; i < selectedItems.size(); i++) {
                        lsRoomName = alRooms.get(selectedItems.get(i)).getName();
                        lsRoomCapacity = alRooms.get(selectedItems.get(i)).getCapacity();
                        break;
                    }

                    Intent intent = new Intent(this, Preview.class);
                    CustomServices.hideSoftKeyboard(this);

                    putintentExtra(intent);

                    startActivity(intent);
                } else {
                    CustomToast.showToast(activity, "Please select the room ", 50);
                }

            } else if (llBooking.getVisibility() == View.VISIBLE) {

                Intent intent = new Intent(this, Preview.class);
                CustomServices.hideSoftKeyboard(this);
                lsDate = tvSelectedDate.getText().toString();

                putintentExtra(intent);

                startActivity(intent);

            } else if (llAgenda.getVisibility() == View.VISIBLE) {

                Intent intent = new Intent(this, Preview.class);
                CustomServices.hideSoftKeyboard(this);

                putintentExtra(intent);

                startActivity(intent);

            } else if (llParticipant.getVisibility() == View.VISIBLE) {
                Intent intent = new Intent(this, Preview.class);
                CustomServices.hideSoftKeyboard(this);
                putintentExtra(intent);
                startActivity(intent);

            } else if (llAttachment.getVisibility() == View.VISIBLE) {
                Intent intent = new Intent(this, Preview.class);
                CustomServices.hideSoftKeyboard(this);
                putintentExtra(intent);
                startActivity(intent);

            } else if (llServices.getVisibility() == View.VISIBLE) {

                ArrayList<Integer> selectedItems = serviceAdapter.getSelectedItems();
                Bundle b = new Bundle();
                alServicesPreview.clear();
                for (int i = 0; i < selectedItems.size(); i++) {
                    String lsServiceName = alServices.get(selectedItems.get(i)).getService();

                    b.putString(Constants.SV_DESC, lsServiceName);
                    ServicesModel model = new ServicesModel(b);
                    alServicesPreview.add(model);
                }

                Intent intent = new Intent(this, Preview.class);
                CustomServices.hideSoftKeyboard(this);
                putintentExtra(intent);
                startActivity(intent);

            }

        }
    }

    public void putintentExtra(Intent intent) {
        intent.putExtra(Constants.MW_TYPE, lsType);
        intent.putExtra(Constants.MW_CITY, lsCityDisplayValue);
        intent.putExtra(Constants.MW_OFFICE, lsOfficeValue);

        if (lsRoomName.length() > 0) {
            intent.putExtra(Constants.MW_NAME, lsRoomName);
        } else {
            intent.putExtra(Constants.MW_NAME, "");
        }
        if (lsRoomCapacity.length() > 0) {
            intent.putExtra(Constants.MW_CAPACITY, lsRoomCapacity);
        } else {
            intent.putExtra(Constants.MW_CAPACITY, "");
        }
        if (startTime.length() > 0) {
            intent.putExtra(WorkspaceBooked.WB_PLANNEDSTARTTIME, startTime);
        } else {
            intent.putExtra(WorkspaceBooked.WB_PLANNEDSTARTTIME, "");
        }
        if (endTime.length() > 0) {
            intent.putExtra(WorkspaceBooked.WB_PLANNEDENDTIME, endTime);
        } else {
            intent.putExtra(WorkspaceBooked.WB_PLANNEDENDTIME, "");
        }
        if (lsDate.length() > 0) {
            intent.putExtra(WorkspaceBooked.WB_MEETINGDATE, lsDate);
        } else {
            intent.putExtra(WorkspaceBooked.WB_MEETINGDATE, "");
        }
        if (lsAgenda.length() > 0) {
            intent.putExtra(WorkspaceBooked.WB_AGENDA, lsAgenda);
        } else {
            intent.putExtra(WorkspaceBooked.WB_AGENDA, "");
        }

        if (alParticipant.size() > 0) {

            Bundle b = new Bundle();
            b.putSerializable(MeetingParticipants.MP_EMPLOYEEEMAIL, alParticipant);
            intent.putExtras(b);
        } else {

            Bundle b = new Bundle();
            b.putSerializable(MeetingParticipants.MP_EMPLOYEEEMAIL, alParticipant);
            intent.putExtras(b);
        }

        if (alAttachment.size() > 0) {

            Bundle b = new Bundle();
            b.putSerializable(MeetingPreread.MPR_FILE, alAttachment);
            intent.putExtras(b);
        } else {

            Bundle b = new Bundle();
            b.putSerializable(MeetingPreread.MPR_FILE, alAttachment);
            intent.putExtras(b);
        }

        if (alServicesPreview.size() > 0) {

            Bundle b = new Bundle();
            b.putSerializable(Constants.SV_DESC, alServicesPreview);
            intent.putExtras(b);
        } else {

            Bundle b = new Bundle();
            b.putSerializable(Constants.SV_DESC, alServicesPreview);
            intent.putExtras(b);
        }
    }

    public void DefaultSelection() {

        ivMeetingHall.setVisibility(View.VISIBLE);
        ivConferenceHall.setVisibility(View.VISIBLE);
        ivBoardRoom.setVisibility(View.VISIBLE);
        ivTrainingHall.setVisibility(View.VISIBLE);
        ivConferenceHallPressed.setVisibility(View.GONE);
        ivMeetingHallPressed.setVisibility(View.GONE);
        ivTrainingHallPressed.setVisibility(View.GONE);
        ivBoardRoomPressed.setVisibility(View.GONE);

    }

    public void backClick() {
        if (llFilter.getVisibility() == View.VISIBLE) {
            setTitle("Select Workspace Type");
            CustomServices.hideSoftKeyboard(this);

            etCity.setText("");

            if (lovCityAdapter.getSelectedCount() > 0) {
                lovCityAdapter.clearSelection();
                lsCityValue = "";
                lsCityDisplayValue = "";
            }

            if (lovOtherCityAdapter.getSelectedCount() > 0) {
                lsCityValue = "";
                lovOtherCityAdapter.clearSelection();
            }

            if (officeAdapter.getSelectedCount() > 0) {
                officeAdapter.clearSelection();
                lsOfficeValue = "";
            }


            tvBack.setVisibility(View.VISIBLE);
            tvPreview.setVisibility(View.GONE);
            tvNext.setVisibility(View.VISIBLE);
            tvNext.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f));
            llFilter.setVisibility(View.GONE);
            vwView.setVisibility(View.GONE);
            llHome.setVisibility(View.VISIBLE);
            tvSubmit.setVisibility(View.GONE);
        } else if (llHome.getVisibility() == View.VISIBLE) {
            Intent intent = new Intent(this, Home.class);
            setTitle("Welcome To TAP2MEET");
            startActivity(intent);
            finish();

        } else if (llRoomDetails.getVisibility() == View.VISIBLE) {
            setTitle("Select Workspace Location");
            tvSubmit.setVisibility(View.GONE);
            tvBack.setVisibility(View.VISIBLE);
            tvPreview.setVisibility(View.VISIBLE);
            tvNext.setVisibility(View.VISIBLE);
            llRoomDetails.setVisibility(View.GONE);
            vwView.setVisibility(View.VISIBLE);
            llFilter.setVisibility(View.VISIBLE);

            if (roomsAdapter.getSelectedCount() > 0) {
                roomsAdapter.clearSelection();
                liRSId = 0;
                lsRoomName = "";
                lsRoomCapacity = "";
            } else {
                //  Toast.makeText(context, "did not selected any room", Toast.LENGTH_SHORT).show();
            }


        } else if (llBooking.getVisibility() == View.VISIBLE) {
            setTitle("Select Meeting Workspace");
            CustomServices.hideSoftKeyboard(this);
            tvSubmit.setVisibility(View.GONE);
            tvBack.setVisibility(View.VISIBLE);
            tvPreview.setVisibility(View.VISIBLE);
            tvNext.setVisibility(View.VISIBLE);
            llBooking.setVisibility(View.GONE);
            vwView.setVisibility(View.VISIBLE);
            llRoomDetails.setVisibility(View.VISIBLE);

            lsDate = "";

            if (mtimeAdapter.getSelectedCount() > 0) {
                mtimeAdapter.clearSelection();
                tvStartTime.setText("");
                tvEndTime.setText("");
                liSelected = -1;
                liSelectedFirst = -1;
                startTime = "";
                endTime = "";
            } else {
                // Toast.makeText(context, "did not selected any slot", Toast.LENGTH_SHORT).show();
            }

        } else if (llParticipant.getVisibility() == View.VISIBLE) {
            setTitle("Add Agenda");
            tvSubmit.setVisibility(View.GONE);
            tvBack.setVisibility(View.VISIBLE);
            tvPreview.setVisibility(View.VISIBLE);
            tvNext.setVisibility(View.VISIBLE);
            llParticipant.setVisibility(View.GONE);
            vwView.setVisibility(View.VISIBLE);
            llAgenda.setVisibility(View.VISIBLE);

            alParticipant.clear();
            participantAdapter.notifyDataSetChanged();

        } else if (llAgenda.getVisibility() == View.VISIBLE) {
            setTitle("Select Meeting Schedule");
            lsAgenda = etAgenda.getText().toString();
            lsTitle = etTitle.getText().toString();
            tvSubmit.setVisibility(View.GONE);
            tvBack.setVisibility(View.VISIBLE);
            tvPreview.setVisibility(View.VISIBLE);
            tvNext.setVisibility(View.VISIBLE);
            llAgenda.setVisibility(View.GONE);
            vwView.setVisibility(View.VISIBLE);
            llBooking.setVisibility(View.VISIBLE);

            if (!etAgenda.equals("") || !etTitle.equals("")) {
                etAgenda.setText("");
                etTitle.setText("");
                lsAgenda = "";
                lsTitle = "";
            } else {
                //   Toast.makeText(context, "doesnot have any change", Toast.LENGTH_SHORT).show();
            }


        } else if (llAttachment.getVisibility() == View.VISIBLE) {
            setTitle("Add Participant");
            tvSubmit.setVisibility(View.GONE);
            tvBack.setVisibility(View.VISIBLE);
            tvPreview.setVisibility(View.VISIBLE);
            tvNext.setVisibility(View.VISIBLE);
            llAttachment.setVisibility(View.GONE);
            vwView.setVisibility(View.VISIBLE);
            llParticipant.setVisibility(View.VISIBLE);

            if (alAttachment.size() > 0) {
                alAttachment.clear();
                alString.clear();
                attachmentAdapter.notifyItemRemoved(alAttachment.size());
            } else {
                // Toast.makeText(context, "not selected any attach", Toast.LENGTH_SHORT).show();
            }

        } else if (llServices.getVisibility() == View.VISIBLE) {
            setTitle("Add Attachment");
            tvSubmit.setVisibility(View.GONE);
            tvBack.setVisibility(View.VISIBLE);
            tvPreview.setVisibility(View.VISIBLE);
            tvNext.setVisibility(View.VISIBLE);
            llServices.setVisibility(View.GONE);
            vwView.setVisibility(View.VISIBLE);
            llAttachment.setVisibility(View.VISIBLE);

            if (serviceAdapter.getSelectedCount() > 0) {
                alServicesPreview.clear();
                serviceAdapter.clearSelection();
                serviceAdapter.notifyDataSetChanged();
                serviceAdapter.notifyItemRemoved(alServicesPreview.size());
            } else {
                // Toast.makeText(context, "not selected any services", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void showOfficeData() {

        llm1 = new LinearLayoutManager(context);
        rcvOffices.setLayoutManager(llm1);

        if (alOffice.size() == 0) {
            Bundle b = new Bundle();
            b.putString(Constants.MW_OFFICE, "No office found...");
            OfficeModel model = new OfficeModel(b);
            alOffice.add(model);
            officeAdapter = new OfficeAdapter(context, alOffice);
        } else {
            officeAdapter = new OfficeAdapter(context, alOffice);
        }

        //officeAdapter = new OfficeAdapter(context, alOffice);
        rcvOffices.setAdapter(officeAdapter);

        officeAdapter.setItemClickIterface(new OfficeAdapter.itemClickInterface() {
            @Override
            public void itemClick(int position) {
                String lsName = alOffice.get(position).getOffice();
                lsOfficeValue = lsName;

            }

        });

    }

    public void showLovCityData() {
        glm = new GridLayoutManager(context, 4);
        rcvCity.setLayoutManager(glm);
        lovCityAdapter = new LovCityAdapter(context, alLovCity);
        rcvCity.setAdapter(lovCityAdapter);


        lovCityAdapter.setItemClickInterface(new LovCityAdapter.itemClickIterface() {
            @Override
            public void itemClick(int position) {
                rcvOffices.setVisibility(View.VISIBLE);
                String lsName = alLovCity.get(position).getDisplayvalue();
                String lsValue = alLovCity.get(position).getStorevalue();
                lsCityValue = lsValue;
                lsCityDisplayValue = lsName;
                getOffices(lsValue, lsType);
            }

        });

        lovCityAdapter.setItemClickInterface1(new LovCityAdapter.itemClickIterface1() {
            @Override
            public void itemClick1(int position) {
                rcvOffices.setVisibility(View.GONE);
                lsOfficeValue = "";
                lsCityValue = "";
                lsCityDisplayValue = "";
            }

        });

        lovCityAdapter.setMultiple(false);
    }

    public void showLovOtherCityData() {

        llm = new LinearLayoutManager(context);
        rcvOtherCity.setLayoutManager(llm);

        if (alLovOtherCity.size() == 0) {
            Bundle b = new Bundle();
            b.putString(Lovs.LOV_DISPLAYVALUE, "No match found...");
            LovOtherCityModel model = new LovOtherCityModel(b);
            alLovOtherCity.add(model);
            lovOtherCityAdapter = new LovOtherCityAdapter(context, alLovOtherCity);
        } else {
            lovOtherCityAdapter = new LovOtherCityAdapter(context, alLovOtherCity);
        }


        rcvOtherCity.setAdapter(lovOtherCityAdapter);

        lovOtherCityAdapter.setItemClickIterface(new LovOtherCityAdapter.itemClickIterface() {
            @Override
            public void itemClick(int position) {
                String lsName = alLovOtherCity.get(position).getDisplayvalue();
                String lsValue = alLovOtherCity.get(position).getStorevalue();
                etCity.setText((lsName));
                rcvOtherCity.setVisibility(View.GONE);
                lsCityValue = lsValue;
                getOffices(lsValue, lsType);
                rcvOffices.setVisibility(View.VISIBLE);

            }

        });

    }

    public void showRooms() {

        llm2 = new LinearLayoutManager(context);
        rcvRooms.setLayoutManager(llm2);

        roomsAdapter = new RoomsAdapter(context, alRooms);
        rcvRooms.setAdapter(roomsAdapter);

        roomsAdapter.setOfficeChangedListener(new RoomsAdapter.officeChanged() {
            @Override
            public void getAmeneties(int position, int officeid) {
                getCloudAmeneties(position, officeid);
            }
        });

        roomsAdapter.setItemClickIterface(new RoomsAdapter.itemClickInterface() {
            @Override
            public void itemClick(int position) {
                liRSId = alRooms.get(position).getId();
                lsRoomName = alRooms.get(position).getName();
                lsRoomCapacity = alRooms.get(position).getCapacity();
            }

        });

    }

    public void getRooms(String lsCity, final String lsType, String lsOffice) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT *  FROM " + Constants.MTG_WORKSPACE_TABLENAME + " WHERE " + Constants.MW_CITY + " = '" + lsCity + "' AND " + Constants.MW_TYPE + " = '" + lsType + "' AND " + Constants.MW_OFFICE + " = '" + lsOffice + "'";


        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

            //    ShowProgressDialog.setTitle("Loading Rooms...");
            //   ShowProgressDialog.showProgress(this);

            pd.show();

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alRooms.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.MW_ID)) {

                                liOfficeId = groupData.getInt(Constants.MW_ID);
                                lsOfficeCapacity = groupData.getString(Constants.MW_CAPACITY);
                                lsOfficeCity = groupData.getString(Constants.MW_OFFICE);
                                lsOfficeName = groupData.getString(Constants.MW_NAME);


                                Bundle b = new Bundle();
                                b.putInt(Constants.MW_ID, liOfficeId);
                                b.putString(Constants.MW_OFFICE, lsOfficeCity);
                                b.putString(Constants.MW_CAPACITY, lsOfficeCapacity);
                                b.putString(Constants.MW_TYPE, lsType);
                                b.putString(Constants.MW_NAME, lsOfficeName);

                                RoomsModel model = new RoomsModel(b);

                                alRooms.add(model);

                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {

                            pd.dismiss();
                            showRooms();
                            //    ShowProgressDialog.hideProgressDialog(context);
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 737 " + e.toString());
                }

            }

        });

    }

    public void getOtherCities(String lsCity) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT * FROM " + Lovs.TABLENAME +
                " WHERE " + Lovs.LOV_TYPE + " = 'CITY' AND " + Lovs.LOV_PARENT + " = 'OC'" +
                "AND " + Lovs.LOV_DISPLAYVALUE + " LIKE '%" + lsCity + "%'";


        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

            // ShowProgressDialog.setTitle("Loading Cities...");
            // ShowProgressDialog.showProgress(this);
            pd.show();

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alLovOtherCity.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Lovs.LOV_ID)) {

                                String lsDisplayvalue = groupData.getString(Lovs.LOV_DISPLAYVALUE);
                                String lsStorevalue = groupData.getString(Lovs.LOV_STOREVALUE);


                                Bundle b = new Bundle();
                                b.putString(Lovs.LOV_DISPLAYVALUE, lsDisplayvalue);
                                b.putString(Lovs.LOV_STOREVALUE, lsStorevalue);


                                LovOtherCityModel model = new LovOtherCityModel(b);

                                alLovOtherCity.add(model);

                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            pd.dismiss();
                            showLovOtherCityData();
                            //  ShowProgressDialog.hideProgressDialog(context);
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 828 " + e.toString());
                }

            }

        });

    }

    public void getOffices(String lsValue, String lsType) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();


        String lsSql = "SELECT DISTINCT " + Constants.MW_OFFICE + " FROM " + Constants.MTG_WORKSPACE_TABLENAME + " WHERE " + Constants.MW_CITY + " = '" + lsValue + "' AND " + Constants.MW_TYPE + " = '" + lsType + "'";

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

            //  ShowProgressDialog.setTitle("Loading Offices...");
            //  ShowProgressDialog.showProgress(this);
            pd.show();

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alOffice.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.MW_OFFICE)) {

                                String lsOffice = groupData.getString(Constants.MW_OFFICE);


                                Bundle b = new Bundle();
                                b.putString(Constants.MW_OFFICE, lsOffice);

                                OfficeModel model = new OfficeModel(b);

                                alOffice.add(model);


                                //got office/
                                //based on office you need to get ameneties
                                //select * from offfice ameneties where ame_officeid = "123";
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {

                            pd.dismiss();
                            showOfficeData();
                            //  ShowProgressDialog.hideProgressDialog(context);
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 919 " + e.toString());
                }

            }

        });

    }

    public void getCity() {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT * FROM " + Lovs.TABLENAME + " WHERE " + Lovs.LOV_TYPE + " = 'CITY' AND " + Lovs.LOV_PARENT + " = 'PC'";

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

            //  ShowProgressDialog.setTitle("Loading Cities...");
            //  ShowProgressDialog.showProgress(this);
            pd.show();
        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alLovCity.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Lovs.LOV_ID)) {

                                String lsDisplayvalue = groupData.getString(Lovs.LOV_DISPLAYVALUE);
                                String lsStorevalue = groupData.getString(Lovs.LOV_STOREVALUE);


                                Bundle b = new Bundle();
                                b.putString(Lovs.LOV_DISPLAYVALUE, lsDisplayvalue);
                                b.putString(Lovs.LOV_STOREVALUE, lsStorevalue);


                                LovCityModel model = new LovCityModel(b);

                                alLovCity.add(model);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {

                            pd.dismiss();
                            showLovCityData();
                            //   ShowProgressDialog.hideProgressDialog(context);
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }

    public void getCloudAmeneties(final int position, final int officeID) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT " + Constants.AM_NAME +
                " FROM "
                + Constants.AMENITIES_TABLENAME + " a , "
                + Constants.MTG_WORKSPACE_TABLENAME + " b , "
                + Constants.MTG_WORKSPACE_AMENITIES_TABLENAME + " c " +
                "WHERE b." + Constants.MW_ID + " = c." + Constants.WA_MWID +
                " AND c." + Constants.WA_AMID + " = a." + Constants.AM_ID +
                " AND b." + Constants.MW_ID + " = '" + officeID + "'";

        // alAmenities.clear();

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alAmenities.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);
                        for (int x = 0; x < resultsArray.length(); x++) {

                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.AM_NAME)) {

                                String lsName = groupData.getString(Constants.AM_NAME);


                                Bundle b = new Bundle();
                                b.putString(Constants.AM_NAME, lsName);


                                AmenitiesModel model = new AmenitiesModel(b);

                                alAmenities.add(model);

                            }

                        }

                    }

                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            alRooms.get(position).setAmenities(alAmenities);
                            roomsAdapter.notifyItemChanged(position);
                        }
                    }));


                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1109 " + e.toString());
                }

            }

        });

    }

    public void getMTimeLovs(final String date) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();
        String lsSql;

        lsSql = "SELECT * FROM " + Constants.TIME_TABLENAME + " ORDER By " + Constants.TIME_SORTINGORDER + " ASC ";

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

            pd.show();

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alMTime.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.TIME_ID)) {

                                String lsTime = groupData.getString(Constants.TIME_TEXT);
                                int liSo = groupData.getInt(Constants.TIME_SORTINGORDER);
                                String lsTimePhase = groupData.getString(Constants.TIME_PHASE);

                                Bundle b = new Bundle();
                                b.putString(Constants.TIME_TEXT, lsTime);
                                b.putInt(Constants.TIME_SORTINGORDER, liSo);
                                b.putString(Constants.TIME_PHASE, lsTimePhase);

                                MTimeModel model = new MTimeModel(b);
                                alMTime.add(model);
                            }
                        }
                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            //  showMTime();
                            checkAvailaility(date);
                        }
                    }));
                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }
            }

        });
    }

    public void checkAvailaility(String lsDate) {

        java.util.Date date = new Date();
        SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
        String currentDate = sf.format(date);
        //String currentDate = String.valueOf(date);

        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        ArrayList alSql = new ArrayList();
        String lssql = "SELECT wb_st_so,wb_et_so,t.time_text,t.time_sortingorder,w.wb_plannedendtime " +
                "from time_lovs t,workspace_booked w " +
                "where t.time_text=w.wb_plannedstarttime and w.wb_meetingdate='" + lsDate + "' " +
                "and w.wb_workspaceid='" + liRSId + "' AND " + WorkspaceBooked.WB_BOOKING_STATUS + " = 'A'";
        alSql.add(lssql);


        for (int i = 0; i < alSql.size(); i++) {
            try {
                lssql = alSql.get(i).toString();
                jsonObject.put("OPERATION", lssql);
                jsonArray.put(jsonObject);
            } catch (Exception e) {
                Log.d(TAG, "Error is" + e.toString());
            }
        }
        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }
        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject1;
                try {
                    jsonObject1 = new JSONObject(data);
                    JSONArray jsonArray1 = jsonObject1.getJSONArray("results");
                    for (int i = 0; i < jsonArray1.length(); i++) {

                        alSortingOrderData.clear();

                        JSONArray resultjarray = jsonArray1.getJSONArray(i);
                        boolean lbRefresh = false;
                        for (int x = 0; x < resultjarray.length(); x++) {
                            JSONObject groupData = resultjarray.getJSONObject(x);
                            if (groupData.has(WorkspaceBooked.WB_PLANNEDENDTIME)) {
                                int liStartSO = groupData.getInt(WorkspaceBooked.WB_ST_SO);
                                int liEndSO = groupData.getInt(WorkspaceBooked.WB_ET_SO);

                                for (int y = 0; y < alMTime.size(); y++) {
                                    int liCurrentSO = alMTime.get(y).getSortingOrder();
                                    if (liCurrentSO >= liStartSO && liCurrentSO <= liEndSO) {
                                        lbRefresh = true;
                                        alMTime.get(y).setBooked(true);
                                    }
                                }

                                Bundle b = new Bundle();
                                b.putInt("startOrder", liStartSO);
                                b.putInt("endOrder", liEndSO);

                                SortingOrderModel model = new SortingOrderModel(b);
                                alSortingOrderData.add(model);

                            }
                        }
                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            // checkAvailaility();
                            pd.dismiss();
                            showMTime();

                        }
                    }));
                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());

                }
            }
        });
    }

    public void showMTime() {
        glmMorning = new GridLayoutManager(context, 5);
        rcvMTime.setLayoutManager(glmMorning);
        mtimeAdapter = new MTimeAdapter(context, alMTime);
        rcvMTime.setAdapter(mtimeAdapter);


        mtimeAdapter.setItemClickInterface(new MTimeAdapter.itemClickIterface() {
            @Override
            public void itemClick(int position) {

                boolean lbCheck = false;

                if (mtimeAdapter.getSelectedCount() == 0) {
                    liSelectedFirst = position;
                    mtimeAdapter.toggleSelection(position);
                    mtimeAdapter.notifyItemChanged(position);
                    tvStartTime.setText(alMTime.get(position).getTime());
                    startTime = alMTime.get(position).getTime();
                    stSortingOrder = alMTime.get(position).getSortingOrder();
                } else if (mtimeAdapter.getSelectedCount() == 1) {
                    String eTime = alMTime.get(position).getTime();
                    int etSorting = alMTime.get(position).getSortingOrder();

                    for (int i = 0; i < alSortingOrderData.size(); i++) {

                        int st_so = alSortingOrderData.get(i).getStartSo();
                        int et_so = alSortingOrderData.get(i).getEndSo();

                        if ((stSortingOrder <= st_so) && (etSorting >= et_so)) {
                            //  Toast.makeText(context, "yes", Toast.LENGTH_SHORT).show();
                            lbCheck = true;
                            break;
                        }
                    }

                    if (lbCheck == false) {
                        if (!CustomServices.TimeValidator(tvStartTime.getText().toString(), eTime)) {
                            tvEndTime.setText("");
                            CustomToast.showToast(activity, "End Time cannot be less than From Time", 50);
                            return;
                        } else {
                            liSelected = position;
                            for (int i = position; i >= liSelectedFirst + 1; i--) {
                                mtimeAdapter.toggleSelection(i);
                                mtimeAdapter.notifyItemChanged(i);
                            }
                            tvEndTime.setText(alMTime.get(position).getTime());
                            etSortingOrder = alMTime.get(position).getSortingOrder();
                            endTime = alMTime.get(position).getTime();

                        }
                    } else {
                        mtimeAdapter.clearSelection();
                        tvStartTime.setText("");
                        tvEndTime.setText("");
                        startTime = "";
                        endTime = "";
                        CustomToast.showToast(activity, "This slot already booked. Choose other one.", 50);
                    }

                } else if (mtimeAdapter.getSelectedCount() >= 1) {

                 /*   int etSorting = alMTime.get(position).getSortingOrder();

                    for (int i = 0; i < alSortingOrderData.size(); i++) {

                        int st_so = alSortingOrderData.get(i).getStartSo();
                        int et_so = alSortingOrderData.get(i).getEndSo();

                        if ((stSortingOrder <= st_so) && (etSorting >= et_so)) {
                          //  Toast.makeText(context, "yes", Toast.LENGTH_SHORT).show();
                            lbCheck = true;
                            break;
                        }
                    }*/


                    if (position == liSelectedFirst) {
                        CustomToast.showToast(activity, "Cannot select same slot for End-time...select other one", 50);
                        return;
                    } else if (position < liSelectedFirst) {
                        liSelectedFirst = position;
                        int stSorting = alMTime.get(position).getSortingOrder();
                        for (int i = 0; i < alSortingOrderData.size(); i++) {

                            int st_so = alSortingOrderData.get(i).getStartSo();
                            int et_so = alSortingOrderData.get(i).getEndSo();

                            if ((stSorting <= st_so) && (etSortingOrder >= et_so)) {
                                //  Toast.makeText(context, "yes", Toast.LENGTH_SHORT).show();
                                lbCheck = true;
                                break;
                            }
                        }
                        if (lbCheck == false) {
                            mtimeAdapter.clearSelection();
                            for (int i = position; i <= liSelected; i++) {
                                mtimeAdapter.toggleSelection(i);
                                mtimeAdapter.notifyItemChanged(i);
                            }
                            tvStartTime.setText(alMTime.get(position).getTime());
                            startTime = alMTime.get(position).getTime();
                            stSortingOrder = alMTime.get(position).getSortingOrder();
                        } else {
                            mtimeAdapter.clearSelection();
                            tvStartTime.setText("");
                            tvEndTime.setText("");
                            startTime = "";
                            endTime = "";
                            // Toast.makeText(context, "Cannot book this slot..choose other one", Toast.LENGTH_SHORT).show();
                            CustomToast.showToast(activity, "This slot already booked. Choose other one.", 50);
                        }
                    } else {
                        liSelected = position;
                        int etSorting = alMTime.get(position).getSortingOrder();
                        for (int i = 0; i < alSortingOrderData.size(); i++) {

                            int st_so = alSortingOrderData.get(i).getStartSo();
                            int et_so = alSortingOrderData.get(i).getEndSo();

                            if ((stSortingOrder <= st_so) && (etSorting >= et_so)) {
                                //  Toast.makeText(context, "yes", Toast.LENGTH_SHORT).show();
                                lbCheck = true;
                                break;
                            }
                        }
                        if (lbCheck == false) {
                            mtimeAdapter.clearSelection();
                            for (int i = position; i >= liSelectedFirst; i--) {
                                mtimeAdapter.toggleSelection(i);
                                mtimeAdapter.notifyItemChanged(i);
                            }
                            tvEndTime.setText(alMTime.get(position).getTime());
                            endTime = alMTime.get(position).getTime();
                            etSortingOrder = alMTime.get(position).getSortingOrder();
                        } else {
                            mtimeAdapter.clearSelection();
                            tvStartTime.setText("");
                            tvEndTime.setText("");
                            startTime = "";
                            endTime = "";
                            // Toast.makeText(context, "Cannot book this slot..choose other one", Toast.LENGTH_SHORT).show();
                            CustomToast.showToast(activity, "This slot already booked. Choose other one.", 50);
                        }
                    }

                }

                mtimeAdapter.setMultiple(true);
            }

        });
    }

    public int validateSlot(int startSo, int endSo) {
        int liCount = 0;
        for (int i = 0; i < alSortingOrderData.size(); i++) {
            int s = alSortingOrderData.get(i).getStartSo();
            int e = alSortingOrderData.get(i).getEndSo();

            if (startSo <= s && endSo >= e) {
                liCount = liCount;
                Toast.makeText(context, "count " + liCount, Toast.LENGTH_SHORT).show();
            } else {
                liCount = liCount++;
                Toast.makeText(context, "count " + liCount, Toast.LENGTH_SHORT).show();
            }
        }
        if (liCount > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public void getServices() {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT * FROM " + Constants.SERVICES_TABLENAME;


        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }
            pd.show();
        }

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alServices.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.SV_ID)) {

                                String lsServices = groupData.getString(Constants.SV_DESC);
                                int liId = groupData.getInt(Constants.SV_ID);


                                Bundle b = new Bundle();
                                b.putString(Constants.SV_DESC, lsServices);
                                b.putInt(Constants.SV_ID, liId);

                                ServicesModel model = new ServicesModel(b);

                                alServices.add(model);
                            }
                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            pd.dismiss();
                            showServices();
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }

    public void showServices() {
        llmServices = new LinearLayoutManager(context);
        rcvServices.setLayoutManager(llmServices);
        serviceAdapter = new ServiceAdapter(context, alServices);
        rcvServices.setAdapter(serviceAdapter);

    }

    public  void sendSamplePush(String lsMessage) {

        getDeviceIds(lsMessage);


    }

    public void getDeviceIds(final String lsMessage) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();
        String lsSql;

        for(int i= 0; i<= alParticipant.size()-1; i++)
        {
            String email = alParticipant.get(i).getName();
            if(!email.equals(lsEmail))
            {
                lsSql = "SELECT "+UserLogin.UL_DEVICEKEY+" FROM " + UserLogin.TABLENAME + " WHERE " + UserLogin.UL_EMAIL + " = '" + email + "'";
                alSql.add(lsSql);
            }
        }


        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    alDeviceKeys.clear();

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(UserLogin.UL_DEVICEKEY)) {

                                String lsDeviceKey = groupData.getString(UserLogin.UL_DEVICEKEY);

                                alDeviceKeys.add(lsDeviceKey);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {


                            for (int i = 0; i <= alDeviceKeys.size()-1;i++)
                            {
                                deviceTokens.add(alDeviceKeys.get(i));
                            }

                            // Convert to String[] array
                            String[] to = deviceTokens.toArray(new String[deviceTokens.size()]);

                            Map<String, String> payload = new HashMap<>();

                            // Add "message" parameter to payload
                            payload.put(App.NOTIFICATION, lsMessage);
                            payload.put(App.MEETING_DATE,lsMeetingDate);
                            payload.put(App.MEETING_STARTTIME,startTime);
                            payload.put(App.MEETING_ENDTIME,endTime);

                            // iOS notification fields
                            Map<String, Object> notification = new HashMap<>();

                            notification.put("badge", 1);
                            notification.put("sound", "ping.aiff");
                            notification.put("body", lsMessage);

                            // Prepare the push request
                            final PushyAPI.PushyPushRequest push = new PushyAPI.PushyPushRequest(payload, to, notification);

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        PushyAPI.sendPush(push);
                                    } catch (Exception ex) {
                                        //ex.printStackTrace();
                                        System.out.println("Exception called exception number is 1001 : " + ex.toString());
                                    }
                                }
                            }).start();
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }


    public void getParticipantsList(String lsText) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT * FROM " + UserLogin.TABLENAME + " WHERE " + UserLogin.UL_EMAIL + " LIKE '%" + lsText + "%'";

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alParticipantList.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(UserLogin.UL_EMAIL)) {

                                String lsEmail = groupData.getString(UserLogin.UL_EMAIL);


                                Bundle b = new Bundle();
                                b.putString(UserLogin.UL_EMAIL, lsEmail);


                                ParticipantsList_Model model = new ParticipantsList_Model(b);

                                alParticipantList.add(model);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {

                            showParticipants();
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }

    public void showParticipants() {
        llParticipantsList = new LinearLayoutManager(context);
        rcvParticipantsList.setLayoutManager(llParticipantsList);
        ParticipantsList_Adapter = new ParticipantsList_Adapter(context, alParticipantList);
        rcvParticipantsList.setAdapter(ParticipantsList_Adapter);

        ParticipantsList_Adapter.setItemClickInterface(new ParticipantsList_Adapter.itemClickIterface() {
            @Override
            public void itemClick(int position) {
                boolean lbCheckParticipant = false;
                String lsTitle = alParticipantList.get(position).getParticipantsList();
                etParticipantsList.setText(lsTitle);
                rcvParticipantsList.setVisibility(View.GONE);

                if (lsTitle.length() > 0) {
                    if ((lsTitle.contains("@"))) {

                        for (int i = 0; i < alParticipant.size(); i++) {
                            lbCheckParticipant = false;
                            String lsName = alParticipant.get(i).getName();
                            if (lsName.equals(lsTitle)) {
                                lbCheckParticipant = true;
                                break;
                            }
                        }

                        if (lbCheckParticipant == true) {
                            CustomToast.showToast(activity, "Already added", 50);
                        } else {
                            b = new Bundle();
                            b.putString(MeetingParticipants.MP_EMPLOYEEEMAIL, lsTitle);
                            ParticipantModel model = new ParticipantModel(b);
                            alParticipant.add(model);
                        }


                    } else {
                        CustomToast.showToast(activity, "Provide valid e-mail", 50);
                    }

                } else {
                    CustomToast.showToast(activity, "Participant e-mail please", 50);
                }

                etParticipantsList.setText("");
                CustomServices.hideSoftKeyboard(activity);

            }
        });

    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        backClick();
    }


/*
    public static long pushAppointmentsToCalender(Activity curActivity, String title, String addInfo, String place, int status, long startDate, boolean needReminder, boolean needMailService) {
        */
/***************** Event: note(without alert) *******************//*


        String eventUriString = "content://com.android.calendar/events";
        ContentValues eventValues = new ContentValues();

        eventValues.put("calendar_id", 1); // id, We need to choose from
        // our mobile for primary
        // its 1
        eventValues.put("title", title);
        eventValues.put("description", addInfo);
        eventValues.put("eventLocation", place);

        long endDate = startDate + 1000 * 60 * 60; // For next 1hr

        eventValues.put("dtstart", startDate);
        eventValues.put("dtend", endDate);

        // values.put("allDay", 1); //If it is bithday alarm or such
        // kind (which should remind me for whole day) 0 for false, 1
        // for true
        eventValues.put("eventStatus", status); // This information is
        // sufficient for most
        // entries tentative (0),
        // confirmed (1) or canceled
        // (2):
        eventValues.put("eventTimezone", "UTC/GMT +2:00");
   */
/*Comment below visibility and transparency  column to avoid java.lang.IllegalArgumentException column visibility is invalid error *//*


    */
/*eventValues.put("visibility", 3); // visibility to default (0),
                                        // confidential (1), private
                                        // (2), or public (3):
    eventValues.put("transparency", 0); // You can control whether
                                        // an event consumes time
                                        // opaque (0) or transparent
                                        // (1).
      *//*

        eventValues.put("hasAlarm", 1); // 0 for false, 1 for true

        Uri eventUri = curActivity.getApplicationContext().getContentResolver().insert(Uri.parse(eventUriString), eventValues);
        long eventID = Long.parseLong(eventUri.getLastPathSegment());

        if (needReminder) {
            */
/***************** Event: Reminder(with alert) Adding reminder to event *******************//*


            String reminderUriString = "content://com.android.calendar/reminders";

            ContentValues reminderValues = new ContentValues();

            reminderValues.put("event_id", eventID);
            reminderValues.put("minutes", 5); // Default value of the
            // system. Minutes is a
            // integer
            reminderValues.put("method", 1); // Alert Methods: Default(0),
            // Alert(1), Email(2),
            // SMS(3)

            Uri reminderUri = curActivity.getApplicationContext().getContentResolver().insert(Uri.parse(reminderUriString), reminderValues);
        }

        */
/***************** Event: Meeting(without alert) Adding Attendies to the meeting *******************//*


        if (needMailService) {
            String attendeuesesUriString = "content://com.android.calendar/attendees";

            */
/********
             * To add multiple attendees need to insert ContentValues multiple
             * times
             ***********//*

            ContentValues attendeesValues = new ContentValues();

            attendeesValues.put("event_id", eventID);
            attendeesValues.put("attendeeName", "xxxxx"); // Attendees name
            attendeesValues.put("attendeeEmail", "yyyy@gmail.com");// Attendee
            // E
            // mail
            // id
            attendeesValues.put("attendeeRelationship", 0); // Relationship_Attendee(1),
            // Relationship_None(0),
            // Organizer(2),
            // Performer(3),
            // Speaker(4)
            attendeesValues.put("attendeeType", 0); // None(0), Optional(1),
            // Required(2), Resource(3)
            attendeesValues.put("attendeeStatus", 0); // NOne(0), Accepted(1),
            // Decline(2),
            // Invited(3),
            // Tentative(4)

            Uri attendeuesesUri = curActivity.getApplicationContext().getContentResolver().insert(Uri.parse(attendeuesesUriString), attendeesValues);
        }

        return eventID;

    }
*/


private void addEvents() {
    int calenderId=-1;
    String calenderEmaillAddress="rajshekharjamadar1993.rj@gmail.com";
    String[] projection = new String[]{
            CalendarContract.Calendars._ID,
            CalendarContract.Calendars.ACCOUNT_NAME};
    ContentResolver cr = activity.getContentResolver();
    Cursor cursor = cr.query(Uri.parse("content://com.android.calendar/calendars"), projection,
            CalendarContract.Calendars.ACCOUNT_NAME + "=? and (" +
                    CalendarContract.Calendars.NAME + "=? or " +
                    CalendarContract.Calendars.CALENDAR_DISPLAY_NAME + "=?)",
            new String[]{calenderEmaillAddress, calenderEmaillAddress,
                    calenderEmaillAddress}, null);

    if (cursor.moveToFirst()) {

        if (cursor.getString(1).equals(calenderEmaillAddress))
            calenderId=cursor.getInt(0); //youre calender id to be insered in above 2 answer
    }
}
}

