package com.WeApplify.Tap2Meet.UI;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.WeApplify.Tap2Meet.Adapters.LovCityAdapter;
import com.WeApplify.Tap2Meet.Adapters.LovOtherCityAdapter;
import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.Models.LovCityModel;
import com.WeApplify.Tap2Meet.Models.LovOtherCityModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.DataTransmitter;
import com.WeApplify.Tap2Meet.Services.Debugger;
import com.WeApplify.Tap2Meet.Tables.Lovs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 08-06-2017.
 */

public class Filter extends BaseActivity implements View.OnClickListener {

    public static final String TAG = "Filter";

    Activity activity;
    Context context;
    LinearLayout llCities,llOffices,llParticipants;
    RecyclerView rcvCity,rcvOtherCity;
    GridLayoutManager glm;
    ArrayList<LovCityModel> alLovModel;
    ArrayList<LovOtherCityModel> alOtherCityModel;
    LovCityAdapter lovAdapter;
    LovOtherCityAdapter adapter;
    LinearLayoutManager llm;
    EditText etCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter);

        activity = this;
        context = this;

        setTitle("Filter");

        initlayout();
    }

    public void initlayout()
    {

        alLovModel = new ArrayList<>();
        alOtherCityModel = new ArrayList<>();


        rcvCity = (RecyclerView)findViewById(R.id.rcvCity);
        rcvOtherCity = (RecyclerView)findViewById(R.id.rcvOtherCity);
        etCity = (EditText)findViewById(R.id.etCity);



        getCity();

        etCity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String lsText = editable.toString();
                if (lsText.length() > 2) {
                    rcvOtherCity.setVisibility(View.VISIBLE);
                    getOtherCities(lsText);
                } else {
                    alOtherCityModel.clear();
                    rcvOtherCity.setVisibility(View.GONE);
                }
            }
        });

        //cbNext.setOnClickListener(this);
        llOffices.setOnClickListener(this);
        llParticipants.setOnClickListener(this);
        llOffices.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){

        }
    }


    public void getCity() {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

//        SELECT a.* , b.para_id FROM records a , test_parameter b WHERE a.rc_para_id = b.para_id AND  rc_profile = '7276754453' AND rc_timestamp = '14345678'

        String lsSql = "SELECT * FROM "+ Lovs.TABLENAME+" WHERE "+Lovs.LOV_TYPE+" = 'CITY'";
        // " AND "+RecordTable.RC_TIMESTAMP+" = '"+lsTimestamp+"' ORDER BY "+Test_Parameters.PARA_NAME+" ASC";



        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {

                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Lovs.LOV_ID)) {

                                String lsDisplayvalue = groupData.getString(Lovs.LOV_DISPLAYVALUE);
                                String lsStorevalue = groupData.getString(Lovs.LOV_STOREVALUE);


                                Bundle b = new Bundle();
                                b.putString(Lovs.LOV_DISPLAYVALUE,lsDisplayvalue);
                                b.putString(Lovs.LOV_STOREVALUE,lsStorevalue);



                                LovCityModel model = new LovCityModel(b);

                                alLovModel.add(model);

                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {

                            showData();
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 172 " + e.toString());
                }

            }

        });

    }

    public void getOtherCities(String lsCity) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT * FROM "+ Lovs.TABLENAME+
                " WHERE "+Lovs.LOV_TYPE+" = 'OTHER_CITY' " +
                "AND "+Lovs.LOV_DISPLAYVALUE+" LIKE '%" + lsCity + "%'";




        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alOtherCityModel.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Lovs.LOV_ID)) {

                                String lsDisplayvalue = groupData.getString(Lovs.LOV_DISPLAYVALUE);
                                String lsStorevalue = groupData.getString(Lovs.LOV_STOREVALUE);


                                Bundle b = new Bundle();
                                b.putString(Lovs.LOV_DISPLAYVALUE,lsDisplayvalue);
                                b.putString(Lovs.LOV_STOREVALUE,lsStorevalue);



                                LovOtherCityModel model = new LovOtherCityModel(b);

                                alOtherCityModel.add(model);

                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            showOtherCitiesData();
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 172 " + e.toString());
                }

            }

        });

    }


    public void showData() {

        glm = new GridLayoutManager(context, 4, GridLayoutManager.VERTICAL, false);
        rcvCity.setLayoutManager(glm);
        lovAdapter = new LovCityAdapter(context, alLovModel);
        rcvCity.setAdapter(lovAdapter);

        lovAdapter.setMultiple(false);
    }

    public void showOtherCitiesData() {

        llm = new LinearLayoutManager(context);
        rcvOtherCity.setLayoutManager(llm);
        adapter = new LovOtherCityAdapter(context, alOtherCityModel);
        rcvOtherCity.setAdapter(adapter);

        adapter.setItemClickIterface(new LovOtherCityAdapter.itemClickIterface() {
            @Override
            public void itemClick(int position) {
                String lsName = alOtherCityModel.get(position).getDisplayvalue();
                etCity.setText((lsName));
                rcvOtherCity.setVisibility(View.GONE);
            }

        });

        //lovAdapter.setMultiple(false);
    }


}
