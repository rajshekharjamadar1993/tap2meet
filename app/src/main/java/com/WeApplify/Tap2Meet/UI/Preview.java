package com.WeApplify.Tap2Meet.UI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.WeApplify.Tap2Meet.Adapters.ParticipantAdapter;
import com.WeApplify.Tap2Meet.Adapters.PreviewAttachmentAdapter;
import com.WeApplify.Tap2Meet.Adapters.PreviewParticipantAdapter;
import com.WeApplify.Tap2Meet.Adapters.PreviewServicesAdapter;
import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.Models.AttachmentModel;
import com.WeApplify.Tap2Meet.Models.ParticipantModel;
import com.WeApplify.Tap2Meet.Models.ServicesModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.MeetingParticipants;
import com.WeApplify.Tap2Meet.Tables.MeetingPreread;
import com.WeApplify.Tap2Meet.Tables.WorkspaceBooked;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by harshagulati on 26/06/17.
 */

public class Preview extends BaseActivity {

    Activity activity;
    Context context;

    LinearLayoutManager llmParticipant,llmAttachments, llmServices;
    RecyclerView rcvPreviewParticipants, rcvPreviewServices;
    ImageView ivBack,ivLogout,ivNotification, ivHome;
    LinearLayout llParticipants;
    TextView tvBack;
    TextView tvSWorkspaceValue, tvSCity, tvSCityValue, tvSOfficeValue, tvSRoomValue, tvSStartTime, tvSStartTimeValue, tvSEndTime, tvSEndTimeValue, tvSDate, tvSDateValue, tvSAgendaValue;
    View vwRoom, vwWorkspace, vwCity, vwStartTime, vwEndTime, vwdate, vwAgenda,vwParticipants,vwServices;
    ImageView ivWorkspace;
    LinearLayout llLocation,llRoomDetails,llSchedule,llAgenda, llServices;
    TextView tvBulCity, tvBulRoom, tvBulDate, tvBulStartTime, tvBulEndTime,tvBulAgenda;

    TextView tvSRoomCapacity,tvSRoomCapacityValue;
    ArrayList<ParticipantModel> alParticipants;
    ArrayList<AttachmentModel> alAttachment;
    ArrayList<ServicesModel> alServices;
    PreviewParticipantAdapter participantAdapter;
    PreviewAttachmentAdapter attachmentAdapter;
    PreviewServicesAdapter servicesAdapter;

    LinearLayout llAttachments;
    View vwAttachments;
    RecyclerView rcvPreviewAttachments;

    FrameLayout fmlProduct;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.preview);
        setTitle("Preview");

        context = this;
        activity = this;

        fmlProduct = (FrameLayout)findViewById(R.id.fmlProduct);
        ivHome = (ImageView)findViewById(R.id.ivHome);
        initLayout();
        //ivSearch.setVisibility(View.GONE);
      //  ivNotification.setVisibility(View.INVISIBLE);
        ivLogout.setVisibility(View.GONE);
        fmlProduct.setVisibility(View.GONE);
        ivHome.setVisibility(View.GONE);
    }

    public void initLayout() {

        alParticipants = new ArrayList<>();
        alAttachment = new ArrayList<>();
        alServices = new ArrayList<>();

        Bundle b = getIntent().getExtras();

        String lsCity = b.getString(Constants.MW_CITY);
        String lsOffice = b.getString(Constants.MW_OFFICE);
        String lsType = b.getString(Constants.MW_TYPE);
        String lsRoom = b.getString(Constants.MW_NAME);
        String lsCapacity = b.getString(Constants.MW_CAPACITY);
        String lsStartTime = b.getString(WorkspaceBooked.WB_PLANNEDSTARTTIME);
        String lsEndTime = b.getString(WorkspaceBooked.WB_PLANNEDENDTIME);
        String lsDate = b.getString(WorkspaceBooked.WB_MEETINGDATE);
        String lsAgenda = b.getString(WorkspaceBooked.WB_AGENDA);


        alParticipants=(ArrayList<ParticipantModel>)getIntent().getSerializableExtra(MeetingParticipants.MP_EMPLOYEEEMAIL);
        alAttachment=(ArrayList<AttachmentModel>)getIntent().getSerializableExtra(MeetingPreread.MPR_FILE);
        alServices=(ArrayList<ServicesModel>)getIntent().getSerializableExtra(Constants.SV_DESC);

        vwAgenda = (View)findViewById(R.id.vwAgenda);

        rcvPreviewParticipants = (RecyclerView)findViewById(R.id.rcvPreviewParticipants);
        rcvPreviewAttachments = (RecyclerView)findViewById(R.id.rcvPreviewAttachments);
        rcvPreviewServices = (RecyclerView)findViewById(R.id.rcvPreviewServices);
        llParticipants = (LinearLayout)findViewById(R.id.llParticipants);
        llAttachments = (LinearLayout)findViewById(R.id.llAttachments);
        llServices = (LinearLayout)findViewById(R.id.llServices);
        vwAttachments = (View)findViewById(R.id.vwAttachments);

        tvSRoomCapacity = (TextView)findViewById(R.id.tvSRoomCapacity);
        tvSRoomCapacityValue = (TextView)findViewById(R.id.tvSRoomCapacityValue);
        tvBack = (TextView) findViewById(R.id.tvBack);
        ivBack = (ImageView) findViewById(R.id.ivBack);
        ivLogout = (ImageView) findViewById(R.id.ivLogout);
        ivNotification = (ImageView)findViewById(R.id.ivNotification);
        //ivSearch = (ImageView)findViewById(R.id.ivSearch);

        tvSWorkspaceValue = (TextView) findViewById(R.id.tvSWorkspaceValue);
        tvSCity = (TextView) findViewById(R.id.tvSCity);
        tvSCityValue = (TextView) findViewById(R.id.tvSCityValue);
        tvSOfficeValue = (TextView) findViewById(R.id.tvSOfficeValue);

        tvSRoomValue = (TextView) findViewById(R.id.tvSRoomValue);

        tvSStartTime = (TextView) findViewById(R.id.tvSStartTime);
        tvSStartTimeValue = (TextView) findViewById(R.id.tvSStartTimeValue);

        tvSEndTime = (TextView) findViewById(R.id.tvSEndTime);
        tvSEndTimeValue = (TextView) findViewById(R.id.tvSEndTimeValue);
        tvSAgendaValue = (TextView) findViewById(R.id.tvsAgendaValue);

        tvSDate = (TextView) findViewById(R.id.tvSDate);
        tvSDateValue = (TextView) findViewById(R.id.tvSDateValue);


        vwCity = (View) findViewById(R.id.vwCity);
        vwRoom = (View) findViewById(R.id.vwRoom);
        vwWorkspace = (View) findViewById(R.id.vwWorkspace);
        vwStartTime = (View) findViewById(R.id.vwStartTime);
        vwEndTime = (View) findViewById(R.id.vwEndTime);
        vwdate = (View) findViewById(R.id.vwDate);
        vwParticipants = (View)findViewById(R.id.vwParticipants);
        vwServices = (View)findViewById(R.id.vwServices);

        ivWorkspace = (ImageView)findViewById(R.id.ivWorkspace);
        llLocation = (LinearLayout)findViewById(R.id.llLocation);
        llRoomDetails = (LinearLayout)findViewById(R.id.llRoom);
        llSchedule = (LinearLayout)findViewById(R.id.llSchedule);
        llAgenda = (LinearLayout)findViewById(R.id.llAgenda);

        tvBulCity = (TextView) findViewById(R.id.tvBulCity);
        tvBulRoom = (TextView)findViewById(R.id.tvBulRoom);
        tvBulDate = (TextView)findViewById(R.id.tvBulDate);
        tvBulStartTime = (TextView)findViewById(R.id.tvBulStartTime);
        tvBulEndTime = (TextView)findViewById(R.id.tvBulEndTime);
        tvBulAgenda = (TextView)findViewById(R.id.tvBulAgenda);


        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvSRoomCapacity.setVisibility(View.GONE);
        tvSRoomCapacityValue.setVisibility(View.GONE);
        tvSWorkspaceValue.setVisibility(View.GONE);
        tvSCity.setVisibility(View.GONE);
        tvSCityValue.setVisibility(View.GONE);
        tvSOfficeValue.setVisibility(View.GONE);

        tvSRoomValue.setVisibility(View.GONE);
        tvSStartTime.setVisibility(View.GONE);
        tvSStartTimeValue.setVisibility(View.GONE);
        tvSEndTime.setVisibility(View.GONE);
        tvSEndTimeValue.setVisibility(View.GONE);
        tvSDate.setVisibility(View.GONE);
        tvSDateValue.setVisibility(View.GONE);
        llLocation.setVisibility(View.GONE);
        llRoomDetails.setVisibility(View.GONE);
        llSchedule.setVisibility(View.GONE);

        tvBulCity.setVisibility(View.GONE);
        tvBulRoom.setVisibility(View.GONE);
        tvBulDate.setVisibility(View.GONE);
        tvBulStartTime.setVisibility(View.GONE);
        tvBulEndTime.setVisibility(View.GONE);

        llParticipants.setVisibility(View.GONE);
        rcvPreviewParticipants.setVisibility(View.GONE);

        vwCity.setVisibility(View.GONE);
        vwWorkspace.setVisibility(View.GONE);
        vwRoom.setVisibility(View.GONE);
        vwStartTime.setVisibility(View.GONE);
        vwEndTime.setVisibility(View.GONE);
        vwdate.setVisibility(View.GONE);
        vwParticipants.setVisibility(View.GONE);

        if(lsType.equals("meeting"))
        {
            ivWorkspace.setImageDrawable(getResources().getDrawable(R.drawable.meeting_hall));
        }
        else if(lsType.equals("conference"))
        {
            ivWorkspace.setImageDrawable(getResources().getDrawable(R.drawable.confrnce_hall));
        }
        else if(lsType.equals("board"))
        {
            ivWorkspace.setImageDrawable(getResources().getDrawable(R.drawable.board_room));
        }
        else if(lsType.equals("training"))
        {
            ivWorkspace.setImageDrawable(getResources().getDrawable(R.drawable.trainin_hall));
        }
        else
        {
            ivWorkspace.setImageDrawable(getResources().getDrawable(R.drawable.meeting_hall));
        }


        if ((!(lsCity.isEmpty()) && lsCity != null) && (!(lsOffice.isEmpty()) && lsOffice != null)) {
            llLocation.setVisibility(View.VISIBLE);
            tvSCity.setVisibility(View.VISIBLE);
            tvSCityValue.setVisibility(View.VISIBLE);
            tvSCityValue.setText(lsCity);
            vwCity.setVisibility(View.VISIBLE);
            tvBulCity.setVisibility(View.VISIBLE);
            tvSOfficeValue.setVisibility(View.VISIBLE);
            tvSOfficeValue.setText(lsOffice);
        } else {
            llLocation.setVisibility(View.GONE);
            tvSCity.setVisibility(View.GONE);
            tvSCityValue.setVisibility(View.GONE);
            tvBulCity.setVisibility(View.GONE);
            vwCity.setVisibility(View.GONE);
        }

     /*   if (!(lsOffice.isEmpty()) && lsOffice != null) {

            tvSOffice.setVisibility(View.VISIBLE);
            tvSOfficeValue.setVisibility(View.VISIBLE);
            tvSOfficeValue.setText(lsOffice);
        } else {
            tvSOffice.setVisibility(View.GONE);
            tvSOfficeValue.setVisibility(View.GONE);
        }*/

        if((lsCity.isEmpty() || lsCity == null) && (lsOffice.isEmpty() || lsOffice == null))
        {
            llLocation.setVisibility(View.GONE);
        }
        else
        {
            llLocation.setVisibility(View.VISIBLE);
        }

        if (!(lsType.isEmpty()) && lsType != null) {
            tvSWorkspaceValue.setVisibility(View.VISIBLE);
            tvSWorkspaceValue.setText(capitalizeFirstLetter(lsType)+" room");
            vwWorkspace.setVisibility(View.VISIBLE);
        } else {
            tvSWorkspaceValue.setVisibility(View.GONE);
            vwWorkspace.setVisibility(View.GONE);
        }

        if ((!(lsRoom.isEmpty()) && lsRoom != null) && (!(lsCapacity.isEmpty()) && lsCapacity != null)) {
            llRoomDetails.setVisibility(View.VISIBLE);
            tvBulRoom.setVisibility(View.VISIBLE);

            tvSRoomValue.setVisibility(View.VISIBLE);
            tvSRoomValue.setText(lsRoom);
            vwRoom.setVisibility(View.VISIBLE);
            tvSRoomCapacity.setVisibility(View.VISIBLE);
            tvSRoomCapacityValue.setVisibility(View.VISIBLE);
            tvSRoomCapacityValue.setText(lsCapacity);
        } else {
            llRoomDetails.setVisibility(View.GONE);
            tvBulRoom.setVisibility(View.GONE);

            tvSRoomValue.setVisibility(View.GONE);
            vwRoom.setVisibility(View.GONE);
            tvSRoomCapacity.setVisibility(View.GONE);
            tvSRoomCapacityValue.setVisibility(View.GONE);
        }


        if (!(lsStartTime.isEmpty()) && lsStartTime != null) {
            llSchedule.setVisibility(View.VISIBLE);
            tvBulStartTime.setVisibility(View.VISIBLE);
            tvSStartTime.setVisibility(View.VISIBLE);
            tvSStartTimeValue.setVisibility(View.VISIBLE);
            tvSStartTimeValue.setText(lsStartTime);
            vwStartTime.setVisibility(View.VISIBLE);
        } else {
            llSchedule.setVisibility(View.GONE);
            tvBulStartTime.setVisibility(View.GONE);
            tvSStartTime.setVisibility(View.GONE);
            tvSStartTimeValue.setVisibility(View.GONE);
            vwStartTime.setVisibility(View.GONE);
        }

        if (!(lsEndTime.isEmpty()) && lsEndTime != null) {
            llSchedule.setVisibility(View.VISIBLE);
            tvBulEndTime.setVisibility(View.VISIBLE);
            tvSEndTime.setVisibility(View.VISIBLE);
            tvSEndTimeValue.setVisibility(View.VISIBLE);
            tvSEndTimeValue.setText(lsEndTime);
            vwEndTime.setVisibility(View.VISIBLE);
        } else {
            llSchedule.setVisibility(View.GONE);
            tvBulEndTime.setVisibility(View.GONE);
            tvSEndTime.setVisibility(View.GONE);
            tvSEndTimeValue.setVisibility(View.GONE);
            vwEndTime.setVisibility(View.GONE);
        }

        if (!(lsDate.isEmpty()) && lsDate != null) {
            llSchedule.setVisibility(View.VISIBLE);
            tvBulDate.setVisibility(View.VISIBLE);
            tvSDate.setVisibility(View.VISIBLE);
            tvSDateValue.setVisibility(View.VISIBLE);
            tvSDateValue.setText(lsDate);
            vwdate.setVisibility(View.VISIBLE);
        } else {
            llSchedule.setVisibility(View.GONE);
            tvBulDate.setVisibility(View.GONE);
            tvSDate.setVisibility(View.GONE);
            tvSDateValue.setVisibility(View.GONE);
            vwdate.setVisibility(View.GONE);
        }

        if (!(lsAgenda.isEmpty()) && lsAgenda != null) {
            llAgenda.setVisibility(View.VISIBLE);
            tvBulAgenda.setVisibility(View.VISIBLE);
            tvSAgendaValue.setVisibility(View.VISIBLE);
            tvSAgendaValue.setText(lsAgenda);
            vwAgenda.setVisibility(View.VISIBLE);
        } else {
            llAgenda.setVisibility(View.GONE);
            tvBulAgenda.setVisibility(View.GONE);
            tvSAgendaValue.setVisibility(View.GONE);
            vwAgenda.setVisibility(View.GONE);
        }

        if(alParticipants.size() > 0)
        {
            llParticipants.setVisibility(View.VISIBLE);
            rcvPreviewParticipants.setVisibility(View.VISIBLE);
            vwParticipants.setVisibility(View.VISIBLE);
        }
        else
        {
            llParticipants.setVisibility(View.GONE);
            rcvPreviewParticipants.setVisibility(View.GONE);
            vwParticipants.setVisibility(View.GONE);
        }

        if(alAttachment.size() > 0)
        {
            llAttachments.setVisibility(View.VISIBLE);
            rcvPreviewAttachments.setVisibility(View.VISIBLE);
            vwAttachments.setVisibility(View.VISIBLE);
        }
        else
        {
            llAttachments.setVisibility(View.GONE);
            rcvPreviewAttachments.setVisibility(View.GONE);
            vwAttachments.setVisibility(View.GONE);
        }

        if(alServices.size() > 0)
        {
            llServices.setVisibility(View.VISIBLE);
            rcvPreviewServices.setVisibility(View.VISIBLE);
            vwServices.setVisibility(View.VISIBLE);
        }
        else
        {
            llServices.setVisibility(View.GONE);
            rcvPreviewServices.setVisibility(View.GONE);
            vwServices.setVisibility(View.GONE);
        }

        if((lsDate.isEmpty() || lsDate == null) && (lsStartTime.isEmpty() || lsStartTime == null) && (lsEndTime.isEmpty() || lsEndTime == null))
        {
            llSchedule.setVisibility(View.GONE);
        }
        else
        {
            llSchedule.setVisibility(View.VISIBLE);
        }

        showParticipant();
        showAttachments();
        showServices();
    }

    public String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }


    public void showParticipant() {
        llmParticipant = new LinearLayoutManager(context);
        rcvPreviewParticipants.setLayoutManager(llmParticipant);
        participantAdapter = new PreviewParticipantAdapter(context, alParticipants);
        rcvPreviewParticipants.setAdapter(participantAdapter);
       // participantAdapter.notifyItemInserted(alParticipants.size() - 1);

    }

    public void showAttachments() {
        llmAttachments = new LinearLayoutManager(context);
        rcvPreviewAttachments.setLayoutManager(llmAttachments);
        attachmentAdapter = new PreviewAttachmentAdapter(context, alAttachment);
        rcvPreviewAttachments.setAdapter(attachmentAdapter);
        // participantAdapter.notifyItemInserted(alParticipants.size() - 1);

    }

    public void showServices() {
        llmServices = new LinearLayoutManager(context);
        rcvPreviewServices.setLayoutManager(llmServices);
        servicesAdapter = new PreviewServicesAdapter(context, alServices);
        rcvPreviewServices.setAdapter(servicesAdapter);
        // participantAdapter.notifyItemInserted(alParticipants.size() - 1);

    }

}
