package com.WeApplify.Tap2Meet.UI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.WeApplify.Tap2Meet.Adapters.ETimeAdapter;
import com.WeApplify.Tap2Meet.Adapters.MTimeAdapter;
import com.WeApplify.Tap2Meet.Adapters.NTimeAdapter;
import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.BaseActivity;
import com.WeApplify.Tap2Meet.Models.ETimeModel;
import com.WeApplify.Tap2Meet.Models.MTimeModel;
import com.WeApplify.Tap2Meet.Models.NTimeModel;
import com.WeApplify.Tap2Meet.Models.SortingOrderModel;
import com.WeApplify.Tap2Meet.R;
import com.WeApplify.Tap2Meet.Services.CalendarView;
import com.WeApplify.Tap2Meet.Services.CustomServices;
import com.WeApplify.Tap2Meet.Services.CustomToast;
import com.WeApplify.Tap2Meet.Services.DataTransmitter;
import com.WeApplify.Tap2Meet.Services.Debugger;
import com.WeApplify.Tap2Meet.Services.PushyAPI;
import com.WeApplify.Tap2Meet.Services.TransperantProgressDialog;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.MeetingParticipants;
import com.WeApplify.Tap2Meet.Tables.UserLogin;
import com.WeApplify.Tap2Meet.Tables.WorkspaceBooked;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by harshagulati on 13/06/17.
 */

public class Schedule extends BaseActivity implements View.OnClickListener {

    public static final String TAG = "Schedule";

    Activity activity;
    Context context;
    TextView tvReschedule;
    String gDate, gNxtDate, gNxtDate1, gNxtDate2, gNxtDate3, lsStartTime = "", lsEndTime = "", lsDate = "", lsMeetDtBeforeModify;
    TextView tvCurrentDate, tvCurrentDay, tvNextDate, tvNextDay, tvNxt1Dt, tvNxt1Day, tvNxt2Dt, tvNxt2Day, tvNxt3Dt, tvNxt3Day;
    FrameLayout flCurrentDt, flNextDt, flNxt1Dt, flNxt2Dt, flNxt3Dt, flNxt4Dt;
    ImageView ivCurrent, ivNext, ivNxt1, ivNxt2, ivNxt3, ivNxt4, ivRefresh;
    ImageView ivDCurrent, ivDNext, ivDNxt1, ivDNxt2, ivDNxt3, ivDNxt4, ivCalender;
    int startSo, endSo;
    TextView tvSelectedDate, tvStartTime, tvEndTime;

    ArrayList<MTimeModel> alMTime;
    ArrayList<SortingOrderModel> alSortingOrderData;
    MTimeAdapter mtimeAdapter;
    int liSelectedFirst = -1,
            liSelected = -1;
    int stSortingOrder, etSortingOrder;

    GridLayoutManager glmMorning;
    RecyclerView rcvMTime;

    String bookingId, meetingDate, startTime, endTime, agenda, employeeId, workspaceId, status, title;
    TextView tvBack;
    TransperantProgressDialog pd;
    SimpleDateFormat sdtf;
    ArrayList<String> alParticipant,alDeviceKeys;
    List<String> deviceTokens = new ArrayList<>();
    SharedPreferences sp;
    String lsEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule);
        setTitle("Schedule");

        context = this;
        activity = this;

        tvBack = (TextView) findViewById(R.id.tvBack);
        tvBack.setVisibility(View.INVISIBLE);
        initLayout();
    }

    public void initLayout() {
        alMTime = new ArrayList<>();
        alSortingOrderData = new ArrayList<>();
        alParticipant = new ArrayList<>();
        alDeviceKeys = new ArrayList<>();

        pd = new TransperantProgressDialog(context);
        sp = getSharedPreferences(App.LOGINCREDENTIALS,context.MODE_PRIVATE);
        lsEmail = sp.getString(UserLogin.UL_EMAIL, "");

        lsMeetDtBeforeModify = getIntent().getStringExtra(WorkspaceBooked.WB_MEETINGDATE);

        sdtf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            if(sdtf.parse(lsMeetDtBeforeModify).before(sdtf.parse(App.getDate())))
            {
                meetingDate = App.getDate();
            }
            else
            {
                meetingDate = getIntent().getStringExtra(WorkspaceBooked.WB_MEETINGDATE);
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        bookingId = getIntent().getStringExtra(WorkspaceBooked.WB_BOOKINGID);
        startTime = getIntent().getStringExtra(WorkspaceBooked.WB_PLANNEDSTARTTIME);
        endTime = getIntent().getStringExtra(WorkspaceBooked.WB_PLANNEDENDTIME);
        agenda = getIntent().getStringExtra(WorkspaceBooked.WB_AGENDA);
        employeeId = getIntent().getStringExtra(WorkspaceBooked.WB_EMPLOYEE);
        workspaceId = getIntent().getStringExtra(WorkspaceBooked.WB_WORKSPACEID);
        status = getIntent().getStringExtra(WorkspaceBooked.WB_STATUS);
        title = getIntent().getStringExtra(WorkspaceBooked.WB_TITLE);
        startSo = getIntent().getIntExtra(WorkspaceBooked.WB_ST_SO, 0);
        endSo = getIntent().getIntExtra(WorkspaceBooked.WB_ET_SO, 0);

        getMTimeLovs(meetingDate);

        ivRefresh = (ImageView) findViewById(R.id.ivRefresh);
        rcvMTime = (RecyclerView) findViewById(R.id.rcvMTime);
        tvReschedule = (TextView) findViewById(R.id.tvReschedule);
        ivCurrent = (ImageView) findViewById(R.id.ivCurrent);
        ivNext = (ImageView) findViewById(R.id.ivNext);
        ivNxt1 = (ImageView) findViewById(R.id.ivNxt1);
        ivNxt2 = (ImageView) findViewById(R.id.ivNxt2);
        ivNxt3 = (ImageView) findViewById(R.id.ivNxt3);
        ivNxt4 = (ImageView) findViewById(R.id.ivNxt4);

        ivDCurrent = (ImageView) findViewById(R.id.ivDCurrent);
        ivDNext = (ImageView) findViewById(R.id.ivDNext);
        ivDNxt1 = (ImageView) findViewById(R.id.ivDNxt1);
        ivDNxt2 = (ImageView) findViewById(R.id.ivDNxt2);
        ivDNxt3 = (ImageView) findViewById(R.id.ivDNxt3);
        ivDNxt4 = (ImageView) findViewById(R.id.ivDNxt4);

        flCurrentDt = (FrameLayout) findViewById(R.id.flCurrentDt);
        flNextDt = (FrameLayout) findViewById(R.id.flNextDt);
        flNxt1Dt = (FrameLayout) findViewById(R.id.flNxt1Dt);
        flNxt2Dt = (FrameLayout) findViewById(R.id.flNxt2Dt);
        flNxt3Dt = (FrameLayout) findViewById(R.id.flNxt3Dt);
        flNxt4Dt = (FrameLayout) findViewById(R.id.flNxt4Dt);

        tvCurrentDate = (TextView) findViewById(R.id.tvCurrentDate);
        tvCurrentDay = (TextView) findViewById(R.id.tvCurrentDay);
        tvNextDate = (TextView) findViewById(R.id.tvNextDate);
        tvNextDay = (TextView) findViewById(R.id.tvNextDay);
        tvNxt1Dt = (TextView) findViewById(R.id.tvNxt1Dt);
        tvNxt1Day = (TextView) findViewById(R.id.tvNxt1Day);
        tvNxt2Dt = (TextView) findViewById(R.id.tvNxt2Dt);
        tvNxt2Day = (TextView) findViewById(R.id.tvNxt2Day);
        tvNxt3Dt = (TextView) findViewById(R.id.tvNxt3Dt);
        tvNxt3Day = (TextView) findViewById(R.id.tvNxt3Day);
        ivCalender = (ImageView) findViewById(R.id.ivCalender);

        tvSelectedDate = (TextView) findViewById(R.id.tvSelectedDate);
        tvStartTime = (TextView) findViewById(R.id.tvStartTime);
        tvEndTime = (TextView) findViewById(R.id.tvEndTime);

        String date = CustomServices.getFormattedDate();
        SimpleDateFormat sdf1 = new SimpleDateFormat("EEE");
        Date date1 = new Date();
        String day = sdf1.format(date1);

        gDate = CustomServices.getTodaysDate();

        tvCurrentDate.setText(date);
        tvCurrentDay.setText(day);

        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_YEAR, 1); // <--
        Date tomorrow = cal.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");

        String nxtDate = sdf.format(tomorrow);
        String nxtDay = sdf1.format(tomorrow);

        gNxtDate = sdf2.format(tomorrow);

        tvNextDate.setText(nxtDate);
        tvNextDay.setText(nxtDay);


        cal.add(Calendar.DAY_OF_YEAR, 1); // <--
        Date tomorrow1 = cal.getTime();
        String nxtDate1 = sdf.format(tomorrow1);
        String nxtDay1 = sdf1.format(tomorrow1);

        gNxtDate1 = sdf2.format(tomorrow1);

        tvNxt1Dt.setText(nxtDate1);
        tvNxt1Day.setText(nxtDay1);


        cal.add(Calendar.DAY_OF_YEAR, 1); // <--
        Date tomorrow2 = cal.getTime();
        String nxtDate2 = sdf.format(tomorrow2);
        String nxtDay2 = sdf1.format(tomorrow2);

        gNxtDate2 = sdf2.format(tomorrow2);

        tvNxt2Dt.setText(nxtDate2);
        tvNxt2Day.setText(nxtDay2);

        cal.add(Calendar.DAY_OF_YEAR, 1); // <--
        Date tomorrow3 = cal.getTime();
        String nxtDate3 = sdf.format(tomorrow3);
        String nxtDay3 = sdf1.format(tomorrow3);

        gNxtDate3 = sdf2.format(tomorrow3);

        tvNxt3Dt.setText(nxtDate3);
        tvNxt3Day.setText(nxtDay3);

        if (meetingDate.length() > 0) {
            tvSelectedDate.setText(meetingDate);
        } else {
            tvSelectedDate.setText(gDate);
        }

        try {
            if(sdtf.parse(lsMeetDtBeforeModify).before(sdtf.parse(App.getDate())))
            {
                tvStartTime.setText("");
                tvEndTime.setText("");
            }
            else
            {
                tvStartTime.setText(startTime);
                tvEndTime.setText(endTime);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        ivDCurrent.setVisibility(View.GONE);
        ivDNext.setVisibility(View.GONE);
        ivDNxt1.setVisibility(View.GONE);
        ivDNxt2.setVisibility(View.GONE);
        ivDNxt3.setVisibility(View.GONE);
        ivDNxt4.setVisibility(View.GONE);

        if (meetingDate.equals(gDate)) {
            setDateColor(ivCurrent, ivDCurrent, tvCurrentDay, tvCurrentDate);
        } else if (meetingDate.equals(gNxtDate)) {
            setDateColor(ivNext, ivDNext, tvNextDay, tvNextDate);
        } else if (meetingDate.equals(gNxtDate1)) {
            setDateColor(ivNxt1, ivDNxt1, tvNxt1Day, tvNxt1Dt);
        } else if (meetingDate.equals(gNxtDate2)) {
            setDateColor(ivNxt2, ivDNxt2, tvNxt2Day, tvNxt2Dt);
        } else if (meetingDate.equals(gNxtDate3)) {
            setDateColor(ivNxt3, ivDNxt3, tvNxt3Day, tvNxt3Dt);
        } else {
            defaultDatesColor();
            ivDNxt4.setVisibility(View.GONE);
            ivNxt4.setColorFilter(getResources().getColor(R.color.app_color_red));
            ivCalender.setColorFilter(getResources().getColor(R.color.White));
        }

        flCurrentDt.setOnClickListener(this);
        flNextDt.setOnClickListener(this);
        flNxt1Dt.setOnClickListener(this);
        flNxt2Dt.setOnClickListener(this);
        flNxt3Dt.setOnClickListener(this);
        flNxt4Dt.setOnClickListener(this);
        tvReschedule.setOnClickListener(this);
        ivRefresh.setOnClickListener(this);


    }

    public void setDateColor(ImageView ivImage, ImageView ivImage1, TextView day, TextView date) {

        defaultDatesColor();
        ivImage1.setVisibility(View.GONE);

        ivImage.setColorFilter(getResources().getColor(R.color.app_color_red));
        day.setTextColor(getResources().getColor(R.color.White));
        date.setTextColor(getResources().getColor(R.color.White));
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.flCurrentDt:
                setDate(gDate, ivDCurrent, ivCurrent, tvCurrentDay, tvCurrentDate);
                break;

            case R.id.flNextDt:
                setDate(gNxtDate, ivDNext, ivNext, tvNextDay, tvNextDate);
                break;


            case R.id.flNxt1Dt:
                setDate(gNxtDate1, ivDNxt1, ivNxt1, tvNxt1Day, tvNxt1Dt);
                break;

            case R.id.flNxt2Dt:
                setDate(gNxtDate2, ivDNxt2, ivNxt2, tvNxt2Day, tvNxt2Dt);
                break;

            case R.id.flNxt3Dt:
                setDate(gNxtDate3, ivDNxt3, ivNxt3, tvNxt3Day, tvNxt3Dt);
                break;

            case R.id.flNxt4Dt:

                setUpDate();
                defaultDatesColor();
                ivDNxt4.setVisibility(View.GONE);
                ivNxt4.setColorFilter(getResources().getColor(R.color.app_color_red));
                ivCalender.setColorFilter(getResources().getColor(R.color.White));

                if (mtimeAdapter.getSelectedCount() > 0) {
                    mtimeAdapter.clearSelection();
                    tvEndTime.setText("");
                    tvStartTime.setText("");
                    startTime = "";
                    endTime = "";
                }
                break;

            case R.id.tvReschedule:
                if (callSave() > 0) {
                    CustomToast.showToast(activity, "Succesfully rescheduled", 50);
                    String lsMessage = "Your meeting got rescheduled";
                    sendSamplePush(bookingId, lsMessage, lsMeetDtBeforeModify, tvSelectedDate.getText().toString(),startTime, endTime);
                    setResult(RESULT_OK);
                    finish();
                } else {
                    CustomToast.showToast(activity, "Succesfully rescheduled", 50);
                }
                break;

            case R.id.ivRefresh:
                if (mtimeAdapter.getSelectedCount() > 0) {
                    mtimeAdapter.clearSelection();
                    tvStartTime.setText("");
                    tvEndTime.setText("");
                    startTime = "";
                    endTime = "";
                }
                break;

        }

    }

    public void sendSamplePush(String lsBookingId , final String lsMessage , final String lsMeetingDateBeforeChange, final String bookingDate, final String plannedStartTime, final String plannedEndTime)
    {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        final ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();

        String lsSql = "SELECT * FROM "+ MeetingParticipants.TABLENAME+" WHERE "+MeetingParticipants.MP_BOOKINGID+" = '"+lsBookingId+"'";

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alParticipant.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(MeetingParticipants.MP_BOOKINGID)) {

                                String participantMail = groupData.getString(MeetingParticipants.MP_EMPLOYEEEMAIL);

                                alParticipant.add(participantMail);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            getDeviceIds(lsMessage, lsMeetingDateBeforeChange, bookingDate,plannedStartTime,plannedEndTime);
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });
    }

    public void getDeviceIds(final String lsMessage , final String lsMeetingDateBeforeChange, final String bookingDate, final String plannedStartTime, final String plannedEndTime) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();
        String lsSql;

        for(int i= 0; i<= alParticipant.size()-1; i++)
        {
            String email = alParticipant.get(i);
            if(!email.equals(lsEmail))
            {
            lsSql = "SELECT "+ UserLogin.UL_DEVICEKEY+" FROM " + UserLogin.TABLENAME + " WHERE " + UserLogin.UL_EMAIL + " = '" + email + "'";
            alSql.add(lsSql);
             }
        }


        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    alDeviceKeys.clear();

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        JSONArray resultsArray = dataArray.getJSONArray(i);
                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(UserLogin.UL_DEVICEKEY)) {

                                String lsDeviceKey = groupData.getString(UserLogin.UL_DEVICEKEY);

                                alDeviceKeys.add(lsDeviceKey);
                            }

                        }

                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            for (int i = 0; i <= alDeviceKeys.size()-1;i++)
                            {
                                deviceTokens.add(alDeviceKeys.get(i));
                            }

                            // Convert to String[] array
                            String[] to = deviceTokens.toArray(new String[deviceTokens.size()]);

                            Map<String, String> payload = new HashMap<>();

                            // Add "message" parameter to payload
                            payload.put(App.NOTIFICATION, lsMessage);
                            payload.put(App.MEETING_DATE,bookingDate);
                            payload.put(App.MEETING_STARTTIME,plannedStartTime);
                            payload.put(App.MEETING_ENDTIME,plannedEndTime);
                            payload.put("MeetingDateBeforeChange", lsMeetingDateBeforeChange);
                            //    payload.put(App.MEETING_NOTIFICATION,lsMeetingDate);


                            // iOS notification fields
                            Map<String, Object> notification = new HashMap<>();

                            notification.put("badge", 1);
                            notification.put("sound", "ping.aiff");
                            notification.put("body", lsMessage);

                            // Prepare the push request
                            final PushyAPI.PushyPushRequest push = new PushyAPI.PushyPushRequest(payload, to, notification);

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        PushyAPI.sendPush(push);
                                    } catch (Exception ex) {
                                        //ex.printStackTrace();
                                        System.out.println("Exception called exception number is 1001 : " + ex.toString());
                                    }
                                }
                            }).start();
                        }
                    }));

                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }

            }

        });

    }


    public int callSave() {
        ArrayList<String> arrayList = new ArrayList<>();
        String lsSql;

        String lsTodaysDate = App.getDate();
        String lsMeetingDate = tvSelectedDate.getText().toString();
        lsStartTime = tvStartTime.getText().toString();
        lsEndTime = tvEndTime.getText().toString();

        lsSql = "UPDATE " + WorkspaceBooked.TABLENAME + " SET " + WorkspaceBooked.WB_MEETINGDATE + " = '" + lsMeetingDate + "' , "
                + WorkspaceBooked.WB_PLANNEDSTARTTIME + " = '" + startTime + "' , "
                + WorkspaceBooked.WB_PLANNEDENDTIME + " = '" + endTime + "' ,"
                + WorkspaceBooked.WB_ST_SO + " = " + stSortingOrder + " , " + WorkspaceBooked.WB_ET_SO + " = " + etSortingOrder +
                " WHERE " + WorkspaceBooked.WB_BOOKINGID + " = '" + bookingId + "'";
        arrayList.add(lsSql);

        if (tvStartTime.length() > 0 && tvEndTime.length() > 0 && tvSelectedDate.length() > 0) {
            callCloud(arrayList);
            return 1;
        } else {
            CustomToast.showToast(activity, "Select schedule for meeting", 50);
            return -1;
        }
    }

    public void callCloud(ArrayList alSql) {

        DataTransmitter dt = new DataTransmitter();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        String lsSql;
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);

            dt.setLink(App.getSQLURL());
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            } else {

            }
        }

    }


    public void setUpDate() {
        CalendarView calendarPop = new CalendarView(activity, context, true);
        String lsDate = tvSelectedDate.getText().toString();
        if (lsDate == null || lsDate.equals("")) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            lsDate = String.valueOf(sdf.format(new Date()));
        }

        CalendarView.CVOnDateChanged mOnDateChanged = new CalendarView.CVOnDateChanged() {
            @Override
            public void CVDateChanged(TextView tvDate, String lsDate) {
                Debugger.debug(TAG, "DAte selected is " + lsDate);
                tvSelectedDate.setText(lsDate);
                getMTimeLovs(lsDate);
            }
        };
        calendarPop.setOnDateChanged(mOnDateChanged);
        calendarPop.setupPopUp(tvSelectedDate, lsDate, context.getResources().getColor(R.color.colorPrimary));
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void setDate(String date, ImageView ivImg1, ImageView ivImg2, TextView tvText1, TextView tvText2) {
        tvSelectedDate.setText(date);
        defaultDatesColor();

        getMTimeLovs(date);
        lsDate = date;

        ivImg1.setVisibility(View.GONE);
        ivImg2.setColorFilter(getResources().getColor(R.color.app_color_red));
        tvText1.setTextColor(getResources().getColor(R.color.White));
        tvText2.setTextColor(getResources().getColor(R.color.White));

        if (mtimeAdapter.getSelectedCount() > 0) {
            mtimeAdapter.clearSelection();
            tvEndTime.setText("");
            tvStartTime.setText("");
            startTime = "";
            endTime = "";
        }
    }


    public void defaultDatesColor() {

        ivDCurrent.setVisibility(View.VISIBLE);
        ivDNext.setVisibility(View.VISIBLE);
        ivDNxt1.setVisibility(View.VISIBLE);
        ivDNxt2.setVisibility(View.VISIBLE);
        ivDNxt3.setVisibility(View.VISIBLE);
        ivDNxt4.setVisibility(View.VISIBLE);


        ivCurrent.setColorFilter(getResources().getColor(R.color.White));
        tvCurrentDay.setTextColor(getResources().getColor(R.color.app_color_red));
        tvCurrentDate.setTextColor(getResources().getColor(R.color.app_color_red));

        ivNext.setColorFilter(getResources().getColor(R.color.White));
        tvNextDay.setTextColor(getResources().getColor(R.color.app_color_red));
        tvNextDate.setTextColor(getResources().getColor(R.color.app_color_red));

        ivNxt1.setColorFilter(getResources().getColor(R.color.White));
        tvNxt1Dt.setTextColor(getResources().getColor(R.color.app_color_red));
        tvNxt1Day.setTextColor(getResources().getColor(R.color.app_color_red));

        ivNxt2.setColorFilter(getResources().getColor(R.color.White));
        tvNxt2Day.setTextColor(getResources().getColor(R.color.app_color_red));
        tvNxt2Dt.setTextColor(getResources().getColor(R.color.app_color_red));

        ivNxt3.setColorFilter(getResources().getColor(R.color.White));
        tvNxt3Day.setTextColor(getResources().getColor(R.color.app_color_red));
        tvNxt3Dt.setTextColor(getResources().getColor(R.color.app_color_red));

        ivNxt4.setColorFilter(getResources().getColor(R.color.White));
        ivCalender.setColorFilter(getResources().getColor(R.color.app_color_red));
    }

    public void getMTimeLovs(final String date) {
        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        JSONArray jsonArray = new JSONArray();
        ArrayList alSql = new ArrayList();
        String lsSql;

        lsSql = "SELECT * FROM " + Constants.TIME_TABLENAME + " ORDER By " + Constants.TIME_SORTINGORDER + " ASC ";

        alSql.add(lsSql);

        for (int i = 0; i < alSql.size(); i++) {
            try {
                lsSql = alSql.get(i).toString();
                JSONObject json = new JSONObject();
                json.put("OPERATION", lsSql);
                jsonArray.put(json);
            } catch (Exception e) {
                Log.v(TAG, "Error is " + e.toString());
            }
        }

        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

            pd.show();
        }  //if JSONArray is !null

        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject;
                try {

                    jsonObject = new JSONObject(data);
                    JSONArray dataArray = jsonObject.getJSONArray("results");

                    for (int i = 0; i < dataArray.length(); i++)  //This will be resultArray for individual SQL Statement.
                    {
                        alMTime.clear();
                        JSONArray resultsArray = dataArray.getJSONArray(i);

                        for (int x = 0; x < resultsArray.length(); x++) {
                            JSONObject groupData = resultsArray.getJSONObject(x);
                            if (groupData.has(Constants.TIME_ID)) {

                                String lsTime = groupData.getString(Constants.TIME_TEXT);
                                int liSo = groupData.getInt(Constants.TIME_SORTINGORDER);
                                String lsTimePhase = groupData.getString(Constants.TIME_PHASE);

                                Bundle b = new Bundle();
                                b.putString(Constants.TIME_TEXT, lsTime);
                                b.putInt(Constants.TIME_SORTINGORDER, liSo);
                                b.putString(Constants.TIME_PHASE, lsTimePhase);

                                MTimeModel model = new MTimeModel(b);
                                alMTime.add(model);
                            }
                        }
                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            //  showMTime();
                            checkAvailaility(date);
                        }
                    }));
                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());
                }
            }

        });
    }

    public void checkAvailaility(String lsDate) {

        java.util.Date date = new Date();
        SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
        String currentDate = sf.format(date);
        //String currentDate = String.valueOf(date);

        DataTransmitter dt = new DataTransmitter();
        String link = App.getDataURL();
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        ArrayList alSql = new ArrayList();
        String lssql = "SELECT wb_st_so,wb_et_so,t.time_text,t.time_sortingorder,w.wb_plannedendtime " +
                "from time_lovs t,workspace_booked w " +
                "where t.time_text=w.wb_plannedstarttime and w.wb_meetingdate='" + lsDate + "' " +
                "and w.wb_workspaceid='" + workspaceId + "' AND " + WorkspaceBooked.WB_BOOKING_STATUS + " = 'A'";
        alSql.add(lssql);


        for (int i = 0; i < alSql.size(); i++) {
            try {
                lssql = alSql.get(i).toString();
                jsonObject.put("OPERATION", lssql);
                jsonArray.put(jsonObject);
            } catch (Exception e) {
                Log.d(TAG, "Error is" + e.toString());
            }
        }
        if (jsonArray != null) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("SQL", jsonArray.toString());
            dt.setPostParams(hm);
            dt.setType(DataTransmitter.DATA);
            dt.setLink(link);
            if (networkInfo != null && networkInfo.isConnected()) {
                dt.execute();
            }

        }
        dt.setOnResponseRevieved(new DataTransmitter.onResponseReceived() {
            @Override
            public void onResponse(String data) {
                JSONObject jsonObject1;
                try {
                    jsonObject1 = new JSONObject(data);
                    JSONArray jsonArray1 = jsonObject1.getJSONArray("results");
                    for (int i = 0; i < jsonArray1.length(); i++) {

                        alSortingOrderData.clear();

                        JSONArray resultjarray = jsonArray1.getJSONArray(i);
                        boolean lbRefresh = false;
                        for (int x = 0; x < resultjarray.length(); x++) {
                            JSONObject groupData = resultjarray.getJSONObject(x);
                            if (groupData.has(WorkspaceBooked.WB_PLANNEDENDTIME)) {
                                int liStartSO = groupData.getInt(WorkspaceBooked.WB_ST_SO);
                                int liEndSO = groupData.getInt(WorkspaceBooked.WB_ET_SO);

                                for (int y = 0; y < alMTime.size(); y++) {
                                    int liCurrentSO = alMTime.get(y).getSortingOrder();
                                    if (liCurrentSO >= liStartSO && liCurrentSO <= liEndSO) {
                                        lbRefresh = true;
                                       /* if(liCurrentSO >= startSo && liCurrentSO <= endSo)
                                        {
                                                alMTime.get(y).setBooked1(true);
                                        }
                                        else
                                        {
                                            alMTime.get(y).setBooked(true);
                                        }
*/
                                        alMTime.get(y).setBooked(true);
                                    }
                                }

                                Bundle b = new Bundle();
                                if (liStartSO == startSo && liEndSO == endSo) {
                                    b.putInt("startOrder1", liStartSO);
                                    b.putInt("endOrder1", liEndSO);
                                } else {
                                    b.putInt("startOrder", liStartSO);
                                    b.putInt("endOrder", liEndSO);
                                }


                                SortingOrderModel model = new SortingOrderModel(b);
                                alSortingOrderData.add(model);

                            }
                        }
                    }
                    runOnUiThread(new Thread(new Runnable() {
                        public void run() {
                            // checkAvailaility();
                            pd.hide();
                            showMTime();

                        }
                    }));
                } catch (Exception e) {
                    Debugger.debug(TAG, "Exception in getting result 1010 " + e.toString());

                }
            }
        });
    }


    public void showMTime() {
        glmMorning = new GridLayoutManager(context, 5);
        rcvMTime.setLayoutManager(glmMorning);
        mtimeAdapter = new MTimeAdapter(context, alMTime);
        rcvMTime.setAdapter(mtimeAdapter);
        mtimeAdapter.setMultiple(true);

        try {
            if(sdtf.parse(lsMeetDtBeforeModify).before(sdtf.parse(App.getDate())))
            {
                Toast.makeText(context,"You missed the meeting , Reschedule it...Thank You!!",Toast.LENGTH_SHORT).show();
            }
            else
            {
                if ((tvSelectedDate.getText().toString()).equals(meetingDate)) {
                    if ((startSo != -1) && (endSo != -1)) {
                        for (int i = 0; i < alMTime.size(); i++) {
                            int sortingOrder = alMTime.get(i).getSortingOrder();
                            if (startSo == sortingOrder) {
                                liSelectedFirst = i;
                                startTime = alMTime.get(i).getTime();
                                stSortingOrder = alMTime.get(i).getSortingOrder();
                            }

                            if (endSo == sortingOrder) {
                                liSelected = i;
                                endTime = alMTime.get(i).getTime();
                                etSortingOrder = alMTime.get(i).getSortingOrder();

                                break;
                            }
                        }

                        for (int i = liSelected; i >= liSelectedFirst; i--) {
                            mtimeAdapter.toggleSelection(i);
                            mtimeAdapter.notifyItemChanged(i);
                        }
                    }
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        mtimeAdapter.setItemClickInterface(new MTimeAdapter.itemClickIterface() {
            @Override
            public void itemClick(int position) {

                boolean lbCheck = false;

                if (mtimeAdapter.getSelectedCount() == 0) {
                    liSelectedFirst = position;
                    mtimeAdapter.toggleSelection(position);
                    mtimeAdapter.notifyItemChanged(position);
                    tvStartTime.setText(alMTime.get(position).getTime());
                    startTime = alMTime.get(position).getTime();
                    stSortingOrder = alMTime.get(position).getSortingOrder();
                } else if (mtimeAdapter.getSelectedCount() == 1) {
                    String eTime = alMTime.get(position).getTime();
                    int etSorting = alMTime.get(position).getSortingOrder();

                    for (int i = 0; i < alSortingOrderData.size(); i++) {

                        int st_so = alSortingOrderData.get(i).getStartSo();
                        int et_so = alSortingOrderData.get(i).getEndSo();

                        if ((stSortingOrder <= st_so) && (etSorting >= et_so)) {
                            //  Toast.makeText(context, "yes", Toast.LENGTH_SHORT).show();
                            lbCheck = true;
                            break;
                        }
                    }

                    if (lbCheck == false) {
                        if (!CustomServices.TimeValidator(tvStartTime.getText().toString(), eTime)) {
                            tvEndTime.setText("");
                            CustomToast.showToast(activity, "End Time cannot be less than From Time", 50);
                            return;
                        } else {
                            liSelected = position;
                            for (int i = position; i >= liSelectedFirst + 1; i--) {
                                mtimeAdapter.toggleSelection(i);
                                mtimeAdapter.notifyItemChanged(i);
                            }
                            tvEndTime.setText(alMTime.get(position).getTime());
                            etSortingOrder = alMTime.get(position).getSortingOrder();
                            endTime = alMTime.get(position).getTime();

                        }
                    } else {
                        mtimeAdapter.clearSelection();
                        tvStartTime.setText("");
                        tvEndTime.setText("");
                        startTime = "";
                        endTime = "";
                        CustomToast.showToast(activity, "This slot already booked. Choose other one.", 50);
                    }

                } else if (mtimeAdapter.getSelectedCount() >= 1) {

                 /*   int etSorting = alMTime.get(position).getSortingOrder();

                    for (int i = 0; i < alSortingOrderData.size(); i++) {

                        int st_so = alSortingOrderData.get(i).getStartSo();
                        int et_so = alSortingOrderData.get(i).getEndSo();

                        if ((stSortingOrder <= st_so) && (etSorting >= et_so)) {
                          //  Toast.makeText(context, "yes", Toast.LENGTH_SHORT).show();
                            lbCheck = true;
                            break;
                        }
                    }*/


                    if (position == liSelectedFirst) {
                        CustomToast.showToast(activity, "Cannot select same slot for End-time...select other one", 50);
                        return;
                    } else if (position < liSelectedFirst) {
                        liSelectedFirst = position;
                        int stSorting = alMTime.get(position).getSortingOrder();
                        for (int i = 0; i < alSortingOrderData.size(); i++) {

                            int st_so = alSortingOrderData.get(i).getStartSo();
                            int et_so = alSortingOrderData.get(i).getEndSo();

                            if ((stSorting <= st_so) && (etSortingOrder >= et_so)) {
                                //  Toast.makeText(context, "yes", Toast.LENGTH_SHORT).show();
                                lbCheck = true;
                                break;
                            }
                        }
                        if (lbCheck == false) {
                            mtimeAdapter.clearSelection();
                            for (int i = position; i <= liSelected; i++) {
                                mtimeAdapter.toggleSelection(i);
                                mtimeAdapter.notifyItemChanged(i);
                            }
                            tvStartTime.setText(alMTime.get(position).getTime());
                            startTime = alMTime.get(position).getTime();
                            stSortingOrder = alMTime.get(position).getSortingOrder();
                        } else {
                            mtimeAdapter.clearSelection();
                            tvStartTime.setText("");
                            tvEndTime.setText("");
                            startTime = "";
                            endTime = "";
                            // Toast.makeText(context, "Cannot book this slot..choose other one", Toast.LENGTH_SHORT).show();
                            CustomToast.showToast(activity, "This slot already booked. Choose other one.", 50);
                        }
                    } else {
                        liSelected = position;
                        int etSorting = alMTime.get(position).getSortingOrder();
                        for (int i = 0; i < alSortingOrderData.size(); i++) {

                            int st_so = alSortingOrderData.get(i).getStartSo();
                            int et_so = alSortingOrderData.get(i).getEndSo();

                            if ((stSortingOrder <= st_so) && (etSorting >= et_so)) {
                                //  Toast.makeText(context, "yes", Toast.LENGTH_SHORT).show();
                                lbCheck = true;
                                break;
                            }
                        }
                        if (lbCheck == false) {
                            mtimeAdapter.clearSelection();
                            for (int i = position; i >= liSelectedFirst; i--) {
                                mtimeAdapter.toggleSelection(i);
                                mtimeAdapter.notifyItemChanged(i);
                            }
                            tvEndTime.setText(alMTime.get(position).getTime());
                            endTime = alMTime.get(position).getTime();
                            etSortingOrder = alMTime.get(position).getSortingOrder();
                        } else {
                            mtimeAdapter.clearSelection();
                            tvStartTime.setText("");
                            tvEndTime.setText("");
                            startTime = "";
                            endTime = "";
                            // Toast.makeText(context, "Cannot book this slot..choose other one", Toast.LENGTH_SHORT).show();
                            CustomToast.showToast(activity, "This slot already booked. Choose other one.", 50);
                        }
                    }

                }

                //  mtimeAdapter.setMultiple(true);
            }

        });

    }




/*
    public void showMTime() {
        llmMorning = new GridLayoutManager(context, 5);
        rcvMTime.setLayoutManager(llmMorning);
        mtimeAdapter = new MTimeAdapter(context, alMTime);
        rcvMTime.setAdapter(mtimeAdapter);

        mtimeAdapter.setItemClickInterface(new MTimeAdapter.itemClickIterface() {
            @Override
            public void itemClick(int position) {
                String lsTime = alMTime.get(position).getTime();

                if (tvStartTime.getText().toString() == "" || tvStartTime.getText().toString() == null) {
                    tvStartTime.setText(lsTime);
                } else {
                    if (!CustomServices.TimeValidator(tvStartTime.getText().toString(), lsTime)) {
                        tvEndTime.setText("");
                        CustomToast.showToast(activity, "End Time cannot be less than From Time", 50);
                        return;
                    } else {
                        tvEndTime.setText(lsTime);
                    }
                }
            }

        });


        mtimeAdapter.setItemClickInterface2(new MTimeAdapter.itemClickIterface2() {
            @Override
            public void itemClick2(int position) {
                String lsTime = alMTime.get(position).getTime();

                if (tvStartTime.getText().toString().equals(lsTime)) {
                    tvStartTime.setText("");
                } else {
                    if (tvEndTime.getText().toString().equals(lsTime)) {
                        tvEndTime.setText("");

                    }

                }
            }

        });

    }
*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
