package com.WeApplify.Tap2Meet.Tables;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.WeApplify.Tap2Meet.Services.DbHelper;
import com.WeApplify.Tap2Meet.Services.Debugger;

/**
 * Created by Admin on 21-06-2017.
 */

public class WorkspaceBooked extends Application {
    public static final String TAG = "WorkspaceBooked";

    public static final String TABLENAME = "workspace_booked";

    public static final String WB_ID = "wb_id";
    public static final String WB_BOOKINGID = "wb_bookingid";
    public static final String WB_EMPLOYEE = "wb_employee";
    public static final String WB_WORKSPACEID = "wb_workspaceid";
    public static final String WB_MEETINGDATE = "wb_Meetingdate";
    public static final String WB_BOOKINGDATE = "wb_bookingdate";
    public static final String WB_PLANNEDSTARTTIME = "wb_plannedstarttime";
    public static final String WB_PLANNEDENDTIME = "wb_plannedendtime";
    public static final String WB_ACTUALSTARTTIME = "wb_actualstarttime";
    public static final String WB_ACTUALENDTIME = "wb_actualendtime";
    public static final String WB_AGENDA = "wb_agenda";
    public static final String WB_STATUS = "wb_status";
    public static final String WB_TITLE = "wb_title";
    public static final String WB_BOOKING_STATUS = "wb_booking_status";
    public static final String WB_ST_SO = "wb_st_so";
    public static final String WB_ET_SO = "wb_et_so";
    public static final String WB_CONFIDENTIAL = "wb_confidential";


    static DbHelper dbHelper;
    public static Context context;
    static SQLiteDatabase db;

    public WorkspaceBooked(Context context) {
        this.context = context;
    }

    public void getDB() {

        dbHelper = DbHelper.getInstance(context);
        db = dbHelper.getWritableDatabase();

    }

    public long CRUD(ContentValues cv) {
        getDB();
        long llInsert = db.insert(TABLENAME, null, cv);
        Debugger.debug(TAG, "User Profile " + llInsert);
        return llInsert;
    }


}