package com.WeApplify.Tap2Meet.Tables;

/**
 * Created by ADMIN on 03-12-2017.
 */

public class NotificationTable {
    public static final String TAG = "NotificationTable";

    public static final String TABLENAME = "notification_table";

    public static final String NOT_ID = "not_id";
    public static final String NOT_TEXT = "not_text";
    public static final String NOT_FLAG = "not_flag";
    public static final String NOT_EMAIL = "not_email";

}
