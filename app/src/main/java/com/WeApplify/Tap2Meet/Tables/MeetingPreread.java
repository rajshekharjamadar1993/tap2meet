package com.WeApplify.Tap2Meet.Tables;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.WeApplify.Tap2Meet.Services.DbHelper;
import com.WeApplify.Tap2Meet.Services.Debugger;

/**
 * Created by Admin on 21-06-2017.
 */

public class MeetingPreread extends Application {
    public static final String TAG = "MeetingPreread";

    public static final String TABLENAME = "meeting_preread";

    public static final String MPR_BOOKINGID = "mpr_bookingid";
    public static final String MPR_CAPTION = "mpr_caption";
    public static final String MPR_FILE = "mpr_file";

    static DbHelper dbHelper;
    public static Context context;
    static SQLiteDatabase db;

    public MeetingPreread(Context context) {
        this.context = context;
    }

    public void getDB() {

        dbHelper = DbHelper.getInstance(context);
        db = dbHelper.getWritableDatabase();

    }

    public long CRUD(ContentValues cv) {
        getDB();
        long llInsert = db.insert(TABLENAME, null, cv);
        Debugger.debug(TAG, "User Profile " + llInsert);
        return llInsert;
    }


}