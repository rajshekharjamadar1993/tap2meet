package com.WeApplify.Tap2Meet.Tables;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.WeApplify.Tap2Meet.Services.DbHelper;

/**
 * Created by harshagulati on 16/08/17.
 */

public class FeedbackAnsTable {

    static DbHelper dbHelper;
    public static Context context;
    static SQLiteDatabase db;

    public FeedbackAnsTable(Context context) {
        this.context = context;
    }

    public static final String TABLENAME = "feedback";
    public static final String FB_EMPLOYEE = "fb_employee";
    public static final String FB_BOOKINGID = "fb_bookingid";
    public static final String FB_QUESTION = "fb_question";
    public static final String FB_SUGGESTION = "fb_suggestion";
    public static final String FB_ANSWER = "fb_answer";




}
