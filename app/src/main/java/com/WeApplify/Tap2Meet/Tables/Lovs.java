package com.WeApplify.Tap2Meet.Tables;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.WeApplify.Tap2Meet.Services.DbHelper;


/**
 * Created by apple on 23/05/17.
 */

public class Lovs extends Application {
    public static final String TAG = "Lovs";

    public static final String TABLENAME = "Lovs";

    public static final String LOV_ID = "lov_id";
    public static final String LOV_TYPE = "lov_type";
    public static final String LOV_DISPLAYVALUE = "lov_displayvalue";
    public static final String LOV_STOREVALUE = "lov_storevalue";
    public static final String LOV_SORTINGORDER = "lov_sortingorder";
    public static final String LOV_PARENT = "lov_parent";

    static DbHelper dbHelper;
    public static Context context;
    static SQLiteDatabase db;

    public Lovs(Context context) {
        this.context = context;
    }

    public void getDB() {

        dbHelper = DbHelper.getInstance(context);
        db = dbHelper.getWritableDatabase();

    }
    public long CRUD(ContentValues cv) {
        getDB();
        long llInsert = db.insert(TABLENAME, null, cv);
        return llInsert;
    }
}

