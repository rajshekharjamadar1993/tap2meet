package com.WeApplify.Tap2Meet.Tables;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.WeApplify.Tap2Meet.Services.DbHelper;
import com.WeApplify.Tap2Meet.Services.Debugger;

/**
 * Created by Admin on 21-06-2017.
 */

public class UserLogin extends Application {
    public static final String TAG = "userlogin";

    public static final String TABLENAME = "userlogin";

    public static final String UL_ID = "ul_id";
    public static final String UL_EMAIL = "ul_email";
    public static final String UL_PASSWORD = "ul_password";
    public static final String UL_EMPLOYEEID = "ul_employeeid";
    public static final String UL_DEVICEKEY = "ul_devicekey";

    static DbHelper dbHelper;
    public static Context context;
    static SQLiteDatabase db;

    public UserLogin(Context context) {
        this.context = context;
    }

    public void getDB() {

        dbHelper = DbHelper.getInstance(context);
        db = dbHelper.getWritableDatabase();

    }

    public long CRUD(ContentValues cv) {
        getDB();
        long llInsert = db.insert(TABLENAME, null, cv);
        Debugger.debug(TAG, "User Profile " + llInsert);
        return llInsert;
    }

    public boolean validateUser(String lsEmail, String lsPassword)
    {
        getDB();
        String lsSql = " SELECT " + UL_EMAIL + "," + UL_PASSWORD + " FROM " + TABLENAME;
        lsSql = lsSql + " WHERE " + UL_EMAIL + " = '" + lsEmail + "' AND " + UL_PASSWORD + " = '" + lsPassword + "'";
        Cursor cursor = db.rawQuery(lsSql, null);
        if(cursor.getCount() > 0)
        {
            return true;
        }
        return false;
    }
}