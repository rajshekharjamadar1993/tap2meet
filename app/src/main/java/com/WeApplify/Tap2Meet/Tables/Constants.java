package com.WeApplify.Tap2Meet.Tables;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.WeApplify.Tap2Meet.Services.DbHelper;


/**
 * Created by apple on 23/05/17.
 */

public class Constants extends Application {
    public static final String TAG = "Lovs";

    public static final String MTG_WORKSPACE_TABLENAME = "mtg_workspace";

    public static final String MW_ID = "mw_id";
    public static final String MW_NAME = "mw_name";
    public static final String MW_TYPE = "mw_type";
    public static final String MW_CITY = "mw_city";
    public static final String MW_OFFICE = "mw_office";
    public static final String MW_CAPACITY = "mw_capacity";
    public static final String MW_ACCESS = "mw_access";
    public static final String TIME_BOOKED = "time_booked";


    public static final String POST_NOTIFICATION ="http://www.weapplify.tech/bmw/pushynotification.php";

    public static final String MTG_WORKSPACE_AMENITIES_TABLENAME = "mtg_wrkspace_amenities";

    public static final String ID = "id";
    public static final String WA_MWID = "wa_mwid";
    public static final String WA_AMID = "wa_amid";


    public static final String AMENITIES_TABLENAME = "amenities";

    public static final String AM_ID = "am_id";
    public static final String AM_NAME = "am_name";
    public static final String AM_DESC = "am_desc";


    public static final String TIME_TABLENAME = "time_lovs";

    public static final String TIME_ID = "time_id";
    public static final String TIME_TEXT = "time_text";
    public static final String TIME_SORTINGORDER = "time_sortingorder";
    public static final String TIME_PHASE = "time_phase";


    public static final String SERVICES_TABLENAME = "services";
    public static final String SV_ID = "sv_id";
    public static final String SV_DESC = "sv_desc";
    public static final String SV_EMAIL = "sv_email";

    public static final String LOVS_TABLENAME = "Lovs";
    public static final String LOVS_ID = "lov_id";
    public static final String LOVS_TYPE = "lov_type";
    public static final String LOVS_DISPLAYVALUE = "lov_displayvalue";
    public static final String LOVS_STOREVALUE = "lov_storevalue";
    public static final String LOVS_SORTINGORDER = "lov_sortingorder";



    public static final String MEETING_WORKSPACE_TABLE = "workspace_booked";
    public static final String MEETING_PARTICIPANTS_TABLE = "meeting_participants";
    public static final String WB_BOOKINGID = "wb_bookingid";
    public static final String MEETING_BOOKINGID = "mp_bookingid";
    public static final String EMPLOYEE_EMAIL = "mp_EmployeeEmail";
    public static final String MEETING_TITLE = "wb_title";
    public static final String MEETING_STARTTIME = "wb_plannedstarttime";
    public static final String MEETING_ENDTIME = "wb_plannedendtime";
    public static final String MEETING_STATUS = "wb_status";

    public static final String ANALYSIS_COUNT = "count(*)";
    public static final String ANALYSIS_SUM = "sum(wb_totalspendtime)";


    static DbHelper dbHelper;
    public static Context context;
    static SQLiteDatabase db;


    public Constants(Context context) {
        this.context = context;
    }





}

