package com.WeApplify.Tap2Meet.Tables;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.WeApplify.Tap2Meet.Services.DbHelper;
import com.WeApplify.Tap2Meet.Services.Debugger;

/**
 * Created by Admin on 21-06-2017.
 */

public class MeetingParticipants extends Application {
    public static final String TAG = "MeetingParticipants";
    public static final String TABLENAME = "meeting_participants";

    public static final String MP_BOOKINGID = "mp_bookingid";
    public static final String MP_EMPLOYEEEMAIL = "mp_EmployeeEmail";

    static DbHelper dbHelper;
    public static Context context;
    static SQLiteDatabase db;

    public MeetingParticipants(Context context) {
        this.context = context;
    }

    public void getDB() {

        dbHelper = DbHelper.getInstance(context);
        db = dbHelper.getWritableDatabase();
    }

    public long CRUD(ContentValues cv) {
        getDB();
        long llInsert = db.insert(TABLENAME, null, cv);
        Debugger.debug(TAG, "User Profile " + llInsert);
        return llInsert;
    }


}