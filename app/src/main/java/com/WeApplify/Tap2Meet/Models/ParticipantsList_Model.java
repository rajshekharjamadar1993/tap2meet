package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.UserLogin;

/**
 * Created by apple on 07/09/17.
 */

public class ParticipantsList_Model {
    static String TAG = "ParticipantsList_Model";

    String lsUsers;

    public ParticipantsList_Model(Bundle b) {

        lsUsers = b.getString(UserLogin.UL_EMAIL);
    }

    public String getParticipantsList(){
        return lsUsers;
    }

    public Bundle getData() {

        Bundle b = new Bundle();

        b.putString(UserLogin.UL_EMAIL, lsUsers);


        return b;
    }

}
