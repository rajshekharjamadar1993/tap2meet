package com.WeApplify.Tap2Meet.Models;

import android.content.Context;
import android.os.Bundle;

import com.WeApplify.Tap2Meet.Tables.Lovs;
import com.WeApplify.Tap2Meet.Tables.UserLogin;


public class UserModel {

    static String TAG = "UserModel";

     String lsName,lsPassword,lsEmployeeId;
     Context context;

    public UserModel(Context context) {
        this.context = context;
    }

    public UserModel(Bundle b) {
        lsName = b.getString(UserLogin.UL_EMAIL);
        lsPassword = b.getString(UserLogin.UL_PASSWORD);
        lsEmployeeId = b.getString(UserLogin.UL_EMPLOYEEID);
    }

    public String getName(){
        return lsName;
    }
    public String getPassword(){
        return lsPassword;
    }
    public String getEmployeeId()
    {
        return lsEmployeeId;
    }


}
