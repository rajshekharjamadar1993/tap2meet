package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.Tables.Constants;

/**
 * Created by Admin on 22-06-2017.
 */

public class ETimeModel {
    static String TAG = "ETimeModel";

    String lsTime;
    int liSo;

    public ETimeModel(Bundle b) {

        lsTime = b.getString(Constants.TIME_TEXT);
        liSo = b.getInt(Constants.TIME_SORTINGORDER);
    }

    public String getTime(){
        return lsTime;
    }

    public int getSortingOrder()
    {
        return  liSo;
    }

    public Bundle getData() {

        Bundle b = new Bundle();

        b.putString(Constants.TIME_TEXT, lsTime);
        b.putInt(Constants.TIME_SORTINGORDER, liSo);

        return b;
    }

}