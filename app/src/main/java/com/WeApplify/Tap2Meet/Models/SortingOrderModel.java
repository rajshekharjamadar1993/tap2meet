package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.Tables.Constants;

/**
 * Created by ADMIN on 09-11-2017.
 */

public class SortingOrderModel {
    static String TAG = "SortingOrderModel";

    int liStartSo , liEndSo , liStartSo1 , liEndSo1;

    public SortingOrderModel(Bundle b) {

        liStartSo = b.getInt("startOrder");
        liEndSo = b.getInt("endOrder");
        liStartSo1 = b.getInt("startOrder1");
        liEndSo1 = b.getInt("endOrder1");
    }

    public int getStartSo() {
        return liStartSo;
    }
    public int getEndSo() {
        return liEndSo;
    }
    public int getStartSo1() {
        return liStartSo1;
    }
    public int getEndSo1() {
        return liEndSo1;
    }

}
