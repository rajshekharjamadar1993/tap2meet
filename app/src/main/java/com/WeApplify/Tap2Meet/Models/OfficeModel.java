package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.Tables.Constants;

import java.util.ArrayList;


public class OfficeModel {

    static String TAG = "OfficeModel";

    String lsOffice;
    ArrayList<String> alAmenities;
    public OfficeModel(Bundle b) {

        lsOffice = b.getString(Constants.MW_OFFICE);


    }

    public String getOffice(){
        return lsOffice;
    }

    public Bundle getData() {

        Bundle b = new Bundle();

        b.putString(Constants.MW_OFFICE, lsOffice);

        return b;
    }


    public void setAmenities(ArrayList<String> alList)
    {
            alAmenities = alList;
    }


    public ArrayList<String> getAmenities()
    {
        return alAmenities;
    }
}
