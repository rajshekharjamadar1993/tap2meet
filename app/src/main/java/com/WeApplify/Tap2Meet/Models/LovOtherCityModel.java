package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.Tables.Lovs;


public class LovOtherCityModel {

    static String TAG = "LovOtherCityModel";

    String lsDisplayvalue,lsStorevalue;

    public LovOtherCityModel(Bundle b) {

        lsDisplayvalue = b.getString(Lovs.LOV_DISPLAYVALUE);
        lsStorevalue = b.getString(Lovs.LOV_STOREVALUE);


    }

    public String getDisplayvalue(){
        return lsDisplayvalue;
    }
    public String getStorevalue(){
        return lsStorevalue;
    }

    public Bundle getData() {

        Bundle b = new Bundle();

        b.putString(Lovs.LOV_DISPLAYVALUE, lsDisplayvalue);
        b.putString(Lovs.LOV_STOREVALUE, lsStorevalue);

        return b;
    }

}
