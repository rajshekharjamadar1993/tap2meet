package com.WeApplify.Tap2Meet.Models;

import android.content.Context;
import android.os.Bundle;

import com.WeApplify.Tap2Meet.Tables.Constants;

/**
 * Created by ADMIN on 30-11-2017.
 */

public class DateFromNotificationModel {
    static String TAG = "DateFromNotificationModel";

    String lsDate;
    Context context;

    public DateFromNotificationModel(Bundle b) {

        lsDate = b.getString("NotificationDate");
    }

    public DateFromNotificationModel(Context context)
    {
        this.context = context;
    }


    public String getNotificationDate(){
        return lsDate;
    }

}
