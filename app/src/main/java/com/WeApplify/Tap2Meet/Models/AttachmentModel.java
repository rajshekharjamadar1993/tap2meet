package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.Tables.MeetingPreread;

import java.io.Serializable;


public class  AttachmentModel implements Serializable {

    static String TAG = "AttachmentModel";

    String lsName;



    public AttachmentModel(Bundle b) {

        lsName = b.getString(MeetingPreread.MPR_FILE);

    }



    public String getAttachmentName(){
        return lsName;
    }






    public Bundle getData() {

        Bundle b = new Bundle();

        b.putString(MeetingPreread.MPR_FILE, lsName);

        return b;
    }

}
