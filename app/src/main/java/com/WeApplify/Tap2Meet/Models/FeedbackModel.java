package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;
import android.widget.RatingBar;

import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.FeedbackAnsTable;

/**
 * Created by Admin on 07-07-2017.
 */

public class FeedbackModel {

    static String TAG = "FeedbackModel";

    String lsQuestion, lsQueId, id;
    String lsValue,lsRating;

    public FeedbackModel(Bundle b) {
        lsQuestion = b.getString(Constants.LOVS_DISPLAYVALUE);
        lsQueId = b.getString(Constants.LOVS_STOREVALUE);
       // id = b.getString("QueId");
       // lsRating = b.getString(FeedbackAnsTable.FB_ANSWER);
    }

    public String getQuestion(){
        return lsQuestion;
    }

    public String getQueId(){
        return lsQueId;
    }


    public void setResults(String lsValue)
    {
        this.lsValue = lsValue;
    }

    public String getResults()
    {
        return lsValue;
    }


    public Bundle getData() {

        Bundle b = new Bundle();

        b.putString(Constants.LOVS_DISPLAYVALUE, lsQuestion);
        b.putString(Constants.LOVS_STOREVALUE, lsQueId);
        return b;
    }

    public Bundle getValues() {

        Bundle b = new Bundle();
        b.putString(Constants.LOVS_STOREVALUE, lsQueId);
        b.putString(FeedbackAnsTable.FB_ANSWER, lsValue);
        return b;
    }


  /*  public Bundle getQId() {

        Bundle b = new Bundle();

        b.putString("QueId", id);
        b.putString(FeedbackAnsTable.FB_ANSWER, lsRating);

        return b;
    }*/

}
