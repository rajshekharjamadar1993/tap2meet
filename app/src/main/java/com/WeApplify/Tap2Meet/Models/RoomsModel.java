package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.Tables.Constants;

import java.util.ArrayList;


public class RoomsModel {

    static String TAG = "RoomsModel";

    String lsCity,lscapacity,lsLandmark,lsAddress,lsAvailibility,lsType,lsName;
    int liId;
    ArrayList<AmenitiesModel> alAmenities;

    public RoomsModel(Bundle b) {

        liId = b.getInt(Constants.MW_ID);
        lsCity = b.getString(Constants.MW_OFFICE);
        lscapacity = b.getString(Constants.MW_CAPACITY);
        lsType = b.getString(Constants.MW_TYPE);
        lsName = b.getString(Constants.MW_NAME);

    }

    public int getId(){
        return liId;
    }
    public String getCity(){
        return lsCity;
    }
    public String getCapacity(){
        return lscapacity;
    }

    public String getLandmark(){
        return lsLandmark;
    }
    public String getAddress(){
        return lsAddress;
    }
    public String getAvailability(){
        return lsAvailibility;
    }
    public String getType(){
        return lsType;
    }
    public String getName(){
        return lsName;
    }


    public void setAmenities(ArrayList<AmenitiesModel> alList)
    {
        alAmenities = alList;
    }


    public ArrayList<AmenitiesModel> getAmenities()
    {
        return alAmenities;
    }

    public Bundle getData() {

        Bundle b = new Bundle();

        b.putInt(Constants.MW_ID, liId);
        b.putString(Constants.MW_OFFICE, lsCity);
        b.putString(Constants.MW_CAPACITY, lscapacity);
        b.putString(Constants.MW_TYPE, lsType);
        b.putString(Constants.MW_NAME,lsName);

        return b;
    }

}
