package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.Tables.MeetingParticipants;
import com.WeApplify.Tap2Meet.Tables.UserLogin;

import java.io.Serializable;


public class  ParticipantModel implements Serializable {

    static String TAG = "ParticipantModel";

    String lsName, lsHost;



    public ParticipantModel(Bundle b) {

        lsName = b.getString(MeetingParticipants.MP_EMPLOYEEEMAIL);
        lsHost = b.getString(UserLogin.UL_EMAIL);

    }



    public String getName(){
        return lsName;
    }
    public String getHost()
    {
        return lsHost;
    }






    public Bundle getData() {

        Bundle b = new Bundle();

        b.putString(MeetingParticipants.MP_EMPLOYEEEMAIL, lsName);
        b.putString(UserLogin.UL_EMAIL,lsHost);

        return b;
    }

}
