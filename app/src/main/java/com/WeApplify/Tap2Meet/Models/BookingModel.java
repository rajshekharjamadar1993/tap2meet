package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.Tables.UserLogin;
import com.WeApplify.Tap2Meet.Tables.WorkspaceBooked;

import java.io.Serializable;


public class  BookingModel {

    static String TAG = "BookingModel";

    String lsName,lsStartTime,lsEndTime,lsId,lsAgenda,lsMeetingDate,
            lsTitle, lsEmployeeId,lsStatus,lsBookingdate, lsBookingStatus,lsBookedMail,lsLoggedInUserEmail;
    int liSso,liEso;



    public BookingModel(Bundle b) {
        lsName = b.getString(WorkspaceBooked.WB_WORKSPACEID);
        lsStartTime = b.getString(WorkspaceBooked.WB_PLANNEDSTARTTIME);
        lsEndTime = b.getString(WorkspaceBooked.WB_PLANNEDENDTIME);
        lsId = b.getString(WorkspaceBooked.WB_BOOKINGID);
        lsAgenda = b.getString(WorkspaceBooked.WB_AGENDA);
        lsMeetingDate = b.getString(WorkspaceBooked.WB_MEETINGDATE);
        lsTitle = b.getString(WorkspaceBooked.WB_TITLE);
        lsEmployeeId = b.getString(WorkspaceBooked.WB_EMPLOYEE);
        lsStatus = b.getString(WorkspaceBooked.WB_STATUS);
        lsBookingdate = b.getString(WorkspaceBooked.WB_BOOKINGDATE);
        lsBookingStatus = b.getString(WorkspaceBooked.WB_BOOKING_STATUS);
        liSso = b.getInt(WorkspaceBooked.WB_ST_SO);
        liEso = b.getInt(WorkspaceBooked.WB_ET_SO);
        lsBookedMail = b.getString(UserLogin.UL_EMAIL);
        lsLoggedInUserEmail = b.getString("LoggedInUserEmail");
    }



    public String getName(){
        return lsName;
    }
    public String getStartTime(){
        return lsStartTime;
    }
    public String getEndTime(){
        return lsEndTime;
    }
    public String getId(){
        return lsId;
    }
    public String getAgenda(){
        return lsAgenda;
    }
    public String getMeetingDate(){
        return lsMeetingDate;
    }
    public String getTitle(){
        return lsTitle;
    }
    public String getEmployeeId()
    {
        return lsEmployeeId;
    }
    public String getStatus()
    {
        return lsStatus;
    }
    public String getBookingDate()
    {
        return lsBookingdate;
    }
    public String getBookingStatus()
    {
        return lsBookingStatus;
    }
    public int getStartSo()
    {
        return liSso;
    }
    public int getEndSo()
    {
        return liEso;
    }
    public String getBookedMail()
    {
        return lsBookedMail;
    }
    public String getLoggedInUser()
    {
       return lsLoggedInUserEmail;
    }









    public Bundle getData() {

        Bundle b = new Bundle();

        b.putString(WorkspaceBooked.WB_WORKSPACEID, lsName);
        b.putString(WorkspaceBooked.WB_PLANNEDENDTIME, lsEndTime);
        b.putString(WorkspaceBooked.WB_PLANNEDSTARTTIME, lsStartTime);
        b.putString(WorkspaceBooked.WB_BOOKINGID, lsId);
        b.putString(WorkspaceBooked.WB_AGENDA, lsAgenda);
        b.putString(WorkspaceBooked.WB_MEETINGDATE, lsMeetingDate);
        b.putString(WorkspaceBooked.WB_TITLE, lsTitle);
        b.putString(WorkspaceBooked.WB_BOOKING_STATUS, lsBookingStatus);
        b.putString(UserLogin.UL_EMAIL, lsBookedMail);
        b.putString("LoggedInUserEmail",lsLoggedInUserEmail);
        return b;
    }

}
