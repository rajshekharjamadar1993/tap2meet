package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.Tables.Constants;

/**
 * Created by Admin on 22-06-2017.
 */

public class MTimeModel {
    static String TAG = "MTimeModel";

    String lsTime, lsTimePhase;
    int liSo;
    boolean lbBooked,lbBooked1;

    public MTimeModel(Bundle b) {

        lsTime = b.getString(Constants.TIME_TEXT);
        liSo = b.getInt(Constants.TIME_SORTINGORDER);
        lsTimePhase = b.getString(Constants.TIME_PHASE);
    }

    public String getTime() {
        return lsTime;
    }

    public int getSortingOrder() {
        return liSo;
    }

    public String getTimePhase() {
        return lsTimePhase;
    }

    public Bundle getData() {
        Bundle b = new Bundle();
        b.putString(Constants.TIME_TEXT, lsTime);
        b.putInt(Constants.TIME_SORTINGORDER, liSo);
        b.putString(Constants.TIME_PHASE, lsTimePhase);
        b.putBoolean(Constants.TIME_BOOKED , lbBooked);
        return b;
    }

    public void setBooked(boolean lbValue) {
        lbBooked = lbValue;
    }
    public void setBooked1(boolean lbValue) {
        lbBooked1 = lbValue;
    }


}