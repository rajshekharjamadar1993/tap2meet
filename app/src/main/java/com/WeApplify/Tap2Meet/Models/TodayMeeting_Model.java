package com.WeApplify.Tap2Meet.Models;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.WeApplify.Tap2Meet.App;
import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.UserLogin;
import com.WeApplify.Tap2Meet.Tables.WorkspaceBooked;

/**
 * Created by apple on 04/09/17.
 */

public class TodayMeeting_Model 
{
    static String TAG = "TodayMeeting_Model";

        String lsMeetingTitle,lsStartTime,lsEndTime,lsRoomName,lsEmail,lsLoginUser;

        public TodayMeeting_Model(Bundle b) {
            lsMeetingTitle = b.getString(Constants.MEETING_TITLE);
            lsStartTime =b.getString(Constants.MEETING_STARTTIME);
            lsEndTime=b.getString(Constants.MEETING_ENDTIME);
            lsRoomName =b.getString(Constants.MW_NAME);
            lsEmail = b.getString(UserLogin.UL_EMAIL);
            lsLoginUser = b.getString("Login_user");
        }

        public String getName(){
            return lsMeetingTitle;
        }
        public String getEmail()
        {
            return lsEmail;
        }

        public Bundle getData() {

            Bundle b = new Bundle();
            b.putString(Constants.MEETING_TITLE, lsMeetingTitle);
            b.putString(Constants.MEETING_STARTTIME, lsStartTime);
            b.putString(Constants.MEETING_ENDTIME, lsEndTime);
            b.putString(Constants.MW_NAME,lsRoomName);
            b.putString(UserLogin.UL_EMAIL,lsEmail);
            b.putString("Login_user",lsLoginUser);
            return b;
        }

}


