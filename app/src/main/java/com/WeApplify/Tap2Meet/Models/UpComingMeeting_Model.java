package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.UserLogin;
import com.WeApplify.Tap2Meet.Tables.WorkspaceBooked;

/**
 * Created by apple on 26/07/17.
 */

public class UpComingMeeting_Model {
    static String TAG = "UpComingMeeting_Model";

    String lsMeetingTitle,lsStartTime,lsEndTime,lsDate,lsRoomName,lsEmail,lsLoginUser;

    public UpComingMeeting_Model(Bundle b) {
        lsMeetingTitle = b.getString(Constants.MEETING_TITLE);
        lsStartTime =b.getString(Constants.MEETING_STARTTIME);
        lsEndTime=b.getString(Constants.MEETING_ENDTIME);
        lsDate =b.getString(WorkspaceBooked.WB_MEETINGDATE);
        lsRoomName =b.getString(Constants.MW_NAME);
        lsEmail = b.getString(UserLogin.UL_EMAIL);
        lsLoginUser = b.getString("Login_user");
    }

    public String getName(){
        return lsMeetingTitle;
    }
    public String getEmail()
    {
        return lsEmail;
    }

    public Bundle getData() {

        Bundle b = new Bundle();
        b.putString(Constants.MEETING_TITLE, lsMeetingTitle);
        b.putString(Constants.MEETING_STARTTIME, lsStartTime);
        b.putString(Constants.MEETING_ENDTIME, lsEndTime);
        b.putString(WorkspaceBooked.WB_MEETINGDATE, lsDate);
        b.putString(Constants.MW_NAME,lsRoomName);
        b.putString(UserLogin.UL_EMAIL,lsEmail);
        b.putString("Login_user",lsLoginUser);
        return b;
    }

}

