package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.Tables.Constants;
import com.WeApplify.Tap2Meet.Tables.NotificationTable;

/**
 * Created by ADMIN on 04-12-2017.
 */

public class NotificationModel {
    static String TAG = "NotificationModel";

    String lsNotification;

    public NotificationModel(Bundle b) {

        lsNotification = b.getString(NotificationTable.NOT_TEXT);
    }

    public String getNotification() {
        return lsNotification;
    }


    public Bundle getData() {

        Bundle b = new Bundle();

        b.putString(NotificationTable.NOT_TEXT, lsNotification);

        return b;
    }
}
