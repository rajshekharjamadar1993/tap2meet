package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.Tables.Constants;

import java.io.Serializable;

/**
 * Created by Admin on 04-07-2017.
 */

public class ServicesModel implements Serializable {
    static String TAG = "ServicesModel";

        String lsService;
        int liId;

        public ServicesModel(Bundle b) {

            lsService = b.getString(Constants.SV_DESC);
            liId = b.getInt(Constants.SV_ID);

        }

        public String getService(){
            return lsService;
        }
        public int getId(){
        return liId;
    }

        public Bundle getData() {

            Bundle b = new Bundle();
            b.putString(Constants.SV_DESC, lsService);
            return b;
        }

}
