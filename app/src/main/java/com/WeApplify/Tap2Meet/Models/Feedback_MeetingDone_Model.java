package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.Tables.Constants;

/**
 * Created by Admin on 21-07-17.
 */

public class Feedback_MeetingDone_Model  {
        static String TAG = "Feedback_MeetingDone_Model";

        String lsMeetingTitle,lsStartTime,lsEndTime,lsBookingId;

        public Feedback_MeetingDone_Model(Bundle b) {
            lsMeetingTitle = b.getString(Constants.MEETING_TITLE);
            lsStartTime =b.getString(Constants.MEETING_STARTTIME);
            lsEndTime=b.getString(Constants.MEETING_ENDTIME);
            lsBookingId = b.getString(Constants.WB_BOOKINGID);
        }

        public String getName(){
            return lsMeetingTitle;
        }

        public String getBookingId(){
        return lsBookingId;
    }

        public Bundle getData() {

            Bundle b = new Bundle();

            b.putString(Constants.MEETING_TITLE, lsMeetingTitle);
            b.putString(Constants.MEETING_STARTTIME, lsStartTime);
            b.putString(Constants.MEETING_ENDTIME, lsEndTime);
            b.putString(Constants.WB_BOOKINGID, lsBookingId);
            return b;
        }

    }
