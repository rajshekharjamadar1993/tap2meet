package com.WeApplify.Tap2Meet.Models;

import android.os.Bundle;

import com.WeApplify.Tap2Meet.Tables.Constants;


public class AmenitiesModel {

    static String TAG = "AmenitiesModel";

    String lsName;

    public AmenitiesModel(Bundle b) {

        lsName = b.getString(Constants.AM_NAME);
        //lsStorevalue = b.getString(Lovs.LOV_STOREVALUE);
    }

    public String getName(){
        return lsName;
    }

    public Bundle getData() {

        Bundle b = new Bundle();

        b.putString(Constants.AM_NAME, lsName);

        return b;
    }

}
