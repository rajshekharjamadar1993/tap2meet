package com.WeApplify.Tap2Meet;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.MatrixCursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.WeApplify.Tap2Meet.Services.CustomDialog;
import com.WeApplify.Tap2Meet.Tables.UserLogin;
import com.WeApplify.Tap2Meet.UI.Home;
import com.WeApplify.Tap2Meet.UI.Login;
import com.WeApplify.Tap2Meet.UI.Notifications;

import java.util.HashMap;

public class BaseActivity extends Activity implements View.OnClickListener {
    Context context;
    Activity activity;
    public LinearLayout baseLayout;
    TextView tvTitle;
    ImageView ivSearch, ivNotification, ivLogout;
    private ActionBarDrawerToggle mDrawerToggle;
    DrawerLayout mDrawer;
    LinearLayout leftMenu;
    TextView tvBack;
    static String TAG = "BaseActivity";
    ImageView ivBack, ivHome;
    int sumHeight;
    MatrixCursor mtrxCursor;
    HashMap<Integer, Integer> itemHeight;

    public LinearLayout llHeader;
    public RelativeLayout rlHeader;

    SharedPreferences sp,sp1;
    SharedPreferences.Editor editor;

    Handler mHandler;
    Runnable runnable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.setContentView(R.layout.base_layout);
        context = this;
        activity = this;
        baseLayout = (LinearLayout) findViewById(R.id.baseLayout);
        tvTitle = (TextView) findViewById(R.id.tvActivityTitle);
        ivLogout = (ImageView) findViewById(R.id.ivLogout);
        llHeader = (LinearLayout) findViewById(R.id.llHeader);
        rlHeader = (RelativeLayout) findViewById(R.id.app_heading);
        tvBack = (TextView) findViewById(R.id.tvBack);
        ivHome = (ImageView) findViewById(R.id.ivHome);
        ivNotification = (ImageView)findViewById(R.id.ivNotification);

        sp = getSharedPreferences(App.LOGINCREDENTIALS, context.MODE_PRIVATE);
        sp1 = getSharedPreferences(App.SCHEDULEDATE, context.MODE_PRIVATE);

        leftMenu = (LinearLayout) findViewById(R.id.leftMenu);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer);


        ivNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Notifications.class);
                editor = sp1.edit();
                editor.remove(App.SCHEDULEDATE);
                editor.commit();
                startActivity(intent);
            }
        });

        ivHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Home.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                editor = sp1.edit();
                editor.remove(App.SCHEDULEDATE);
                editor.commit();
                startActivity(intent);
            }
        });

        ivLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Login.class);
                editor = sp.edit();
                editor = sp1.edit();
                editor.remove(UserLogin.UL_EMAIL);
                editor.remove(UserLogin.UL_PASSWORD);
                editor.remove(UserLogin.UL_EMPLOYEEID);
                editor.remove(App.SCHEDULEDATE);
                editor.commit();
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

    }


    @Override
    public void setContentView(int id) {

        LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(id, baseLayout);
        //setFont(linBase,typeFace);

    }

    @Override
    public void setTitle(CharSequence ls_title) {
        tvTitle.setText(ls_title);
    }

    @Override
    public void onClick(View v) {
        //  super.onClick(v);
        switch (v.getId()) {

            case R.id.tvBack:
                super.onBackPressed();
                break;

        }
    }




    public void closeDrawer() {
        if (mDrawer != null) {
            mDrawer.closeDrawers();
        }
    }


    @Override
    public void onBackPressed() {

        if (mDrawer.isDrawerOpen(Gravity.LEFT)) {
            mDrawer.closeDrawers();
            return;
        } else {
            super.onBackPressed();
        }
    }


}